﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebHosting.DTOs;
using System.Windows.Forms;
using WebHosting.Entiteti;
using WebHosting.Mapiranja;
using NHibernate;
using NHibernate.Linq;


namespace WebHosting
{
    public class DTOManager
    {
        #region Radnici

        #region RadnikTehnickeStruke

        public static List<RadnikView> getRadnikeTehnickeStruke(string pozicija)
        {
            List<RadnikView> lista = new List<RadnikView>();
            try
            {
                ISession s = DataLayer.GetSession();

                ISQLQuery q = s.CreateSQLQuery("SELECT O.* FROM RADNIK O WHERE SPECIJALIZACIJA='Radnik_tehnicke_struke' AND POZICIJA='" + pozicija + "'");
                q.AddEntity(typeof(RadnikTehnickeStruke));


                IList<RadnikTehnickeStruke> listaTehnickeStruke = q.List<RadnikTehnickeStruke>();

                foreach (RadnikTehnickeStruke r in listaTehnickeStruke)
                {
                    lista.Add(new RadnikView(r));
                }

                s.Close();

            }
            catch
            {

            }
            return lista;
        }
        public static List<RadnikTehnickeStrukeView> getRadnikTehnickeStrukeView()
        {
            List<RadnikTehnickeStrukeView> lista = new List<RadnikTehnickeStrukeView>();
            try
            {
                ISession s = DataLayer.GetSession();

                IEnumerable<RadnikTehnickeStruke> rtsLista = from o in s.Query<RadnikTehnickeStruke>()
                                                             select o;

                foreach (RadnikTehnickeStruke rts in rtsLista)
                {
                    lista.Add(new RadnikTehnickeStrukeView(rts));
                }

                s.Close();

            }
            catch
            {

            }
            return lista;
        }

        public static RadnikTehnickeStruke getRadnikTehnickeStruke(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                RadnikTehnickeStruke rts = s.Get<RadnikTehnickeStruke>(id);

                if (rts != null)
                {
                    return rts;
                }

                s.Close();

                return null;
            }
            catch
            {
            }
            return null;
        }


        public static FizickoLiceView getFizickoLiceView(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                FizickoLice rts = s.Get<FizickoLice>(id);

                FizickoLiceView rtsv = new FizickoLiceView(rts);

                s.Close();

                return rtsv;
            }
            catch
            {

            }
            return null;
        }



        public static PravnoLiceView getPravnoLiceView(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                PravnoLice rts = s.Get<PravnoLice>(id);

                PravnoLiceView rtsv = new PravnoLiceView(rts);

                s.Close();

                return rtsv;
            }
            catch
            {

            }
            return null;
        }

        public static RadnikTehnickeStrukeView getRadnikTehnickeStrukeView(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                RadnikTehnickeStruke rts = s.Load<RadnikTehnickeStruke>(id);

                RadnikTehnickeStrukeView rtsv = new RadnikTehnickeStrukeView(rts);

                s.Close();

                return rtsv;
            }
            catch
            {

            }
            return null;
        }

        public static void AddRadnikTehnickeStruke(RadnikTehnickeStruke rts)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(rts);

                s.Flush();
                s.Close();
            }
            catch
            {

            }
        }

        public static void AddFizickoLice(FizickoLice lice)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(lice);

                s.Flush();
                s.Close();
            }
            catch
            {

            }
        }

        public static void AddPravnoLice(PravnoLice lice)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(lice);

                s.Flush();
                s.Close();
            }
            catch
            {

            }
        }



        public static void deleteFizickoLice(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                FizickoLice rts = s.Get<FizickoLice>(id);

                ITransaction t = s.BeginTransaction();

                foreach (KontaktOsoba osoba in rts.KontaktOsobe)
                {
                    KontaktOsoba osobaBrisanje = s.Load<KontaktOsoba>(osoba.Id);

                    //ili to ili da ga nadjem u bazi
                    s.Delete(osobaBrisanje);
                    //t.Commit();
                }

                rts.KontaktOsobe.Clear();
                s.Delete(rts);
                t.Commit();

                s.Flush();
                s.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException.Message);
            }
        }



        public static void deletePravnoLice(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                PravnoLice rts = s.Get<PravnoLice>(id);

                ITransaction t = s.BeginTransaction();

                foreach (KontaktOsoba osoba in rts.KontaktOsobe)
                {
                    KontaktOsoba osobaBrisanje = s.Load<KontaktOsoba>(osoba.Id);

                    //ili to ili da ga nadjem u bazi
                    s.Delete(osobaBrisanje);
                    //t.Commit();
                }

                rts.KontaktOsobe.Clear();
                s.Delete(rts);
                t.Commit();

                s.Flush();
                s.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException.Message);
            }
        }



        public static void deleteRadnikTehnickeStruke(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                RadnikTehnickeStruke rts = s.Load<RadnikTehnickeStruke>(id);

                ITransaction t = s.BeginTransaction();

                if (rts.Pozicija == "Programer")
                {
                    //                    for (int i = 0; i < rts.ProgJezici.Count; i++)
                    foreach (ProgramskiJezici jezik in rts.ProgJezici)
                    {
                        ProgramskiJezici jezikBrisanje = s.Load<ProgramskiJezici>(jezik.Id);

                        //ili to ili da ga nadjem u bazi
                        s.Delete(jezikBrisanje);
                        //t.Commit();
                    }
                }
                else
                {
                    foreach (OperativniSistemi os in rts.OperSistemi)
                    {
                        OperativniSistemi osBrisanje = s.Load<OperativniSistemi>(os.Id);

                        //ili to ili da ga nadjem u bazi
                        s.Delete(osBrisanje);
                        //t.Commit();
                    }
                }
                rts.ProgJezici.Clear();
                rts.OperSistemi.Clear();
                s.Delete(rts);
                t.Commit();

                s.Flush();
                s.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException.Message);
            }
        }

        public static void updateRadnikTehnickeStruke(RadnikTehnickeStrukeView rts)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                RadnikTehnickeStruke r = s.Load<RadnikTehnickeStruke>(rts.Id);
                r.Licno_ime = rts.Licno_ime;
                r.Ime_oca = rts.Ime_oca;
                r.Prezime = rts.Prezime;
                r.Datum_zaposlenja = rts.DatumZaposlenja;
                r.Godine_radnog_staza = rts.Godine_radnog_staza;
                r.Godina_rodjenja = rts.Godina_rodjenja;
                //if (String.Compare(r.Pozicija, "Programer") == 0)
                //{
                //    r.ProgJezici.Clear();
                //    foreach (ProgramskiJezici p in rts.ProgJezici)
                //    {
                //        r.ProgJezici.Add(p);
                //    }
                //}
                //else
                //{
                //    r.OperSistemi.Clear();
                //    foreach (OperativniSistemi os in rts.OperSistemi)
                //    {
                //        r.OperSistemi.Add(os);
                //    }
                //}

                s.SaveOrUpdate(r);

                s.Flush();
                s.Close();
            }
            catch
            {

            }
        }

        #endregion

        #region Radnik tehnicke podrske

        public static List<RadnikView> getRadnikeTehnickePodrske()
        {
            List<RadnikView> lista = new List<RadnikView>();
            try
            {
                ISession s = DataLayer.GetSession();

                ISQLQuery q = s.CreateSQLQuery("SELECT O.* FROM RADNIK O WHERE SPECIJALIZACIJA='Ekonomista_tehnicke_podrske'");
                q.AddEntity(typeof(EkonomistaTehnickePodrske));
                //                q.AddEntity(typeof(Radnik));

                //IList<Radnik> listaTehnickePodrske = q.List<Radnik>();

                IList<EkonomistaTehnickePodrske> listaTehnickePodrske = q.List<EkonomistaTehnickePodrske>();

                //   foreach (Radnik r in listaTehnickePodrske)

                foreach (EkonomistaTehnickePodrske r in listaTehnickePodrske)
                {
                    lista.Add(new RadnikView(r));
                }

                s.Close();

            }
            catch
            {

            }
            return lista;
        }
        public static List<Radnik> getRadnikeTehnickePodrskeEntiteti()
        {
            List<Radnik> lista = new List<Radnik>();
            try
            {
                ISession s = DataLayer.GetSession();

                ISQLQuery q = s.CreateSQLQuery("SELECT O.* FROM RADNIK O WHERE SPECIJALIZACIJA='Ekonomista_tehnicke_podrske'");
                q.AddEntity(typeof(EkonomistaTehnickePodrske));
                //                q.AddEntity(typeof(Radnik));

                //IList<Radnik> listaTehnickePodrske = q.List<Radnik>();

                IList<EkonomistaTehnickePodrske> listaTehnickePodrske = q.List<EkonomistaTehnickePodrske>();

                //   foreach (Radnik r in listaTehnickePodrske)

                foreach (EkonomistaTehnickePodrske r in listaTehnickePodrske)
                {
                    lista.Add(r);
                }

                s.Close();

            }
            catch
            {

            }
            return lista;
        }

        public static List<PravnoLiceView> getPravnaLica()
        {
            List<PravnoLiceView> lista = new List<PravnoLiceView>();
            try
            {
                ISession s = DataLayer.GetSession();

                //IQuery q = s.CreateQuery("from pravno_lice");
                //IList<PravnoLice> listaPravnihLica = s.Query<PravnoLice>().ToList();

                // 
                ISQLQuery q = s.CreateSQLQuery("SELECT * FROM PRAVNO_LICE");
                q.AddEntity(typeof(PravnoLice));
                //                q.AddEntity(typeof(Radnik));

                ////IList<Radnik> listaTehnickePodrske = q.List<Radnik>();


                IList<PravnoLice> listaPravnihLica = q.List<PravnoLice>();

                //IList<PravnoLice> listaPravnihLica = q.List<PravnoLice>();
                ////lista = q.List<PravnoLice>();

                ////   foreach (Radnik r in listaTehnickePodrske)

                foreach (PravnoLice r in listaPravnihLica)
                {
                    lista.Add(new PravnoLiceView(r));
                }

                s.Close();

            }
            catch
            {

            }
            return lista;
            //return (List<PravnoLice>)lista;
        }


        public static List<Korisnik> getKorisnici()
        {
            List<Korisnik> lista = new List<Korisnik>();
            try
            {
                ISession s = DataLayer.GetSession();

                IQuery q = s.CreateQuery("from Korisnik");
                IList<Korisnik> korisnici = q.List<Korisnik>();

                s.Close();

                foreach (Korisnik k in korisnici)
                    lista.Add(k);




            }
            catch (Exception ex)
            {
                MessageBox.Show("" + ex);
            }
            return lista;
            //return (List<PravnoLice>)lista;
        }

        public static List<PravnoLice> getPravnaLicaa()
        {
            List<PravnoLice> lista = new List<PravnoLice>();
            try
            {
                ISession s = DataLayer.GetSession();



                IQuery q = s.CreateQuery("from PravnoLice");
                IList<PravnoLice> pravnaLica = q.List<PravnoLice>();
                s.Close();


                foreach (PravnoLice k in pravnaLica)
                    lista.Add(k);





            }
            catch (Exception ex)
            {
                MessageBox.Show("" + ex);
            }
            return lista;
            //return (List<PravnoLice>)lista;
        }

        public static List<FizickoLice> getFizickaLicaa()
        {
            List<FizickoLice> lista = new List<FizickoLice>();
            try
            {
                ISession s = DataLayer.GetSession();

                IQuery q = s.CreateQuery("from FizickoLice");
                IList<FizickoLice> fizickaLica = q.List<FizickoLice>();

                foreach (FizickoLice k in fizickaLica)
                    lista.Add(k);



                s.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show("" + ex);
            }
            return lista;
            //return (List<PravnoLice>)lista;
        }

        public static RadnikView getRadnikView(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Radnik rts = s.Get<Radnik>(id);

                RadnikView rtsv = new RadnikView(rts);

                s.Close();

                return rtsv;
            }
            catch
            {

            }
            return null;
        }
        public static Radnik getRadnikKorisnickePodrske(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                Radnik rts = s.Load<Radnik>(id);
                s.Close();
                return rts;
            }
            catch
            {
            }
            return null;
        }

        public static EkonomistaTehnickePodrskeView getRadnikTehnickePodrskeView(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                EkonomistaTehnickePodrske rts = s.Load<EkonomistaTehnickePodrske>(id);

                EkonomistaTehnickePodrskeView rtsv = new EkonomistaTehnickePodrskeView(rts);

                s.Close();

                return rtsv;
            }
            catch
            {

            }
            return null;
        }

        public static EkonomistaTehnickePodrske getRadnikTehnickePodrske(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                EkonomistaTehnickePodrske rts = s.Load<EkonomistaTehnickePodrske>(id);

                s.Close();

                return rts;
            }
            catch
            {

            }
            return null;
        }


        public static void AddRadnikKorisnickePodrske(Radnik rts)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(rts);

                s.Flush();
                s.Close();
            }
            catch
            {

            }
        }



        public static void AddRadnikTehnickePodrske(EkonomistaTehnickePodrske rts)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(rts);

                s.Flush();
                s.Close();
            }
            catch
            {

            }
        }

        public static void deleteRadnikTehnickePodrske(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                EkonomistaTehnickePodrske rts = s.Get<EkonomistaTehnickePodrske>(id);

                ITransaction t = s.BeginTransaction();

                // rts.Korisnici.Clear();

                //s.SaveOrUpdate(rts);

                s.Delete(rts);
                t.Commit();

                s.Flush();
                s.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException.Message);
            }
        }

        public static void deleteRadnikKorisnickePodrske(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Radnik rts = s.Load<Radnik>(id);

                ITransaction t = s.BeginTransaction();

                // rts.Korisnici.Clear();

                //s.SaveOrUpdate(rts);

                s.Delete(rts);
                t.Commit();

                s.Flush();
                s.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException.Message);
            }
        }

        public static void updateRadnikKorisnicke(RadnikView rts)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                Radnik r = s.Get<Radnik>(rts.Id);
                r.Licno_ime = rts.Licno_ime;
                r.Ime_oca = rts.Ime_oca;
                r.Prezime = rts.Prezime;
                r.Datum_zaposlenja = rts.DatumZaposlenja;

                r.Datum_isteka_ugovora = rts.Datum_isteka_ugovora;
                r.Godina_rodjenja = rts.Godina_rodjenja;
                r.Godine_radnog_staza = rts.Godine_radnog_staza;
                r.Jmbg = rts.Jmbg;

                s.SaveOrUpdate(r);

                s.Flush();
                s.Close();
            }
            catch
            {

            }
        }


        public static void updatePravnoLice(PravnoLiceView rts)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                PravnoLice r = s.Get<PravnoLice>(rts.Id);
                r.Adresa = rts.Adresa;
                r.Do = rts.Do;
                r.KorespondovaoSaEkonomistom = rts.Ekonomista;
                r.Naziv = rts.Naziv;
                r.Od = rts.Od;
                r.PIB = rts.PIB;

                //r.KontaktOsobe.Clear();
                //foreach (KontaktOsoba p in rts.KontaktOsobe)
                //{
                //    r.KontaktOsobe.Add(p);
                //}


                s.SaveOrUpdate(r);

                s.Flush();
                s.Close();
            }
            catch
            {

            }
        }

        public static void updateFizickoLice(FizickoLiceView rts)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                FizickoLice r = s.Get<FizickoLice>(rts.Id);
                r.Adresa = rts.Adresa;
                r.Do = rts.Do;
                r.KorespondovaoSaEkonomistom = rts.Ekonomista;
                r.LicnoIme = rts.LicnoIme;
                r.Prezime = rts.Prezime;
                r.JMBG = rts.JMBG;

                //r.KontaktOsobe.Clear();
                //foreach (KontaktOsoba p in rts.KontaktOsobe)
                //{
                //    r.KontaktOsobe.Add(p);
                //}
                s.SaveOrUpdate(r);

                s.Flush();
                s.Close();
            }
            catch
            {

            }
        }


        public static void updateRadnikTehnickePodrske(EkonomistaTehnickePodrskeView rts)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                EkonomistaTehnickePodrske r = s.Load<EkonomistaTehnickePodrske>(rts.Id);
                r.Licno_ime = rts.Licno_ime;
                r.Ime_oca = rts.Ime_oca;
                r.Prezime = rts.Prezime;
                r.Datum_zaposlenja = rts.DatumZaposlenja;

                r.Datum_isteka_ugovora = rts.Datum_isteka_ugovora;
                r.Godina_rodjenja = rts.Godina_rodjenja;
                r.Godine_radnog_staza = rts.Godine_radnog_staza;
                r.Jmbg = rts.Jmbg;
                r.Stepen_strucne_spreme = rts.Stepen_strucne_spreme;

                s.SaveOrUpdate(r);

                s.Flush();
                s.Close();
            }
            catch
            {

            }
        }

        #endregion

        #region Radnik korisnicke podrske

        public static List<RadnikView> getRadnikeKorisnickePodrske()
        {
            List<RadnikView> lista = new List<RadnikView>();
            try
            {
                ISession s = DataLayer.GetSession();

                ISQLQuery q = s.CreateSQLQuery("SELECT O.* FROM RADNIK O WHERE SPECIJALIZACIJA NOT LIKE 'Ekonomista_tehnicke_podrske' AND SPECIJALIZACIJA NOT LIKE 'Radnik_tehnicke_struke'");
                q.AddEntity(typeof(Radnik));


                IList<Radnik> listaKorisnickePodrske = q.List<Radnik>();

                foreach (Radnik r in listaKorisnickePodrske)
                {
                    lista.Add(new RadnikView(r));
                }

                s.Close();

            }
            catch
            {

            }
            return lista;
        }

        //public static RadnikTehnickeStrukeView getRadnikKorisnickePodrskeView(int id)
        //{
        //    try
        //    {
        //        ISession s = DataLayer.GetSession();

        //        Radnik rkorisnicke = s.Load<Radnik>(id);

        //        RadnikTehnickeStrukeView rtsv = new RadnikTehnickeStrukeView(rts);

        //        s.Close();

        //        return rtsv;
        //    }
        //    catch
        //    {

        //    }
        //    return null;
        //}

        #endregion

        public static void addSistemAdministrator(int idAdm, string sistem)
        {
            try
            {
                ISession session = DataLayer.GetSession();
                RadnikTehnickeStruke administrator = session.Load<RadnikTehnickeStruke>(idAdm);
                OperativniSistemi os = new OperativniSistemi();
                os.OperativniSistem = sistem;
                os.Administrator = administrator;

                session.Save(os);

                administrator.OperSistemi.Add(os);
                session.Save(administrator);
                session.Flush();
                session.Close();
            }
            catch
            {

            }
        }


        public static void addKontaktKorisnik(int idKorisnika, KontaktOsoba osoba)
        {
            try
            {
                ISession session = DataLayer.GetSession();

                Korisnik korinsik = session.Get<Korisnik>(idKorisnika);
                KontaktOsoba os = new KontaktOsoba();
                os.LicnoIme = osoba.LicnoIme;
                os.Prezime = osoba.Prezime;
                os.BrojTelefona = osoba.BrojTelefona;

                session.Save(os);

                korinsik.KontaktOsobe.Add(os);
                session.Save(korinsik);
                session.Flush();
                session.Close();
            }
            catch
            {

            }
        }




        public static void addOsobaKorisnik(int idKorisnika, KontaktOsoba osoba)
        {
            try
            {
                //
                ISession session = DataLayer.GetSession();
                Korisnik korisnik = session.Get<Korisnik>(idKorisnika);
                KontaktOsoba os = new KontaktOsoba();
                os.BrojTelefona = osoba.BrojTelefona;
                os.KorisnikZaKogPamtimo = korisnik;
                os.LicnoIme = osoba.LicnoIme;
                os.Prezime = osoba.Prezime;

                session.Save(os);

                korisnik.KontaktOsobe.Add(os);
                session.Save(korisnik);
                session.Flush();
                session.Close();
            }
            catch
            {

            }
        }


        public static void addHostujeSe(int idServera, int idPaketa)
        {
            try
            {
                //
                ISession session = DataLayer.GetSession();
                HostujeSe hostuje = new HostujeSe();
                WindowsServer server = session.Get<WindowsServer>(idServera);

                hostuje.HostujeServer = server;

                Paket paket = session.Get<Paket>(idPaketa);
                hostuje.HostujePaket = paket;
                session.Save(hostuje);

                server.Paketi.Add(hostuje.HostujePaket);
                session.Save(server);
                session.Flush();
                session.Close();
            }
            catch
            {

            }
        }





        //ovo sam ja dodao da bih testirao programera i listu programskih jezika
        public static void addJezikProgramer(int idProg, string jezik)
        {
            try
            {
                ISession session = DataLayer.GetSession();
                RadnikTehnickeStruke programer = session.Load<RadnikTehnickeStruke>(idProg);
                ProgramskiJezici os = new ProgramskiJezici();
                os.ProgramskiJezik = jezik;
                os.Programer = programer;

                session.Save(os);

                programer.ProgJezici.Add(os);
                session.Save(programer);
                session.Flush();
                session.Close();
            }
            catch
            {

            }
        }




        public static void deleteSistemAdministrator(int id)
        {
            try
            {
                ISession session = DataLayer.GetSession();

                OperativniSistemi os = session.Load<OperativniSistemi>(id);

                session.Delete(os);

                session.Flush();
                session.Close();
            }
            catch
            {

            }
        }


        public static void deleteKontaktKorisnik(int id)
        {
            try
            {
                ISession session = DataLayer.GetSession();

                KontaktOsoba os = session.Load<KontaktOsoba>(id);

                session.Delete(os);

                session.Flush();
                session.Close();
            }
            catch
            {

            }
        }



        //aj i ovo da napravim mada ne razumem sta ce nam
        public static void deleteJezikProgramer(int id)
        {
            try
            {
                ISession session = DataLayer.GetSession();

                ProgramskiJezici os = session.Load<ProgramskiJezici>(id);

                session.Delete(os);

                session.Flush();
                session.Close();
            }
            catch
            {

            }
        }




        #endregion

        #region Paket

        public static List<Paket> getListaPaket(bool argumenti, string baza, string skripting, string staticne)
        {
            //List<PaketView> lista = new List<PaketView>();
            IList<Paket> listaPaketa = new List<Paket>();
            try
            {
                ISession s = DataLayer.GetSession();

                ISQLQuery q;
                if (argumenti)
                {
                    q = s.CreateSQLQuery("SELECT O.* FROM PAKET O WHERE WHP_SA_BAZOM='" + baza + "' OR WHP_SA_SKRIPTING='" + skripting + "' OR WHP_ZA_STATICNE='" + staticne + "'");
                }
                else
                {

                    q = s.CreateSQLQuery("SELECT * FROM PAKET");
                }
                q.AddEntity(typeof(Paket));


                listaPaketa = q.List<Paket>();
                
                //if (listaPaketa != null)
                //{
                //    foreach (Paket p in listaPaketa)
                //    {
                //        lista.Add(new PaketView(p));
                //    }
                //}

                s.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException.Message);
            }
            return (List<Paket>)listaPaketa;
        }

        public static List<PaketView> getListaPaketView(bool argumenti, string baza, string skripting, string staticne)
        {
            List<PaketView> lista = new List<PaketView>();
            try
            {
                ISession s = DataLayer.GetSession();

                ISQLQuery q;
                if (argumenti)
                {
                    q = s.CreateSQLQuery("SELECT O.* FROM PAKET O WHERE WHP_SA_BAZOM='" + baza + "' AND WHP_SA_SKRIPTING='" + skripting + "' AND WHP_ZA_STATICNE='" + staticne + "'");
                }
                else
                {

                    q = s.CreateSQLQuery("SELECT * FROM PAKET");
                }
                q.AddEntity(typeof(Paket));


                IList<Paket> listaPaketa = q.List<Paket>();

                foreach (Paket p in listaPaketa)
                {
                    lista.Add(new PaketView(p));
                }

                s.Close();

            }
            catch
            {

            }
            return lista;
        }

        public static PaketView getPaketView(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Paket p = s.Load<Paket>(id);

                PaketView pv = new PaketView(p);

                s.Close();

                return pv;
            }
            catch
            {

            }
            return null;
        }

        public static Paket getPaket(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Paket p = s.Load<Paket>(id);

                //PaketView pv = new PaketView(p);

                s.Close();

                return p;
            }
            catch
            {

            }
            return null;
        }

        public static void AddPaket(Paket p)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(p);

                s.Flush();
                s.Close();
            }
            catch
            {

            }
        }

        public static void deletePaket(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Paket p = s.Load<Paket>(id);

                s.Delete(p);

                s.Flush();
                s.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException.Message);
            }
        }

        public static void updatePaket(Paket p)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                Paket paket = s.Load<Paket>(p.Id);
                paket.Cena = p.Cena;
                paket.Prostor_u_mb = p.Prostor_u_mb;
                paket.Whp_sa_bazom = p.Whp_sa_bazom;
                if (String.Compare(p.Whp_sa_bazom, "Da") == 0)
                {
                    paket.Dbms = p.Dbms;
                }
                paket.Whp_sa_skripting = p.Whp_sa_skripting;
                if (String.Compare(paket.Whp_sa_skripting, "Da") == 0)
                {
                    paket.Skripting_jezik = p.Skripting_jezik;
                }
                paket.Whp_za_staticne = p.Whp_za_staticne;

                paket.PripadajuNarudzbinama.Clear();
                if (p.PripadajuNarudzbinama.Count > 0)
                    foreach (Narudzbina n in p.PripadajuNarudzbinama)
                        paket.PripadajuNarudzbinama.Add(n);

                paket.HostujuSeNaServerima.Clear();
                if (p.HostujuSeNaServerima.Count > 0)
                    foreach (WindowsServer server in p.HostujuSeNaServerima)
                        paket.HostujuSeNaServerima.Add(server);

                paket.OdrzavajuRadnici.Clear();
                if (p.OdrzavajuRadnici.Count > 0)
                    foreach (RadnikTehnickeStruke r in p.OdrzavajuRadnici)
                        paket.OdrzavajuRadnici.Add(r);

                s.SaveOrUpdate(paket);

                s.Flush();
                s.Close();
            }
            catch
            {

            }
        }

        #endregion

        #region Serveri

        public static List<Server> getListaServer()
        {
            IList<Server> listaServera = new List<Server>();
            try
            {
                ISession s = DataLayer.GetSession();

                ISQLQuery q;

                q = s.CreateSQLQuery("SELECT * FROM SERVER");

                q.AddEntity(typeof(Server));

                listaServera = q.List<Server>();

                s.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException.Message);
            }
            return (List<Server>)listaServera;
        }

        public static List<ServerView> getListaServerView(bool argument, string tip)
        {
            List<ServerView> lista = new List<ServerView>();
            try
            {
                ISession s = DataLayer.GetSession();

                ISQLQuery q;

                if (argument)
                    q = s.CreateSQLQuery("SELECT O.* FROM SERVER O WHERE OS_TIP='S_WINDOWS'");
                else
                    q = s.CreateSQLQuery("SELECT * FROM SERVER");
                q.AddEntity(typeof(Server));


                IList<Server> listaServera = q.List<Server>();

                if (listaServera != null)
                {
                    foreach (Server server in listaServera)
                    {
                        lista.Add(new ServerView(server));
                    }
                }

                s.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException.Message);
            }
            return lista;
        }

        public static ServerView getServerView(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Server p = s.Get<Server>(id);

                if (p is WindowsServer)
                    p.OS_tip = "S_WINDOWS";
                else
                    p.OS_tip = "S_LINUX";

                ServerView pv = new ServerView(p);

                s.Close();

                return pv;
            }
            catch
            {

            }
            return null;
        }

        public static WindowsServerView getWindowsServerView(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                WindowsServer p = s.Get<WindowsServer>(id);
                //foreach (Paket paket in p.Paketi)
                //    MessageBox.Show(paket.Cena.ToString());
                WindowsServerView pv = new WindowsServerView(p);

                s.Close();

                return pv;
            }
            catch
            {

            }
            return null;
        }

        public static LinuxServerView getLinuxServerView(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                LinuxServer p = s.Get<LinuxServer>(id);

                LinuxServerView pv = new LinuxServerView(p);

                s.Close();

                return pv;
            }
            catch
            {

            }
            return null;
        }

        public static void addWindowsServer(WindowsServer ws)
        {
            try
            {
                ISession s = DataLayer.GetSession();


                //WindowsServer server = new WindowsServer();

                //server.IP_adresa = "192.168.10.10";

                s.Save(ws);

                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }


        public static void addLinuxServer(LinuxServer ws)
        {
            try
            {
                ISession s = DataLayer.GetSession();


                //WindowsServer server = new WindowsServer();

                //server.IP_adresa = "192.168.10.10";

                s.Save(ws);

                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }



        #endregion

        #region Narudzbine

        public static void AddNarudzbinu(Narudzbina narudzbina)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(narudzbina);

                s.Flush();
                s.Close();

            }
            catch
            {

            }
        }

        public static List<NarudzbinaView> getListaNarudzbinaView(bool argument, int idKupca)
        {
            List<NarudzbinaView> lista = new List<NarudzbinaView>();
            try
            {
                ISession s = DataLayer.GetSession();

                ISQLQuery q;

                if (argument)
                    q = s.CreateSQLQuery("SELECT O.* FROM NARUDZBINA O WHERE ID_KORISNIKA=" + idKupca.ToString());
                else
                    q = s.CreateSQLQuery("SELECT * FROM NARUDZBINA");
                q.AddEntity(typeof(Narudzbina));


                IList<Narudzbina> listaNarudzbina = q.List<Narudzbina>();

                if (listaNarudzbina != null)
                {
                    foreach (Narudzbina n in listaNarudzbina)
                    {
                        lista.Add(new NarudzbinaView(n));
                    }
                }

                s.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException.Message);
            }
            return lista;
        }

        #endregion

        #region Odrzavanje paketa

        public static List<OdrzavaPaketeView> getListaOdrzavaPaketeView(bool argument, int idRadnika)
        {
            List<OdrzavaPaketeView> lista = new List<OdrzavaPaketeView>();
            try
            {
                ISession s = DataLayer.GetSession();

                ISQLQuery q;

                if (argument)
                    q = s.CreateSQLQuery("SELECT O.* FROM ODRZAVA_PAKETE O WHERE ID_RADNIKA=" + idRadnika.ToString());
                else
                    q = s.CreateSQLQuery("SELECT * FROM ODRZAVA_PAKETE");
                q.AddEntity(typeof(OdrzavaPakete));


                IList<OdrzavaPakete> listaOdrzavanja = q.List<OdrzavaPakete>();

                if (listaOdrzavanja != null)
                {
                    foreach (OdrzavaPakete n in listaOdrzavanja)
                    {
                        lista.Add(new OdrzavaPaketeView(n));
                    }
                }

                s.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException.Message);
            }
            return lista;
        }

        #endregion

        #region Odrzavanje servera

        public static List<OdrzavaServereView> getListaOdrzavaServereView(bool argument, int idRadnika)
        {
            List<OdrzavaServereView> lista = new List<OdrzavaServereView>();
            try
            {
                ISession s = DataLayer.GetSession();

                ISQLQuery q;

                if (argument)
                    q = s.CreateSQLQuery("SELECT O.* FROM ODRZAVA_SERVERE O WHERE ID_RADNIKA=" + idRadnika.ToString());
                else
                    q = s.CreateSQLQuery("SELECT * FROM ODRZAVA_SERVERE");
                q.AddEntity(typeof(OdrzavaServere));


                IList<OdrzavaServere> listaOdrzavanja = q.List<OdrzavaServere>();

                if (listaOdrzavanja != null)
                {
                    foreach (OdrzavaServere n in listaOdrzavanja)
                    {
                        lista.Add(new OdrzavaServereView(n));
                    }
                }

                s.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException.Message);
            }
            return lista;
        }

        #endregion

        #region Hostovanje

        public static List<HostujeSeView> getListaHostujeSeView(bool argument, int idPaketa)
        {
            List<HostujeSeView> lista = new List<HostujeSeView>();
            try
            {
                ISession s = DataLayer.GetSession();

                ISQLQuery q;

                if (argument)
                    q = s.CreateSQLQuery("SELECT O.* FROM HOSTUJE_SE O WHERE ID_RADNIKA=" + idPaketa.ToString());
                else
                    q = s.CreateSQLQuery("SELECT * FROM HOSTUJE_SE");
                q.AddEntity(typeof(HostujeSe));


                IList<HostujeSe> listaHostovanja = q.List<HostujeSe>();

                if (listaHostovanja != null)
                {
                    foreach (HostujeSe n in listaHostovanja)
                    {
                        lista.Add(new HostujeSeView(n));
                    }
                }

                s.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException.Message);
            }
            return lista;
        }

        #endregion

    }
}
