﻿using NHibernate;
using WebHosting.Entiteti;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Linq;
using WebHosting.DTOs;

namespace WebHosting
{
    public class DataProvider
    {
        #region Radnici

        #region EkonomistaTehnickePodrske

        //public IEnumerable<EkonomistaTehnickePodrske> GetEkonomistiTehnickePodrske()
        //{
        //    ISession s = DataLayer.GetSession();

        //    IEnumerable<EkonomistaTehnickePodrske> ekonomisti = s.Query<EkonomistaTehnickePodrske>()
        //                                        .Select(p => p);
        //    return ekonomisti;
        //}

        public IEnumerable<EkonomistaTehnickePodrskeView> GetEkonomistiTehnickePodrskeView()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<EkonomistaTehnickePodrske> ekonomisti = s.Query<EkonomistaTehnickePodrske>()
                                                .Select(p => p);
            List<EkonomistaTehnickePodrskeView> rez = new List<EkonomistaTehnickePodrskeView>();
            if (ekonomisti != null)
                foreach (EkonomistaTehnickePodrske ek in ekonomisti)
                    rez.Add(new EkonomistaTehnickePodrskeView(ek));

            IEnumerable<EkonomistaTehnickePodrskeView> vrati = rez;

            return vrati;

            //Ne zatvaramo sesiju?
        }

        //public EkonomistaTehnickePodrske GetEkonomistaTehnickePodrske(int id)
        //{
        //    ISession s = DataLayer.GetSession();

        //    return s.Query<EkonomistaTehnickePodrske>()
        //        .Where(v => v.Id == id).Select(p => p).FirstOrDefault();
        //}

        public EkonomistaTehnickePodrskeView GetEkonomistaTehnickePodrskeView(int id)
        {
            ISession s = DataLayer.GetSession();

            EkonomistaTehnickePodrske eko = s.Query<EkonomistaTehnickePodrske>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();

            if (eko == null) return new EkonomistaTehnickePodrskeView();

            return new EkonomistaTehnickePodrskeView(eko);
        }

        public int AddEkonomistaTehnickePodrske(EkonomistaTehnickePodrskeView e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                EkonomistaTehnickePodrske eko = new EkonomistaTehnickePodrske();
                OdradiDodeluRadnik(eko, e);
                eko.Stepen_strucne_spreme = e.Stepen_strucne_spreme;

                s.Save(eko);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public int RemoveEkonomistaTehnickePodrske(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                EkonomistaTehnickePodrske e = s.Load<EkonomistaTehnickePodrske>(id);

                s.Delete(e);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception exc)
            {
                return -1;
            }

        }
        public void OdradiDodeluRadnik(Radnik r, RadnikView rv)
        {
            r.Ime_oca = rv.Ime_oca;
            r.Licno_ime = rv.Licno_ime;
            r.Prezime = rv.Prezime;
            r.Jmbg = rv.Jmbg;
            r.Datum_isteka_ugovora = rv.Datum_isteka_ugovora;
            r.Datum_zaposlenja = rv.DatumZaposlenja;
            r.Godina_rodjenja = rv.Godina_rodjenja;
            r.Godine_radnog_staza = rv.Godine_radnog_staza;
            
        }
        public int UpdateEkonomistaTehnickePodrske(int id, EkonomistaTehnickePodrskeView e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                EkonomistaTehnickePodrske eko = s.Load<EkonomistaTehnickePodrske>(id);

                OdradiDodeluRadnik(eko, e);
                eko.Stepen_strucne_spreme = e.Stepen_strucne_spreme;

                s.Save(eko);
                s.Flush();
                s.Close();
                return 1;
            }
            catch (Exception exc)
            {
                
                return -1;
            }
        }

        #endregion

        #region RadnikTehnickeStruke

        public IEnumerable<RadnikTehnickeStrukeView> GetRadnikTehnickeStrukeView(string pozicija)
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<RadnikTehnickeStruke> radnici = s.Query<RadnikTehnickeStruke>()
                                                .Where(v=>v.Pozicija.Equals(pozicija)).Select(p => p);
            List<RadnikTehnickeStrukeView> rez = new List<RadnikTehnickeStrukeView>();
            if (radnici != null)
                foreach (RadnikTehnickeStruke ek in radnici)
                        rez.Add(new RadnikTehnickeStrukeView(ek));

            IEnumerable<RadnikTehnickeStrukeView> vrati = rez;

            return vrati;
        }


        public RadnikTehnickeStrukeView GetRadnikTehnickeStrukeView(int id, string pozicija)
        {
            ISession s = DataLayer.GetSession();

            RadnikTehnickeStruke rad = s.Query<RadnikTehnickeStruke>()
                .Where(v => (v.Id == id) && (v.Pozicija.Equals(pozicija))).Select(p => p).FirstOrDefault();

            if (rad == null) return new RadnikTehnickeStrukeView();

            return new RadnikTehnickeStrukeView(rad);
        }

        public int AddRadnikTehnickeStruke(RadnikTehnickeStrukeView r)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                RadnikTehnickeStruke rad = new RadnikTehnickeStruke();
                OdradiDodeluRadnik(rad, r);
                rad.Pozicija = "Administrator";//!!!vrati se

                s.Save(rad);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public int RemoveRadnikTehnickeStruke(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                RadnikTehnickeStruke r = s.Load<RadnikTehnickeStruke>(id);

                s.Delete(r);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception exc)
            {
                return -1;
            }

        }
        public int UpdateRadnikTehnickeStruke(int id, RadnikTehnickeStrukeView r)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                RadnikTehnickeStruke rad = s.Load<RadnikTehnickeStruke>(id);

                OdradiDodeluRadnik(rad, r);

                s.Save(rad);
                s.Flush();
                s.Close();
                return 1;
            }
            catch (Exception exc)
            {

                return -1;
            }
        }

        #endregion

        #region RadnikKorisnickePodrske

        public IEnumerable<RadnikView> GetRadniciKorisnickePodrskeView()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<Radnik> radnici = s.Query<Radnik>()
                                                .Where(v => v.SpecijalnostRadnika.Equals("Radnik_korisnicke_podrske")).Select(p => p);
            List<RadnikView> rez = new List<RadnikView>();
            if (radnici != null)
                foreach (Radnik ek in radnici)
                    rez.Add(new RadnikView(ek));

            IEnumerable<RadnikView> vrati = rez;

            return vrati;
        }


        public RadnikView GetRadnikKorisnickePodrskeView(int id)
        {
            ISession s = DataLayer.GetSession();

            Radnik rad = s.Query<Radnik>()
                .Where(v => (v.Id == id) && (v.SpecijalnostRadnika.Equals("Radnik_korisnicke_podrske"))).Select(p => p).FirstOrDefault();

            if (rad == null) return new RadnikView();

            return new RadnikView(rad);
        }

        public int AddRadnikKorisnickePodrske(RadnikView r)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Radnik rad = new Radnik();
                OdradiDodeluRadnik(rad, r);

                s.Save(r);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public int RemoveRadnikKorisnickePodrske(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Radnik r = s.Get<Radnik>(id);

                s.Delete(r);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception exc)
            {
                return -1;
            }

        }

        public int UpdateRadnikKorisnickePodrske(int id, RadnikView r)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Radnik rad = s.Get<Radnik>(id);

                OdradiDodeluRadnik(rad, r);

                s.Save(rad);
                s.Flush();
                s.Close();
                return 1;
            }
            catch (Exception exc)
            {

                return -1;
            }
        }

        #endregion

        #endregion

        #region Korisnici

        #region PravnoLice

        public IEnumerable<PravnoLiceView> GetPravnaLicaView()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<PravnoLice> korisnici = s.Query<PravnoLice>()
                                                .Select(p => p);
            List<PravnoLiceView> rez = new List<PravnoLiceView>();
            if (korisnici != null)
                foreach (PravnoLice ek in korisnici)
                    rez.Add(new PravnoLiceView(ek));

            IEnumerable<PravnoLiceView> vrati = rez;

            return vrati;
        }

        public PravnoLiceView GetPravnoLiceView(int id)
        {
            ISession s = DataLayer.GetSession();

            PravnoLice prl = s.Query<PravnoLice>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();

            if (prl == null) return new PravnoLiceView();

            return new PravnoLiceView(prl);
        }

        public int AddPravnoLice(PravnoLiceView p)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                PravnoLice prl = new PravnoLice();
                OdradiDodeluPravnoLice(prl, p);

                s.Save(prl);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public int RemovePravnoLice(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                PravnoLice p = s.Get<PravnoLice>(id);

                s.Delete(p);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception exc)
            {
                return -1;
            }

        }
        public void OdradiDodeluPravnoLice(PravnoLice prl, PravnoLiceView prlv)
        {
            prl.Adresa = prlv.Adresa;
            prl.PIB = prlv.PIB;
            prl.Naziv = prlv.Naziv;
            //ako bi se dodeljivao i ekonomista s kojim koresponduje, moralo bi ovde da se napravi novi
            //i da se redom iskopiraju atributi
        }
        public int UpdatePravnoLice(int id, PravnoLiceView p)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                PravnoLice prl = s.Get<PravnoLice>(id);

                OdradiDodeluPravnoLice(prl, p);

                s.Save(prl);
                s.Flush();
                s.Close();
                return 1;
            }
            catch (Exception exc)
            {

                return -1;
            }
        }

        #endregion

        #region FizickoLice

        public IEnumerable<FizickoLiceView> GetFizickaLicaView()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<FizickoLice> korisnici = s.Query<FizickoLice>()
                                                .Select(p => p);
            List<FizickoLiceView> rez = new List<FizickoLiceView>();
            if (korisnici != null)
                foreach (FizickoLice ek in korisnici)
                    rez.Add(new FizickoLiceView(ek));

            IEnumerable<FizickoLiceView> vrati = rez;

            return vrati;
        }

        public FizickoLiceView GetFizickoLiceView(int id)
        {
            ISession s = DataLayer.GetSession();

            FizickoLice fzl = s.Query<FizickoLice>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();

            if (fzl == null) return new FizickoLiceView();

            return new FizickoLiceView(fzl);
        }

        public int AddFizickoLice(FizickoLiceView f)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                FizickoLice fzl = new FizickoLice();
                OdradiDodeluFizickoLice(fzl, f);

                s.Save(fzl);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public int RemoveFizickoLice(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                FizickoLice f = s.Get<FizickoLice>(id);

                s.Delete(f);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception exc)
            {
                return -1;
            }

        }
        public void OdradiDodeluFizickoLice(FizickoLice fzl, FizickoLiceView fzlv)
        {
            fzl.Adresa = fzlv.Adresa;
            fzl.JMBG = fzlv.JMBG;
            fzl.LicnoIme = fzlv.LicnoIme;
            fzl.Prezime = fzlv.Prezime;
            //ako bi se dodeljivao i ekonomista s kojim koresponduje, moralo bi ovde da se napravi novi
            //i da se redom iskopiraju atributi
        }
        public int UpdateFizickoLice(int id, FizickoLiceView f)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                FizickoLice fzl = s.Get<FizickoLice>(id);

                OdradiDodeluFizickoLice(fzl, f);

                s.Save(fzl);
                s.Flush();
                s.Close();
                return 1;
            }
            catch (Exception exc)
            {

                return -1;
            }
        }

        #endregion

        #region KontaktOsoba

        public IEnumerable<KontaktOsobaView> GetKontaktOsobe()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<KontaktOsoba> osobe = s.Query<KontaktOsoba>()
                                                .Select(p => p);

            List<KontaktOsobaView> rez = new List<KontaktOsobaView>();
            if (osobe != null)
                foreach (KontaktOsoba ek in osobe)
                    rez.Add(new KontaktOsobaView(ek));

            IEnumerable<KontaktOsobaView> vrati = rez;

            return vrati;
        }

        public KontaktOsoba GetKontaktOsoba(int id)
        {
            ISession s = DataLayer.GetSession();

            KontaktOsoba kos = s.Query<KontaktOsoba>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();

            return kos;
        }

        public KontaktOsobaView GetKontaktOsobaView(int id)
        {
            ISession s = DataLayer.GetSession();

            KontaktOsoba kos = s.Query<KontaktOsoba>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();

            if (kos == null) return new KontaktOsobaView();

            return new KontaktOsobaView(kos);
        
        }

        public int AddKontaktOsoba(KontaktOsoba k)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                //KontaktOsoba kos = new KontaktOsoba();
                //OdradiDodeluKontaktOsoba(kos, k);

                s.Save(k);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public int RemoveKontaktOsoba(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                KontaktOsoba k = s.Get<KontaktOsoba>(id);

                s.Delete(k);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception exc)
            {
                return -1;
            }

        }

        public void OdradiDodeluKontaktOsoba(KontaktOsoba kos, KontaktOsobaView kosv)
        {
            kos.LicnoIme = kosv.Licno_ime;
            kos.Prezime = kosv.Prezime;
            kos.BrojTelefona = kosv.Broj_telefona;
        }
        public int UpdateKontaktOsoba(int id, KontaktOsobaView k)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                KontaktOsoba kos = s.Get<KontaktOsoba>(id);

                OdradiDodeluKontaktOsoba(kos, k);

                s.Save(kos);
                s.Flush();
                s.Close();
                return 1;
            }
            catch (Exception exc)
            {

                return -1;
            }
        }

        #endregion

        #endregion

        #region Paket

        public IEnumerable<PaketView> GetPaketi(string baza, string skripting, string staticne)
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<Paket> paketi = s.Query<Paket>()
                                                .Where(v=>(v.Whp_sa_skripting.Equals(skripting)&&v.Whp_sa_bazom.Equals(baza)&&v.Whp_za_staticne.Equals(staticne))).Select(p => p);

            List<PaketView> rez = new List<PaketView>();
            if (paketi != null)
                foreach (Paket ek in paketi)
                    rez.Add(new PaketView(ek));

            IEnumerable<PaketView> vrati = rez;

            return vrati;
        }

        public Paket GetPaket(int id)
        {
            ISession s = DataLayer.GetSession();

            return s.Query<Paket>()
                .Where(d => d.Id == id).Select(p => p).FirstOrDefault();
        }

        public PaketView GetPaketView(int id)
        {
            ISession s = DataLayer.GetSession();

            Paket pk = s.Query<Paket>()
                .Where(x => x.Id == id).Select(p => p).FirstOrDefault();

            if (pk == null) return new PaketView();

            return new PaketView(pk);
        }

        public void OdradiDodeluPaket(Paket pak, PaketView pakv)
        {
            pak.Cena = pakv.Cena;
            pak.Prostor_u_mb = pakv.Prostor_u_mb;
            pak.Whp_sa_bazom = pakv.Whp_sa_bazom;
            pak.Dbms = pakv.Dbms;
            pak.Whp_sa_skripting = pakv.Whp_sa_skripting;
            pak.Skripting_jezik = pakv.Skripting_jezik;
            pak.Whp_za_staticne = pakv.Whp_za_staticne;
        }

        public int AddPaket(PaketView p)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Paket pak = new Paket();
                OdradiDodeluPaket(pak, p);

                s.Save(pak);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public int RemovePaket(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Paket p = s.Load<Paket>(id);

                s.Delete(p);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception exc)
            {
                return -1;
            }

        }
        public int UpdatePaket(int id, PaketView p)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Paket pak = s.Get<Paket>(id);

                OdradiDodeluPaket(pak, p);

                s.Save(pak);
                s.Flush();
                s.Close();
                return 1;
            }
            catch (Exception exc)
            {
                return -1;
            }
        }

        #endregion

        #region Narudzbina

        public IEnumerable<NarudzbinaView> GetNarudzbineView()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<Narudzbina> narudzbine = s.Query<Narudzbina>()
                                                .Select(p => p);

            List<NarudzbinaView> rez = new List<NarudzbinaView>();
            if (narudzbine != null)
                foreach (Narudzbina nar in narudzbine)
                    rez.Add(new NarudzbinaView(nar));

            IEnumerable<NarudzbinaView> vrati = rez;

            return vrati;
        }

        public NarudzbinaView GetNarudzbinaView(int id)
        {
            ISession s = DataLayer.GetSession();

            Narudzbina nar = s.Query<Narudzbina>()
                .Where(x => x.Id == id).Select(p => p).FirstOrDefault();

            if (nar == null) return new NarudzbinaView();

            return new NarudzbinaView(nar);
        }

        public void OdradiDodeluNarudzbina(Narudzbina nar, NarudzbinaView narv)
        {
            nar.Datum = narv.Datum;
            nar.TrajanjeMeseci = narv.TrajanjeMeseci;
            //nar.KupujeNarudzbinu = new Korisnik();

            //nar.PosedujePaket = new Paket();
            //nar.PosedujePaket.Cena = narv.CenaPaketa;
            //nar.PosedujePaket.Prostor_u_mb = narv.ProstorKojiZauzimaPaket;

            //Pitanje da li treba ovo?

        }

        public int AddNarudzbina(Narudzbina n)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(n);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public int RemoveNarudzbina(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Narudzbina n = s.Get<Narudzbina>(id);

                s.Delete(n);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception exc)
            {
                return -1;
            }

        }
        public int UpdateNarudzbina(int id, NarudzbinaView n)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Narudzbina nar = s.Get<Narudzbina>(id);

                OdradiDodeluNarudzbina(nar, n);

                s.Save(nar);
                s.Flush();
                s.Close();
                return 1;
            }
            catch (Exception exc)
            {
                return -1;
            }
        }

        #endregion

        #region Server

        public IEnumerable<ServerView> GetServeriView(string tip)
        {
            ISession s = DataLayer.GetSession();
            
            IEnumerable<Server> serveri;
            if (tip.Equals("windows"))
            {
                serveri = s.Query<WindowsServer>()
                                                .Select(p => p);
                List<WindowsServerView> rez = new List<WindowsServerView>();
                if (serveri != null)
                    foreach (WindowsServer ser in serveri)
                        rez.Add(new WindowsServerView(ser));

                IEnumerable<WindowsServerView> vrati = rez;

                return vrati;
            }
            else if (tip.Equals("linux"))
            {
                serveri = s.Query<LinuxServer>()
                                                .Select(p => p);
                List<LinuxServerView> rez = new List<LinuxServerView>();
                if (serveri != null)
                    foreach (LinuxServer ser in serveri)
                        rez.Add(new LinuxServerView(ser));

                IEnumerable<LinuxServerView> vrati = rez;

                return vrati;
            }
            else
                return null;
        }

        public ServerView GetServerView(int id, string tip)
        {
            ISession s = DataLayer.GetSession();

            if (tip.Equals("windows"))
            {
                WindowsServer ser = s.Query<WindowsServer>()
                    .Where(v => (v.Id == id)).Select(p => p).FirstOrDefault();

                if (ser == null) return new WindowsServerView();

                return new WindowsServerView(ser);
            }
            else if(tip.Equals("linux"))
            {
                LinuxServer ser = s.Query<LinuxServer>()
                    .Where(v => (v.Id == id)).Select(p => p).FirstOrDefault();

                if (ser == null) return new LinuxServerView();

                return new LinuxServerView(ser);
            }
            else
            {
                return null;
            }
        }

        public void OdradiDodeluServer(Server ser, ServerView serv)
        {
            ser.IP_adresa = serv.IP_adresa;
        }

        public int AddServer(ServerView r, string tip)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                if (tip.Equals("windows"))
                {
                    WindowsServer ser = new WindowsServer();
                    OdradiDodeluServer(ser, r);
                    
                    s.Save(ser);
                }
                else
                {
                    //neka bude da se podrazumevano kreira linux server
                    LinuxServer ser = new LinuxServer();
                    OdradiDodeluServer(ser, r);

                    s.Save(ser);
                }
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public int RemoveServer(int id, string tip)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                if (tip.Equals("windows"))
                {
                    WindowsServer r = s.Get<WindowsServer>(id);
                    s.Delete(r);
                }
                else if(tip.Equals("linux"))
                {
                    LinuxServer r = s.Get<LinuxServer>(id);
                    s.Delete(r);
                }

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception exc)
            {
                return -1;
            }

        }
        public int UpdateServer(int id, ServerView r, string tip)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                if(tip.Equals("windows"))
                {
                    WindowsServer ser = s.Get<WindowsServer>(id);
                    OdradiDodeluServer(ser, r);
                    s.Save(ser);
                }
                else
                {
                    //podrazumevano
                    LinuxServer ser = s.Get<LinuxServer>(id);
                    OdradiDodeluServer(ser, r);
                    s.Save(ser);
                }

                s.Flush();
                s.Close();
                return 1;
            }
            catch (Exception exc)
            {

                return -1;
            }
        }

        #endregion

        #region Odrzavanje

        #region OdrzavaPakete

        public IEnumerable<OdrzavaPaketeView> GetOdrzavaPaketeListaView()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<OdrzavaPakete> odrzavanja = s.Query<OdrzavaPakete>()
                                                .Select(p => p);

            List<OdrzavaPaketeView> rez = new List<OdrzavaPaketeView>();
            if (odrzavanja != null)
                foreach (OdrzavaPakete odr in odrzavanja)
                    rez.Add(new OdrzavaPaketeView(odr));

            IEnumerable<OdrzavaPaketeView> vrati = rez;

            return vrati;
        }

        public OdrzavaPaketeView GetOdrzavaPaketeView(int id)
        {
            ISession s = DataLayer.GetSession();

            OdrzavaPakete odr = s.Query<OdrzavaPakete>()
                .Where(x => x.Id == id).Select(p => p).FirstOrDefault();

            if (odr == null) return new OdrzavaPaketeView();

            return new OdrzavaPaketeView(odr);
        }

        #endregion

        #region OdrzavaServere

        public IEnumerable<OdrzavaServereView> GetOdrzavaServereListaView()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<OdrzavaServere> odrzavanja = s.Query<OdrzavaServere>()
                                                .Select(p => p);

            List<OdrzavaServereView> rez = new List<OdrzavaServereView>();
            if (odrzavanja != null)
                foreach (OdrzavaServere odr in odrzavanja)
                    rez.Add(new OdrzavaServereView(odr));

            IEnumerable<OdrzavaServereView> vrati = rez;

            return vrati;
        }

        public OdrzavaServereView GetOdrzavaServereView(int id)
        {
            ISession s = DataLayer.GetSession();

            OdrzavaServere odr = s.Query<OdrzavaServere>()
                .Where(x => x.Id == id).Select(p => p).FirstOrDefault();

            if (odr == null) return new OdrzavaServereView();

            return new OdrzavaServereView(odr);
        }

        #endregion

        #endregion

        #region Hostovanja

        public IEnumerable<HostujeSeView> GetHostujeSeListaView()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<HostujeSe> hostovanja = s.Query<HostujeSe>()
                                                .Select(p => p);

            List<HostujeSeView> rez = new List<HostujeSeView>();
            if (hostovanja != null)
                foreach (HostujeSe odr in hostovanja)
                    rez.Add(new HostujeSeView(odr));

            IEnumerable<HostujeSeView> vrati = rez;

            return vrati;
        }

        public HostujeSeView GetHostujeSeView(int id)
        {
            ISession s = DataLayer.GetSession();

            HostujeSe odr = s.Query<HostujeSe>()
                .Where(x => x.Id == id).Select(p => p).FirstOrDefault();

            if (odr == null) return new HostujeSeView();

            return new HostujeSeView(odr);
        }

        #endregion
    }
}
