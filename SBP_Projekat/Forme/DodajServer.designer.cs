﻿namespace WebHosting.Forme
{
    partial class DodajServer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnOdustani = new MetroFramework.Controls.MetroButton();
            this.btnDodajServer = new MetroFramework.Controls.MetroButton();
            this.gridDodatiPaketi = new MetroFramework.Controls.MetroGrid();
            this.labelPostojeciPaketi = new MetroFramework.Controls.MetroLabel();
            this.labelPaketiKojiSeHostujuNaS = new MetroFramework.Controls.MetroLabel();
            this.gridPostojeciBez = new MetroFramework.Controls.MetroGrid();
            this.radioWindows = new MetroFramework.Controls.MetroRadioButton();
            this.radioLinux = new MetroFramework.Controls.MetroRadioButton();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.txtIPadresa = new MetroFramework.Controls.MetroTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.gridDodatiPaketi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPostojeciBez)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOdustani
            // 
            this.btnOdustani.Location = new System.Drawing.Point(631, 437);
            this.btnOdustani.Name = "btnOdustani";
            this.btnOdustani.Size = new System.Drawing.Size(75, 23);
            this.btnOdustani.TabIndex = 42;
            this.btnOdustani.Text = "Odustani";
            this.btnOdustani.UseSelectable = true;
            this.btnOdustani.Click += new System.EventHandler(this.btnOdustani_Click);
            // 
            // btnDodajServer
            // 
            this.btnDodajServer.Location = new System.Drawing.Point(712, 437);
            this.btnDodajServer.Name = "btnDodajServer";
            this.btnDodajServer.Size = new System.Drawing.Size(75, 23);
            this.btnDodajServer.TabIndex = 41;
            this.btnDodajServer.Text = "Dodaj server";
            this.btnDodajServer.UseSelectable = true;
            this.btnDodajServer.Click += new System.EventHandler(this.BtnDodajServer_Click);
            // 
            // gridDodatiPaketi
            // 
            this.gridDodatiPaketi.AllowUserToResizeRows = false;
            this.gridDodatiPaketi.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.gridDodatiPaketi.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gridDodatiPaketi.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.gridDodatiPaketi.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridDodatiPaketi.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gridDodatiPaketi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridDodatiPaketi.DefaultCellStyle = dataGridViewCellStyle2;
            this.gridDodatiPaketi.EnableHeadersVisualStyles = false;
            this.gridDodatiPaketi.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.gridDodatiPaketi.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.gridDodatiPaketi.Location = new System.Drawing.Point(420, 193);
            this.gridDodatiPaketi.Name = "gridDodatiPaketi";
            this.gridDodatiPaketi.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridDodatiPaketi.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.gridDodatiPaketi.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gridDodatiPaketi.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridDodatiPaketi.Size = new System.Drawing.Size(367, 210);
            this.gridDodatiPaketi.TabIndex = 40;
            // 
            // labelPostojeciPaketi
            // 
            this.labelPostojeciPaketi.AutoSize = true;
            this.labelPostojeciPaketi.Location = new System.Drawing.Point(41, 165);
            this.labelPostojeciPaketi.Name = "labelPostojeciPaketi";
            this.labelPostojeciPaketi.Size = new System.Drawing.Size(103, 19);
            this.labelPostojeciPaketi.TabIndex = 39;
            this.labelPostojeciPaketi.Text = "Postojeci paketi:";
            // 
            // labelPaketiKojiSeHostujuNaS
            // 
            this.labelPaketiKojiSeHostujuNaS.AutoSize = true;
            this.labelPaketiKojiSeHostujuNaS.Location = new System.Drawing.Point(420, 165);
            this.labelPaketiKojiSeHostujuNaS.Name = "labelPaketiKojiSeHostujuNaS";
            this.labelPaketiKojiSeHostujuNaS.Size = new System.Drawing.Size(196, 19);
            this.labelPaketiKojiSeHostujuNaS.TabIndex = 38;
            this.labelPaketiKojiSeHostujuNaS.Text = "Paketi koji se hostuju na serveru:";
            // 
            // gridPostojeciBez
            // 
            this.gridPostojeciBez.AllowUserToResizeRows = false;
            this.gridPostojeciBez.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.gridPostojeciBez.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gridPostojeciBez.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.gridPostojeciBez.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridPostojeciBez.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.gridPostojeciBez.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridPostojeciBez.DefaultCellStyle = dataGridViewCellStyle5;
            this.gridPostojeciBez.EnableHeadersVisualStyles = false;
            this.gridPostojeciBez.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.gridPostojeciBez.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.gridPostojeciBez.Location = new System.Drawing.Point(41, 193);
            this.gridPostojeciBez.Name = "gridPostojeciBez";
            this.gridPostojeciBez.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridPostojeciBez.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.gridPostojeciBez.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gridPostojeciBez.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridPostojeciBez.Size = new System.Drawing.Size(357, 210);
            this.gridPostojeciBez.TabIndex = 37;
            this.gridPostojeciBez.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridPostojeciBez_CellContentDoubleClick);
            this.gridPostojeciBez.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.GridPostojeciBez_CellMouseDoubleClick);
            // 
            // radioWindows
            // 
            this.radioWindows.AutoSize = true;
            this.radioWindows.Location = new System.Drawing.Point(41, 135);
            this.radioWindows.Name = "radioWindows";
            this.radioWindows.Size = new System.Drawing.Size(106, 15);
            this.radioWindows.TabIndex = 36;
            this.radioWindows.Text = "Windows server";
            this.radioWindows.UseSelectable = true;
            this.radioWindows.CheckedChanged += new System.EventHandler(this.radioWindows_CheckedChanged);
            // 
            // radioLinux
            // 
            this.radioLinux.AutoSize = true;
            this.radioLinux.Location = new System.Drawing.Point(41, 114);
            this.radioLinux.Name = "radioLinux";
            this.radioLinux.Size = new System.Drawing.Size(85, 15);
            this.radioLinux.TabIndex = 35;
            this.radioLinux.Text = "Linux server";
            this.radioLinux.UseSelectable = true;
            this.radioLinux.CheckedChanged += new System.EventHandler(this.radioLinux_CheckedChanged);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(41, 66);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(66, 19);
            this.metroLabel4.TabIndex = 33;
            this.metroLabel4.Text = "Ip adresa:";
            // 
            // txtIPadresa
            // 
            // 
            // 
            // 
            this.txtIPadresa.CustomButton.Image = null;
            this.txtIPadresa.CustomButton.Location = new System.Drawing.Point(181, 1);
            this.txtIPadresa.CustomButton.Name = "";
            this.txtIPadresa.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtIPadresa.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtIPadresa.CustomButton.TabIndex = 1;
            this.txtIPadresa.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtIPadresa.CustomButton.UseSelectable = true;
            this.txtIPadresa.CustomButton.Visible = false;
            this.txtIPadresa.Lines = new string[0];
            this.txtIPadresa.Location = new System.Drawing.Point(212, 66);
            this.txtIPadresa.MaxLength = 32767;
            this.txtIPadresa.Name = "txtIPadresa";
            this.txtIPadresa.PasswordChar = '\0';
            this.txtIPadresa.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtIPadresa.SelectedText = "";
            this.txtIPadresa.SelectionLength = 0;
            this.txtIPadresa.SelectionStart = 0;
            this.txtIPadresa.ShortcutsEnabled = true;
            this.txtIPadresa.Size = new System.Drawing.Size(203, 23);
            this.txtIPadresa.TabIndex = 34;
            this.txtIPadresa.UseSelectable = true;
            this.txtIPadresa.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtIPadresa.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // DodajServer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(804, 477);
            this.Controls.Add(this.btnOdustani);
            this.Controls.Add(this.btnDodajServer);
            this.Controls.Add(this.gridDodatiPaketi);
            this.Controls.Add(this.labelPostojeciPaketi);
            this.Controls.Add(this.labelPaketiKojiSeHostujuNaS);
            this.Controls.Add(this.gridPostojeciBez);
            this.Controls.Add(this.radioWindows);
            this.Controls.Add(this.radioLinux);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.txtIPadresa);
            this.Name = "DodajServer";
            this.Text = "DodajServer";
            this.Load += new System.EventHandler(this.DodajServer_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridDodatiPaketi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPostojeciBez)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroButton btnOdustani;
        private MetroFramework.Controls.MetroButton btnDodajServer;
        private MetroFramework.Controls.MetroGrid gridDodatiPaketi;
        private MetroFramework.Controls.MetroLabel labelPostojeciPaketi;
        private MetroFramework.Controls.MetroLabel labelPaketiKojiSeHostujuNaS;
        private MetroFramework.Controls.MetroGrid gridPostojeciBez;
        private MetroFramework.Controls.MetroRadioButton radioWindows;
        private MetroFramework.Controls.MetroRadioButton radioLinux;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroTextBox txtIPadresa;
    }
}