﻿namespace WebHosting.Forme
{
    partial class DodajRTehnickeStruke
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.datumZaposlenja = new MetroFramework.Controls.MetroDateTime();
            this.txtImeOca = new MetroFramework.Controls.MetroTextBox();
            this.txtPrezime = new MetroFramework.Controls.MetroTextBox();
            this.txtGodinaRodj = new MetroFramework.Controls.MetroTextBox();
            this.txtGodRadStaza = new MetroFramework.Controls.MetroTextBox();
            this.txtJmbg = new MetroFramework.Controls.MetroTextBox();
            this.txtLicnoIme = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroPanel2 = new MetroFramework.Controls.MetroPanel();
            this.txtSistem = new MetroFramework.Controls.MetroTextBox();
            this.btnObrisiSistem = new MetroFramework.Controls.MetroButton();
            this.btnDodajSistem = new MetroFramework.Controls.MetroButton();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.gridSistemi = new MetroFramework.Controls.MetroGrid();
            this.RadnikTab = new MetroFramework.Controls.MetroTabControl();
            this.AdministratorPage = new MetroFramework.Controls.MetroTabPage();
            this.ProgramerPage = new MetroFramework.Controls.MetroTabPage();
            this.metroPanel3 = new MetroFramework.Controls.MetroPanel();
            this.txtProgJezik = new MetroFramework.Controls.MetroTextBox();
            this.btnObrisiJezik = new MetroFramework.Controls.MetroButton();
            this.btnDodajJezik = new MetroFramework.Controls.MetroButton();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.gridJezici = new MetroFramework.Controls.MetroGrid();
            this.btnDodajKonacni = new MetroFramework.Controls.MetroButton();
            this.btnOdustani = new MetroFramework.Controls.MetroButton();
            this.metroPanel1.SuspendLayout();
            this.metroPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSistemi)).BeginInit();
            this.RadnikTab.SuspendLayout();
            this.AdministratorPage.SuspendLayout();
            this.ProgramerPage.SuspendLayout();
            this.metroPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridJezici)).BeginInit();
            this.SuspendLayout();
            // 
            // metroPanel1
            // 
            this.metroPanel1.Controls.Add(this.datumZaposlenja);
            this.metroPanel1.Controls.Add(this.txtImeOca);
            this.metroPanel1.Controls.Add(this.txtPrezime);
            this.metroPanel1.Controls.Add(this.txtGodinaRodj);
            this.metroPanel1.Controls.Add(this.txtGodRadStaza);
            this.metroPanel1.Controls.Add(this.txtJmbg);
            this.metroPanel1.Controls.Add(this.txtLicnoIme);
            this.metroPanel1.Controls.Add(this.metroLabel7);
            this.metroPanel1.Controls.Add(this.metroLabel6);
            this.metroPanel1.Controls.Add(this.metroLabel5);
            this.metroPanel1.Controls.Add(this.metroLabel4);
            this.metroPanel1.Controls.Add(this.metroLabel3);
            this.metroPanel1.Controls.Add(this.metroLabel2);
            this.metroPanel1.Controls.Add(this.metroLabel1);
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(23, 63);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(481, 250);
            this.metroPanel1.TabIndex = 0;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // datumZaposlenja
            // 
            this.datumZaposlenja.Location = new System.Drawing.Point(181, 185);
            this.datumZaposlenja.MinimumSize = new System.Drawing.Size(0, 29);
            this.datumZaposlenja.Name = "datumZaposlenja";
            this.datumZaposlenja.Size = new System.Drawing.Size(190, 29);
            this.datumZaposlenja.TabIndex = 15;
            // 
            // txtImeOca
            // 
            // 
            // 
            // 
            this.txtImeOca.CustomButton.Image = null;
            this.txtImeOca.CustomButton.Location = new System.Drawing.Point(168, 1);
            this.txtImeOca.CustomButton.Name = "";
            this.txtImeOca.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtImeOca.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtImeOca.CustomButton.TabIndex = 1;
            this.txtImeOca.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtImeOca.CustomButton.UseSelectable = true;
            this.txtImeOca.CustomButton.Visible = false;
            this.txtImeOca.Lines = new string[0];
            this.txtImeOca.Location = new System.Drawing.Point(181, 40);
            this.txtImeOca.MaxLength = 32767;
            this.txtImeOca.Name = "txtImeOca";
            this.txtImeOca.PasswordChar = '\0';
            this.txtImeOca.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtImeOca.SelectedText = "";
            this.txtImeOca.SelectionLength = 0;
            this.txtImeOca.SelectionStart = 0;
            this.txtImeOca.ShortcutsEnabled = true;
            this.txtImeOca.Size = new System.Drawing.Size(190, 23);
            this.txtImeOca.TabIndex = 14;
            this.txtImeOca.UseSelectable = true;
            this.txtImeOca.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtImeOca.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtImeOca.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtImeOca_KeyPress);
            // 
            // txtPrezime
            // 
            // 
            // 
            // 
            this.txtPrezime.CustomButton.Image = null;
            this.txtPrezime.CustomButton.Location = new System.Drawing.Point(168, 1);
            this.txtPrezime.CustomButton.Name = "";
            this.txtPrezime.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtPrezime.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtPrezime.CustomButton.TabIndex = 1;
            this.txtPrezime.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtPrezime.CustomButton.UseSelectable = true;
            this.txtPrezime.CustomButton.Visible = false;
            this.txtPrezime.Lines = new string[0];
            this.txtPrezime.Location = new System.Drawing.Point(181, 67);
            this.txtPrezime.MaxLength = 32767;
            this.txtPrezime.Name = "txtPrezime";
            this.txtPrezime.PasswordChar = '\0';
            this.txtPrezime.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtPrezime.SelectedText = "";
            this.txtPrezime.SelectionLength = 0;
            this.txtPrezime.SelectionStart = 0;
            this.txtPrezime.ShortcutsEnabled = true;
            this.txtPrezime.Size = new System.Drawing.Size(190, 23);
            this.txtPrezime.TabIndex = 13;
            this.txtPrezime.UseSelectable = true;
            this.txtPrezime.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtPrezime.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtPrezime.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPrezime_KeyPress);
            // 
            // txtGodinaRodj
            // 
            // 
            // 
            // 
            this.txtGodinaRodj.CustomButton.Image = null;
            this.txtGodinaRodj.CustomButton.Location = new System.Drawing.Point(168, 1);
            this.txtGodinaRodj.CustomButton.Name = "";
            this.txtGodinaRodj.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtGodinaRodj.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtGodinaRodj.CustomButton.TabIndex = 1;
            this.txtGodinaRodj.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtGodinaRodj.CustomButton.UseSelectable = true;
            this.txtGodinaRodj.CustomButton.Visible = false;
            this.txtGodinaRodj.Lines = new string[0];
            this.txtGodinaRodj.Location = new System.Drawing.Point(181, 153);
            this.txtGodinaRodj.MaxLength = 32767;
            this.txtGodinaRodj.Name = "txtGodinaRodj";
            this.txtGodinaRodj.PasswordChar = '\0';
            this.txtGodinaRodj.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtGodinaRodj.SelectedText = "";
            this.txtGodinaRodj.SelectionLength = 0;
            this.txtGodinaRodj.SelectionStart = 0;
            this.txtGodinaRodj.ShortcutsEnabled = true;
            this.txtGodinaRodj.Size = new System.Drawing.Size(190, 23);
            this.txtGodinaRodj.TabIndex = 12;
            this.txtGodinaRodj.UseSelectable = true;
            this.txtGodinaRodj.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtGodinaRodj.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtGodinaRodj.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtGodinaRodj_KeyPress);
            // 
            // txtGodRadStaza
            // 
            // 
            // 
            // 
            this.txtGodRadStaza.CustomButton.Image = null;
            this.txtGodRadStaza.CustomButton.Location = new System.Drawing.Point(168, 1);
            this.txtGodRadStaza.CustomButton.Name = "";
            this.txtGodRadStaza.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtGodRadStaza.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtGodRadStaza.CustomButton.TabIndex = 1;
            this.txtGodRadStaza.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtGodRadStaza.CustomButton.UseSelectable = true;
            this.txtGodRadStaza.CustomButton.Visible = false;
            this.txtGodRadStaza.Lines = new string[0];
            this.txtGodRadStaza.Location = new System.Drawing.Point(181, 125);
            this.txtGodRadStaza.MaxLength = 32767;
            this.txtGodRadStaza.Name = "txtGodRadStaza";
            this.txtGodRadStaza.PasswordChar = '\0';
            this.txtGodRadStaza.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtGodRadStaza.SelectedText = "";
            this.txtGodRadStaza.SelectionLength = 0;
            this.txtGodRadStaza.SelectionStart = 0;
            this.txtGodRadStaza.ShortcutsEnabled = true;
            this.txtGodRadStaza.Size = new System.Drawing.Size(190, 23);
            this.txtGodRadStaza.TabIndex = 11;
            this.txtGodRadStaza.UseSelectable = true;
            this.txtGodRadStaza.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtGodRadStaza.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtGodRadStaza.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtGodRadStaza_KeyPress);
            // 
            // txtJmbg
            // 
            // 
            // 
            // 
            this.txtJmbg.CustomButton.Image = null;
            this.txtJmbg.CustomButton.Location = new System.Drawing.Point(168, 1);
            this.txtJmbg.CustomButton.Name = "";
            this.txtJmbg.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtJmbg.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtJmbg.CustomButton.TabIndex = 1;
            this.txtJmbg.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtJmbg.CustomButton.UseSelectable = true;
            this.txtJmbg.CustomButton.Visible = false;
            this.txtJmbg.Lines = new string[0];
            this.txtJmbg.Location = new System.Drawing.Point(181, 95);
            this.txtJmbg.MaxLength = 32767;
            this.txtJmbg.Name = "txtJmbg";
            this.txtJmbg.PasswordChar = '\0';
            this.txtJmbg.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtJmbg.SelectedText = "";
            this.txtJmbg.SelectionLength = 0;
            this.txtJmbg.SelectionStart = 0;
            this.txtJmbg.ShortcutsEnabled = true;
            this.txtJmbg.Size = new System.Drawing.Size(190, 23);
            this.txtJmbg.TabIndex = 10;
            this.txtJmbg.UseSelectable = true;
            this.txtJmbg.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtJmbg.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtJmbg.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtJmbg_KeyPress);
            // 
            // txtLicnoIme
            // 
            // 
            // 
            // 
            this.txtLicnoIme.CustomButton.Image = null;
            this.txtLicnoIme.CustomButton.Location = new System.Drawing.Point(168, 1);
            this.txtLicnoIme.CustomButton.Name = "";
            this.txtLicnoIme.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtLicnoIme.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtLicnoIme.CustomButton.TabIndex = 1;
            this.txtLicnoIme.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtLicnoIme.CustomButton.UseSelectable = true;
            this.txtLicnoIme.CustomButton.Visible = false;
            this.txtLicnoIme.Lines = new string[0];
            this.txtLicnoIme.Location = new System.Drawing.Point(181, 11);
            this.txtLicnoIme.MaxLength = 32767;
            this.txtLicnoIme.Name = "txtLicnoIme";
            this.txtLicnoIme.PasswordChar = '\0';
            this.txtLicnoIme.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtLicnoIme.SelectedText = "";
            this.txtLicnoIme.SelectionLength = 0;
            this.txtLicnoIme.SelectionStart = 0;
            this.txtLicnoIme.ShortcutsEnabled = true;
            this.txtLicnoIme.Size = new System.Drawing.Size(190, 23);
            this.txtLicnoIme.TabIndex = 9;
            this.txtLicnoIme.UseSelectable = true;
            this.txtLicnoIme.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtLicnoIme.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtLicnoIme.Click += new System.EventHandler(this.txtLicnoIme_Click);
            this.txtLicnoIme.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLicnoIme_KeyPress);
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(17, 67);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(60, 19);
            this.metroLabel7.TabIndex = 8;
            this.metroLabel7.Text = "Prezime:";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(17, 95);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(45, 19);
            this.metroLabel6.TabIndex = 7;
            this.metroLabel6.Text = "Jmbg:";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(17, 185);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(116, 19);
            this.metroLabel5.TabIndex = 6;
            this.metroLabel5.Text = "Datum zaposlenja:";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(17, 157);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(106, 19);
            this.metroLabel4.TabIndex = 5;
            this.metroLabel4.Text = "Godina rodjenja:";
            this.metroLabel4.Click += new System.EventHandler(this.metroLabel4_Click);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(17, 129);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(134, 19);
            this.metroLabel3.TabIndex = 4;
            this.metroLabel3.Text = "Godine radnog staza:";
            this.metroLabel3.Click += new System.EventHandler(this.metroLabel3_Click);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(17, 39);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(59, 19);
            this.metroLabel2.TabIndex = 3;
            this.metroLabel2.Text = "Ime oca:";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(17, 11);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(68, 19);
            this.metroLabel1.TabIndex = 2;
            this.metroLabel1.Text = "Licno ime:";
            // 
            // metroPanel2
            // 
            this.metroPanel2.Controls.Add(this.txtSistem);
            this.metroPanel2.Controls.Add(this.btnObrisiSistem);
            this.metroPanel2.Controls.Add(this.btnDodajSistem);
            this.metroPanel2.Controls.Add(this.metroLabel8);
            this.metroPanel2.Controls.Add(this.gridSistemi);
            this.metroPanel2.HorizontalScrollbarBarColor = true;
            this.metroPanel2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel2.HorizontalScrollbarSize = 10;
            this.metroPanel2.Location = new System.Drawing.Point(3, 6);
            this.metroPanel2.Name = "metroPanel2";
            this.metroPanel2.Size = new System.Drawing.Size(464, 180);
            this.metroPanel2.TabIndex = 2;
            this.metroPanel2.VerticalScrollbarBarColor = true;
            this.metroPanel2.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel2.VerticalScrollbarSize = 10;
            // 
            // txtSistem
            // 
            // 
            // 
            // 
            this.txtSistem.CustomButton.Image = null;
            this.txtSistem.CustomButton.Location = new System.Drawing.Point(196, 1);
            this.txtSistem.CustomButton.Name = "";
            this.txtSistem.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtSistem.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtSistem.CustomButton.TabIndex = 1;
            this.txtSistem.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtSistem.CustomButton.UseSelectable = true;
            this.txtSistem.CustomButton.Visible = false;
            this.txtSistem.Lines = new string[0];
            this.txtSistem.Location = new System.Drawing.Point(134, 11);
            this.txtSistem.MaxLength = 32767;
            this.txtSistem.Name = "txtSistem";
            this.txtSistem.PasswordChar = '\0';
            this.txtSistem.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtSistem.SelectedText = "";
            this.txtSistem.SelectionLength = 0;
            this.txtSistem.SelectionStart = 0;
            this.txtSistem.ShortcutsEnabled = true;
            this.txtSistem.Size = new System.Drawing.Size(218, 23);
            this.txtSistem.TabIndex = 6;
            this.txtSistem.UseSelectable = true;
            this.txtSistem.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtSistem.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtSistem.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSistem_KeyPress);
            // 
            // btnObrisiSistem
            // 
            this.btnObrisiSistem.Location = new System.Drawing.Point(358, 40);
            this.btnObrisiSistem.Name = "btnObrisiSistem";
            this.btnObrisiSistem.Size = new System.Drawing.Size(94, 23);
            this.btnObrisiSistem.TabIndex = 5;
            this.btnObrisiSistem.Text = "Obrisi sistem";
            this.btnObrisiSistem.UseSelectable = true;
            this.btnObrisiSistem.Click += new System.EventHandler(this.btnObrisiSistem_Click);
            // 
            // btnDodajSistem
            // 
            this.btnDodajSistem.Location = new System.Drawing.Point(358, 11);
            this.btnDodajSistem.Name = "btnDodajSistem";
            this.btnDodajSistem.Size = new System.Drawing.Size(94, 23);
            this.btnDodajSistem.TabIndex = 4;
            this.btnDodajSistem.Text = "Dodaj sistem";
            this.btnDodajSistem.UseSelectable = true;
            this.btnDodajSistem.Click += new System.EventHandler(this.btnDodajSistem_Click);
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(17, 11);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(116, 19);
            this.metroLabel8.TabIndex = 3;
            this.metroLabel8.Text = "Operativni sistemi:";
            // 
            // gridSistemi
            // 
            this.gridSistemi.AllowUserToResizeRows = false;
            this.gridSistemi.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.gridSistemi.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gridSistemi.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.gridSistemi.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridSistemi.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.gridSistemi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridSistemi.DefaultCellStyle = dataGridViewCellStyle14;
            this.gridSistemi.EnableHeadersVisualStyles = false;
            this.gridSistemi.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.gridSistemi.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.gridSistemi.Location = new System.Drawing.Point(17, 40);
            this.gridSistemi.Name = "gridSistemi";
            this.gridSistemi.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridSistemi.RowHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.gridSistemi.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gridSistemi.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridSistemi.Size = new System.Drawing.Size(335, 100);
            this.gridSistemi.TabIndex = 2;
            // 
            // RadnikTab
            // 
            this.RadnikTab.Controls.Add(this.AdministratorPage);
            this.RadnikTab.Controls.Add(this.ProgramerPage);
            this.RadnikTab.Location = new System.Drawing.Point(23, 331);
            this.RadnikTab.Name = "RadnikTab";
            this.RadnikTab.SelectedIndex = 0;
            this.RadnikTab.Size = new System.Drawing.Size(481, 278);
            this.RadnikTab.TabIndex = 5;
            this.RadnikTab.UseSelectable = true;
            // 
            // AdministratorPage
            // 
            this.AdministratorPage.Controls.Add(this.metroPanel2);
            this.AdministratorPage.HorizontalScrollbarBarColor = true;
            this.AdministratorPage.HorizontalScrollbarHighlightOnWheel = false;
            this.AdministratorPage.HorizontalScrollbarSize = 10;
            this.AdministratorPage.Location = new System.Drawing.Point(4, 38);
            this.AdministratorPage.Name = "AdministratorPage";
            this.AdministratorPage.Size = new System.Drawing.Size(473, 236);
            this.AdministratorPage.TabIndex = 0;
            this.AdministratorPage.Text = "Administrator";
            this.AdministratorPage.VerticalScrollbarBarColor = true;
            this.AdministratorPage.VerticalScrollbarHighlightOnWheel = false;
            this.AdministratorPage.VerticalScrollbarSize = 10;
            this.AdministratorPage.Click += new System.EventHandler(this.metroTabPage1_Click);
            // 
            // ProgramerPage
            // 
            this.ProgramerPage.Controls.Add(this.metroPanel3);
            this.ProgramerPage.HorizontalScrollbarBarColor = true;
            this.ProgramerPage.HorizontalScrollbarHighlightOnWheel = false;
            this.ProgramerPage.HorizontalScrollbarSize = 10;
            this.ProgramerPage.Location = new System.Drawing.Point(4, 38);
            this.ProgramerPage.Name = "ProgramerPage";
            this.ProgramerPage.Size = new System.Drawing.Size(473, 236);
            this.ProgramerPage.TabIndex = 1;
            this.ProgramerPage.Text = "Programer";
            this.ProgramerPage.VerticalScrollbarBarColor = true;
            this.ProgramerPage.VerticalScrollbarHighlightOnWheel = false;
            this.ProgramerPage.VerticalScrollbarSize = 10;
            // 
            // metroPanel3
            // 
            this.metroPanel3.Controls.Add(this.txtProgJezik);
            this.metroPanel3.Controls.Add(this.btnObrisiJezik);
            this.metroPanel3.Controls.Add(this.btnDodajJezik);
            this.metroPanel3.Controls.Add(this.metroLabel9);
            this.metroPanel3.Controls.Add(this.gridJezici);
            this.metroPanel3.HorizontalScrollbarBarColor = true;
            this.metroPanel3.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel3.HorizontalScrollbarSize = 10;
            this.metroPanel3.Location = new System.Drawing.Point(4, 24);
            this.metroPanel3.Name = "metroPanel3";
            this.metroPanel3.Size = new System.Drawing.Size(464, 172);
            this.metroPanel3.TabIndex = 3;
            this.metroPanel3.VerticalScrollbarBarColor = true;
            this.metroPanel3.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel3.VerticalScrollbarSize = 10;
            // 
            // txtProgJezik
            // 
            // 
            // 
            // 
            this.txtProgJezik.CustomButton.Image = null;
            this.txtProgJezik.CustomButton.Location = new System.Drawing.Point(196, 1);
            this.txtProgJezik.CustomButton.Name = "";
            this.txtProgJezik.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtProgJezik.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtProgJezik.CustomButton.TabIndex = 1;
            this.txtProgJezik.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtProgJezik.CustomButton.UseSelectable = true;
            this.txtProgJezik.CustomButton.Visible = false;
            this.txtProgJezik.Lines = new string[0];
            this.txtProgJezik.Location = new System.Drawing.Point(134, 11);
            this.txtProgJezik.MaxLength = 32767;
            this.txtProgJezik.Name = "txtProgJezik";
            this.txtProgJezik.PasswordChar = '\0';
            this.txtProgJezik.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtProgJezik.SelectedText = "";
            this.txtProgJezik.SelectionLength = 0;
            this.txtProgJezik.SelectionStart = 0;
            this.txtProgJezik.ShortcutsEnabled = true;
            this.txtProgJezik.Size = new System.Drawing.Size(218, 23);
            this.txtProgJezik.TabIndex = 6;
            this.txtProgJezik.UseSelectable = true;
            this.txtProgJezik.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtProgJezik.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtProgJezik.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtProgJezik_KeyPress);
            // 
            // btnObrisiJezik
            // 
            this.btnObrisiJezik.Location = new System.Drawing.Point(358, 40);
            this.btnObrisiJezik.Name = "btnObrisiJezik";
            this.btnObrisiJezik.Size = new System.Drawing.Size(94, 23);
            this.btnObrisiJezik.TabIndex = 5;
            this.btnObrisiJezik.Text = "Obrisi jezik";
            this.btnObrisiJezik.UseSelectable = true;
            this.btnObrisiJezik.Click += new System.EventHandler(this.btnObrisiJezik_Click);
            // 
            // btnDodajJezik
            // 
            this.btnDodajJezik.Location = new System.Drawing.Point(358, 11);
            this.btnDodajJezik.Name = "btnDodajJezik";
            this.btnDodajJezik.Size = new System.Drawing.Size(94, 23);
            this.btnDodajJezik.TabIndex = 4;
            this.btnDodajJezik.Text = "Dodaj jezik";
            this.btnDodajJezik.UseSelectable = true;
            this.btnDodajJezik.Click += new System.EventHandler(this.btnDodajJezik_Click);
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(17, 11);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(111, 19);
            this.metroLabel9.TabIndex = 3;
            this.metroLabel9.Text = "Programski jezici:";
            // 
            // gridJezici
            // 
            this.gridJezici.AllowUserToResizeRows = false;
            this.gridJezici.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.gridJezici.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gridJezici.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.gridJezici.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridJezici.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this.gridJezici.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridJezici.DefaultCellStyle = dataGridViewCellStyle17;
            this.gridJezici.EnableHeadersVisualStyles = false;
            this.gridJezici.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.gridJezici.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.gridJezici.Location = new System.Drawing.Point(17, 40);
            this.gridJezici.Name = "gridJezici";
            this.gridJezici.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridJezici.RowHeadersDefaultCellStyle = dataGridViewCellStyle18;
            this.gridJezici.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gridJezici.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridJezici.Size = new System.Drawing.Size(335, 100);
            this.gridJezici.TabIndex = 2;
            // 
            // btnDodajKonacni
            // 
            this.btnDodajKonacni.Location = new System.Drawing.Point(389, 615);
            this.btnDodajKonacni.Name = "btnDodajKonacni";
            this.btnDodajKonacni.Size = new System.Drawing.Size(75, 23);
            this.btnDodajKonacni.TabIndex = 8;
            this.btnDodajKonacni.Text = "Dodaj";
            this.btnDodajKonacni.UseSelectable = true;
            this.btnDodajKonacni.Click += new System.EventHandler(this.btnDodajKonacni_Click);
            // 
            // btnOdustani
            // 
            this.btnOdustani.Location = new System.Drawing.Point(292, 615);
            this.btnOdustani.Name = "btnOdustani";
            this.btnOdustani.Size = new System.Drawing.Size(75, 23);
            this.btnOdustani.TabIndex = 7;
            this.btnOdustani.Text = "Odustani";
            this.btnOdustani.UseSelectable = true;
            this.btnOdustani.Click += new System.EventHandler(this.btnOdustani_Click_1);
            // 
            // DodajRTehnickeStruke
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(539, 675);
            this.Controls.Add(this.btnDodajKonacni);
            this.Controls.Add(this.btnOdustani);
            this.Controls.Add(this.RadnikTab);
            this.Controls.Add(this.metroPanel1);
            this.Name = "DodajRTehnickeStruke";
            this.Text = "Dodaj radnika tehnicke struke";
            this.Load += new System.EventHandler(this.DodajAdministratora_Load);
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel1.PerformLayout();
            this.metroPanel2.ResumeLayout(false);
            this.metroPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSistemi)).EndInit();
            this.RadnikTab.ResumeLayout(false);
            this.AdministratorPage.ResumeLayout(false);
            this.ProgramerPage.ResumeLayout(false);
            this.metroPanel3.ResumeLayout(false);
            this.metroPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridJezici)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroPanel metroPanel2;
        private MetroFramework.Controls.MetroDateTime datumZaposlenja;
        private MetroFramework.Controls.MetroTextBox txtImeOca;
        private MetroFramework.Controls.MetroTextBox txtPrezime;
        private MetroFramework.Controls.MetroTextBox txtGodinaRodj;
        private MetroFramework.Controls.MetroTextBox txtGodRadStaza;
        private MetroFramework.Controls.MetroTextBox txtJmbg;
        private MetroFramework.Controls.MetroTextBox txtLicnoIme;
        private MetroFramework.Controls.MetroGrid gridSistemi;
        private MetroFramework.Controls.MetroTextBox txtSistem;
        private MetroFramework.Controls.MetroButton btnObrisiSistem;
        private MetroFramework.Controls.MetroButton btnDodajSistem;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroTabControl RadnikTab;
        private MetroFramework.Controls.MetroTabPage AdministratorPage;
        private MetroFramework.Controls.MetroTabPage ProgramerPage;
        private MetroFramework.Controls.MetroPanel metroPanel3;
        private MetroFramework.Controls.MetroTextBox txtProgJezik;
        private MetroFramework.Controls.MetroButton btnObrisiJezik;
        private MetroFramework.Controls.MetroButton btnDodajJezik;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroGrid gridJezici;
        private MetroFramework.Controls.MetroButton btnDodajKonacni;
        private MetroFramework.Controls.MetroButton btnOdustani;
    }
}