﻿namespace WebHosting.Forme
{
    partial class DodajKorisnika
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.KorisnikTab = new MetroFramework.Controls.MetroTabControl();
            this.PravnoLicePage = new MetroFramework.Controls.MetroTabPage();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.txtNaziv = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.txtPIB = new MetroFramework.Controls.MetroTextBox();
            this.btnDodajAdm = new MetroFramework.Controls.MetroButton();
            this.btnOdustaniAdm = new MetroFramework.Controls.MetroButton();
            this.FizickoLicePage = new MetroFramework.Controls.MetroTabPage();
            this.txtPrezime = new MetroFramework.Controls.MetroTextBox();
            this.txtJmbg = new MetroFramework.Controls.MetroTextBox();
            this.txtLicnoIme = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.btnDodaj = new MetroFramework.Controls.MetroButton();
            this.btnOdustani = new MetroFramework.Controls.MetroButton();
            this.metroPanel3 = new MetroFramework.Controls.MetroPanel();
            this.txtPrezimeKontakta = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.txtBrojTelefonaKontakta = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.txtImeKontakta = new MetroFramework.Controls.MetroTextBox();
            this.btnObrisiKontakt = new MetroFramework.Controls.MetroButton();
            this.btnDodajKontakt = new MetroFramework.Controls.MetroButton();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.dgvKontakti = new MetroFramework.Controls.MetroGrid();
            this.txtAdresa = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.dgvEkonomisti = new MetroFramework.Controls.MetroGrid();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.datumOd = new MetroFramework.Controls.MetroDateTime();
            this.datumDo = new MetroFramework.Controls.MetroDateTime();
            this.KorisnikTab.SuspendLayout();
            this.PravnoLicePage.SuspendLayout();
            this.FizickoLicePage.SuspendLayout();
            this.metroPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKontakti)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEkonomisti)).BeginInit();
            this.SuspendLayout();
            // 
            // KorisnikTab
            // 
            this.KorisnikTab.Controls.Add(this.PravnoLicePage);
            this.KorisnikTab.Controls.Add(this.FizickoLicePage);
            this.KorisnikTab.Location = new System.Drawing.Point(23, 63);
            this.KorisnikTab.Name = "KorisnikTab";
            this.KorisnikTab.SelectedIndex = 0;
            this.KorisnikTab.Size = new System.Drawing.Size(452, 131);
            this.KorisnikTab.TabIndex = 7;
            this.KorisnikTab.UseSelectable = true;
            // 
            // PravnoLicePage
            // 
            this.PravnoLicePage.Controls.Add(this.metroLabel2);
            this.PravnoLicePage.Controls.Add(this.txtNaziv);
            this.PravnoLicePage.Controls.Add(this.metroLabel4);
            this.PravnoLicePage.Controls.Add(this.txtPIB);
            this.PravnoLicePage.Controls.Add(this.btnDodajAdm);
            this.PravnoLicePage.Controls.Add(this.btnOdustaniAdm);
            this.PravnoLicePage.HorizontalScrollbarBarColor = true;
            this.PravnoLicePage.HorizontalScrollbarHighlightOnWheel = false;
            this.PravnoLicePage.HorizontalScrollbarSize = 10;
            this.PravnoLicePage.Location = new System.Drawing.Point(4, 38);
            this.PravnoLicePage.Name = "PravnoLicePage";
            this.PravnoLicePage.Size = new System.Drawing.Size(444, 89);
            this.PravnoLicePage.TabIndex = 0;
            this.PravnoLicePage.Text = "Pravno lice";
            this.PravnoLicePage.VerticalScrollbarBarColor = true;
            this.PravnoLicePage.VerticalScrollbarHighlightOnWheel = false;
            this.PravnoLicePage.VerticalScrollbarSize = 10;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(8, 57);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(44, 19);
            this.metroLabel2.TabIndex = 10;
            this.metroLabel2.Text = "Naziv:";
            // 
            // txtNaziv
            // 
            // 
            // 
            // 
            this.txtNaziv.CustomButton.Image = null;
            this.txtNaziv.CustomButton.Location = new System.Drawing.Point(168, 1);
            this.txtNaziv.CustomButton.Name = "";
            this.txtNaziv.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtNaziv.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNaziv.CustomButton.TabIndex = 1;
            this.txtNaziv.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNaziv.CustomButton.UseSelectable = true;
            this.txtNaziv.CustomButton.Visible = false;
            this.txtNaziv.Lines = new string[0];
            this.txtNaziv.Location = new System.Drawing.Point(165, 57);
            this.txtNaziv.MaxLength = 32767;
            this.txtNaziv.Name = "txtNaziv";
            this.txtNaziv.PasswordChar = '\0';
            this.txtNaziv.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNaziv.SelectedText = "";
            this.txtNaziv.SelectionLength = 0;
            this.txtNaziv.SelectionStart = 0;
            this.txtNaziv.ShortcutsEnabled = true;
            this.txtNaziv.Size = new System.Drawing.Size(190, 23);
            this.txtNaziv.TabIndex = 11;
            this.txtNaziv.UseSelectable = true;
            this.txtNaziv.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNaziv.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(8, 20);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(31, 19);
            this.metroLabel4.TabIndex = 12;
            this.metroLabel4.Text = "PIB:";
            // 
            // txtPIB
            // 
            // 
            // 
            // 
            this.txtPIB.CustomButton.Image = null;
            this.txtPIB.CustomButton.Location = new System.Drawing.Point(168, 1);
            this.txtPIB.CustomButton.Name = "";
            this.txtPIB.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtPIB.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtPIB.CustomButton.TabIndex = 1;
            this.txtPIB.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtPIB.CustomButton.UseSelectable = true;
            this.txtPIB.CustomButton.Visible = false;
            this.txtPIB.Lines = new string[0];
            this.txtPIB.Location = new System.Drawing.Point(165, 20);
            this.txtPIB.MaxLength = 32767;
            this.txtPIB.Name = "txtPIB";
            this.txtPIB.PasswordChar = '\0';
            this.txtPIB.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtPIB.SelectedText = "";
            this.txtPIB.SelectionLength = 0;
            this.txtPIB.SelectionStart = 0;
            this.txtPIB.ShortcutsEnabled = true;
            this.txtPIB.Size = new System.Drawing.Size(190, 23);
            this.txtPIB.TabIndex = 13;
            this.txtPIB.UseSelectable = true;
            this.txtPIB.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtPIB.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // btnDodajAdm
            // 
            this.btnDodajAdm.Location = new System.Drawing.Point(380, 192);
            this.btnDodajAdm.Name = "btnDodajAdm";
            this.btnDodajAdm.Size = new System.Drawing.Size(75, 23);
            this.btnDodajAdm.TabIndex = 4;
            this.btnDodajAdm.Text = "Dodaj";
            this.btnDodajAdm.UseSelectable = true;
            // 
            // btnOdustaniAdm
            // 
            this.btnOdustaniAdm.Location = new System.Drawing.Point(292, 192);
            this.btnOdustaniAdm.Name = "btnOdustaniAdm";
            this.btnOdustaniAdm.Size = new System.Drawing.Size(75, 23);
            this.btnOdustaniAdm.TabIndex = 3;
            this.btnOdustaniAdm.Text = "Odustani";
            this.btnOdustaniAdm.UseSelectable = true;
            // 
            // FizickoLicePage
            // 
            this.FizickoLicePage.Controls.Add(this.txtPrezime);
            this.FizickoLicePage.Controls.Add(this.txtJmbg);
            this.FizickoLicePage.Controls.Add(this.txtLicnoIme);
            this.FizickoLicePage.Controls.Add(this.metroLabel7);
            this.FizickoLicePage.Controls.Add(this.metroLabel6);
            this.FizickoLicePage.Controls.Add(this.metroLabel3);
            this.FizickoLicePage.HorizontalScrollbarBarColor = true;
            this.FizickoLicePage.HorizontalScrollbarHighlightOnWheel = false;
            this.FizickoLicePage.HorizontalScrollbarSize = 10;
            this.FizickoLicePage.Location = new System.Drawing.Point(4, 38);
            this.FizickoLicePage.Name = "FizickoLicePage";
            this.FizickoLicePage.Size = new System.Drawing.Size(444, 89);
            this.FizickoLicePage.TabIndex = 1;
            this.FizickoLicePage.Text = "Fizicko lice";
            this.FizickoLicePage.VerticalScrollbarBarColor = true;
            this.FizickoLicePage.VerticalScrollbarHighlightOnWheel = false;
            this.FizickoLicePage.VerticalScrollbarSize = 10;
            this.FizickoLicePage.Click += new System.EventHandler(this.FizickoLicePage_Click);
            // 
            // txtPrezime
            // 
            // 
            // 
            // 
            this.txtPrezime.CustomButton.Image = null;
            this.txtPrezime.CustomButton.Location = new System.Drawing.Point(168, 1);
            this.txtPrezime.CustomButton.Name = "";
            this.txtPrezime.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtPrezime.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtPrezime.CustomButton.TabIndex = 1;
            this.txtPrezime.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtPrezime.CustomButton.UseSelectable = true;
            this.txtPrezime.CustomButton.Visible = false;
            this.txtPrezime.Lines = new string[0];
            this.txtPrezime.Location = new System.Drawing.Point(165, 47);
            this.txtPrezime.MaxLength = 32767;
            this.txtPrezime.Name = "txtPrezime";
            this.txtPrezime.PasswordChar = '\0';
            this.txtPrezime.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtPrezime.SelectedText = "";
            this.txtPrezime.SelectionLength = 0;
            this.txtPrezime.SelectionStart = 0;
            this.txtPrezime.ShortcutsEnabled = true;
            this.txtPrezime.Size = new System.Drawing.Size(190, 23);
            this.txtPrezime.TabIndex = 21;
            this.txtPrezime.UseSelectable = true;
            this.txtPrezime.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtPrezime.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtJmbg
            // 
            // 
            // 
            // 
            this.txtJmbg.CustomButton.Image = null;
            this.txtJmbg.CustomButton.Location = new System.Drawing.Point(168, 1);
            this.txtJmbg.CustomButton.Name = "";
            this.txtJmbg.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtJmbg.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtJmbg.CustomButton.TabIndex = 1;
            this.txtJmbg.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtJmbg.CustomButton.UseSelectable = true;
            this.txtJmbg.CustomButton.Visible = false;
            this.txtJmbg.Lines = new string[0];
            this.txtJmbg.Location = new System.Drawing.Point(165, 80);
            this.txtJmbg.MaxLength = 32767;
            this.txtJmbg.Name = "txtJmbg";
            this.txtJmbg.PasswordChar = '\0';
            this.txtJmbg.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtJmbg.SelectedText = "";
            this.txtJmbg.SelectionLength = 0;
            this.txtJmbg.SelectionStart = 0;
            this.txtJmbg.ShortcutsEnabled = true;
            this.txtJmbg.Size = new System.Drawing.Size(190, 23);
            this.txtJmbg.TabIndex = 20;
            this.txtJmbg.UseSelectable = true;
            this.txtJmbg.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtJmbg.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtLicnoIme
            // 
            // 
            // 
            // 
            this.txtLicnoIme.CustomButton.Image = null;
            this.txtLicnoIme.CustomButton.Location = new System.Drawing.Point(168, 1);
            this.txtLicnoIme.CustomButton.Name = "";
            this.txtLicnoIme.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtLicnoIme.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtLicnoIme.CustomButton.TabIndex = 1;
            this.txtLicnoIme.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtLicnoIme.CustomButton.UseSelectable = true;
            this.txtLicnoIme.CustomButton.Visible = false;
            this.txtLicnoIme.Lines = new string[0];
            this.txtLicnoIme.Location = new System.Drawing.Point(165, 18);
            this.txtLicnoIme.MaxLength = 32767;
            this.txtLicnoIme.Name = "txtLicnoIme";
            this.txtLicnoIme.PasswordChar = '\0';
            this.txtLicnoIme.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtLicnoIme.SelectedText = "";
            this.txtLicnoIme.SelectionLength = 0;
            this.txtLicnoIme.SelectionStart = 0;
            this.txtLicnoIme.ShortcutsEnabled = true;
            this.txtLicnoIme.Size = new System.Drawing.Size(190, 23);
            this.txtLicnoIme.TabIndex = 19;
            this.txtLicnoIme.UseSelectable = true;
            this.txtLicnoIme.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtLicnoIme.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(3, 47);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(60, 19);
            this.metroLabel7.TabIndex = 18;
            this.metroLabel7.Text = "Prezime:";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(8, 79);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(45, 19);
            this.metroLabel6.TabIndex = 17;
            this.metroLabel6.Text = "Jmbg:";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(3, 15);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(68, 19);
            this.metroLabel3.TabIndex = 15;
            this.metroLabel3.Text = "Licno ime:";
            // 
            // btnDodaj
            // 
            this.btnDodaj.Location = new System.Drawing.Point(524, 692);
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(75, 23);
            this.btnDodaj.TabIndex = 6;
            this.btnDodaj.Text = "Dodaj";
            this.btnDodaj.UseSelectable = true;
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // btnOdustani
            // 
            this.btnOdustani.Location = new System.Drawing.Point(423, 671);
            this.btnOdustani.Name = "btnOdustani";
            this.btnOdustani.Size = new System.Drawing.Size(75, 23);
            this.btnOdustani.TabIndex = 5;
            this.btnOdustani.Text = "Odustani";
            this.btnOdustani.UseSelectable = true;
            this.btnOdustani.Click += new System.EventHandler(this.btnOdustani_Click);
            // 
            // metroPanel3
            // 
            this.metroPanel3.Controls.Add(this.txtPrezimeKontakta);
            this.metroPanel3.Controls.Add(this.metroLabel12);
            this.metroPanel3.Controls.Add(this.txtBrojTelefonaKontakta);
            this.metroPanel3.Controls.Add(this.metroLabel11);
            this.metroPanel3.Controls.Add(this.txtImeKontakta);
            this.metroPanel3.Controls.Add(this.btnObrisiKontakt);
            this.metroPanel3.Controls.Add(this.btnDodajKontakt);
            this.metroPanel3.Controls.Add(this.metroLabel9);
            this.metroPanel3.Controls.Add(this.dgvKontakti);
            this.metroPanel3.HorizontalScrollbarBarColor = true;
            this.metroPanel3.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel3.HorizontalScrollbarSize = 10;
            this.metroPanel3.Location = new System.Drawing.Point(37, 332);
            this.metroPanel3.Name = "metroPanel3";
            this.metroPanel3.Size = new System.Drawing.Size(576, 309);
            this.metroPanel3.TabIndex = 3;
            this.metroPanel3.VerticalScrollbarBarColor = true;
            this.metroPanel3.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel3.VerticalScrollbarSize = 10;
            // 
            // txtPrezimeKontakta
            // 
            // 
            // 
            // 
            this.txtPrezimeKontakta.CustomButton.Image = null;
            this.txtPrezimeKontakta.CustomButton.Location = new System.Drawing.Point(196, 1);
            this.txtPrezimeKontakta.CustomButton.Name = "";
            this.txtPrezimeKontakta.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtPrezimeKontakta.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtPrezimeKontakta.CustomButton.TabIndex = 1;
            this.txtPrezimeKontakta.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtPrezimeKontakta.CustomButton.UseSelectable = true;
            this.txtPrezimeKontakta.CustomButton.Visible = false;
            this.txtPrezimeKontakta.Lines = new string[0];
            this.txtPrezimeKontakta.Location = new System.Drawing.Point(134, 49);
            this.txtPrezimeKontakta.MaxLength = 32767;
            this.txtPrezimeKontakta.Name = "txtPrezimeKontakta";
            this.txtPrezimeKontakta.PasswordChar = '\0';
            this.txtPrezimeKontakta.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtPrezimeKontakta.SelectedText = "";
            this.txtPrezimeKontakta.SelectionLength = 0;
            this.txtPrezimeKontakta.SelectionStart = 0;
            this.txtPrezimeKontakta.ShortcutsEnabled = true;
            this.txtPrezimeKontakta.Size = new System.Drawing.Size(218, 23);
            this.txtPrezimeKontakta.TabIndex = 10;
            this.txtPrezimeKontakta.UseSelectable = true;
            this.txtPrezimeKontakta.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtPrezimeKontakta.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel12
            // 
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.Location = new System.Drawing.Point(17, 49);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(60, 19);
            this.metroLabel12.TabIndex = 9;
            this.metroLabel12.Text = "Prezime:";
            // 
            // txtBrojTelefonaKontakta
            // 
            // 
            // 
            // 
            this.txtBrojTelefonaKontakta.CustomButton.Image = null;
            this.txtBrojTelefonaKontakta.CustomButton.Location = new System.Drawing.Point(196, 1);
            this.txtBrojTelefonaKontakta.CustomButton.Name = "";
            this.txtBrojTelefonaKontakta.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtBrojTelefonaKontakta.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtBrojTelefonaKontakta.CustomButton.TabIndex = 1;
            this.txtBrojTelefonaKontakta.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtBrojTelefonaKontakta.CustomButton.UseSelectable = true;
            this.txtBrojTelefonaKontakta.CustomButton.Visible = false;
            this.txtBrojTelefonaKontakta.Lines = new string[0];
            this.txtBrojTelefonaKontakta.Location = new System.Drawing.Point(134, 87);
            this.txtBrojTelefonaKontakta.MaxLength = 32767;
            this.txtBrojTelefonaKontakta.Name = "txtBrojTelefonaKontakta";
            this.txtBrojTelefonaKontakta.PasswordChar = '\0';
            this.txtBrojTelefonaKontakta.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtBrojTelefonaKontakta.SelectedText = "";
            this.txtBrojTelefonaKontakta.SelectionLength = 0;
            this.txtBrojTelefonaKontakta.SelectionStart = 0;
            this.txtBrojTelefonaKontakta.ShortcutsEnabled = true;
            this.txtBrojTelefonaKontakta.Size = new System.Drawing.Size(218, 23);
            this.txtBrojTelefonaKontakta.TabIndex = 8;
            this.txtBrojTelefonaKontakta.UseSelectable = true;
            this.txtBrojTelefonaKontakta.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtBrojTelefonaKontakta.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.Location = new System.Drawing.Point(17, 87);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(87, 19);
            this.metroLabel11.TabIndex = 7;
            this.metroLabel11.Text = "Broj telefona:";
            // 
            // txtImeKontakta
            // 
            // 
            // 
            // 
            this.txtImeKontakta.CustomButton.Image = null;
            this.txtImeKontakta.CustomButton.Location = new System.Drawing.Point(196, 1);
            this.txtImeKontakta.CustomButton.Name = "";
            this.txtImeKontakta.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtImeKontakta.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtImeKontakta.CustomButton.TabIndex = 1;
            this.txtImeKontakta.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtImeKontakta.CustomButton.UseSelectable = true;
            this.txtImeKontakta.CustomButton.Visible = false;
            this.txtImeKontakta.Lines = new string[0];
            this.txtImeKontakta.Location = new System.Drawing.Point(134, 11);
            this.txtImeKontakta.MaxLength = 32767;
            this.txtImeKontakta.Name = "txtImeKontakta";
            this.txtImeKontakta.PasswordChar = '\0';
            this.txtImeKontakta.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtImeKontakta.SelectedText = "";
            this.txtImeKontakta.SelectionLength = 0;
            this.txtImeKontakta.SelectionStart = 0;
            this.txtImeKontakta.ShortcutsEnabled = true;
            this.txtImeKontakta.Size = new System.Drawing.Size(218, 23);
            this.txtImeKontakta.TabIndex = 6;
            this.txtImeKontakta.UseSelectable = true;
            this.txtImeKontakta.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtImeKontakta.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // btnObrisiKontakt
            // 
            this.btnObrisiKontakt.Location = new System.Drawing.Point(474, 136);
            this.btnObrisiKontakt.Name = "btnObrisiKontakt";
            this.btnObrisiKontakt.Size = new System.Drawing.Size(94, 23);
            this.btnObrisiKontakt.TabIndex = 5;
            this.btnObrisiKontakt.Text = "Obrisi kontakt";
            this.btnObrisiKontakt.UseSelectable = true;
            this.btnObrisiKontakt.Click += new System.EventHandler(this.btnObrisiKontakt_Click);
            // 
            // btnDodajKontakt
            // 
            this.btnDodajKontakt.Location = new System.Drawing.Point(474, 176);
            this.btnDodajKontakt.Name = "btnDodajKontakt";
            this.btnDodajKontakt.Size = new System.Drawing.Size(94, 23);
            this.btnDodajKontakt.TabIndex = 4;
            this.btnDodajKontakt.Text = "Dodaj kontakt";
            this.btnDodajKontakt.UseSelectable = true;
            this.btnDodajKontakt.Click += new System.EventHandler(this.btnDodajKontakt_Click);
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(17, 11);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(68, 19);
            this.metroLabel9.TabIndex = 3;
            this.metroLabel9.Text = "Licno ime:";
            // 
            // dgvKontakti
            // 
            this.dgvKontakti.AllowUserToResizeRows = false;
            this.dgvKontakti.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgvKontakti.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvKontakti.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvKontakti.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvKontakti.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvKontakti.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvKontakti.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvKontakti.EnableHeadersVisualStyles = false;
            this.dgvKontakti.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgvKontakti.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgvKontakti.Location = new System.Drawing.Point(17, 136);
            this.dgvKontakti.Name = "dgvKontakti";
            this.dgvKontakti.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvKontakti.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvKontakti.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvKontakti.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvKontakti.Size = new System.Drawing.Size(395, 170);
            this.dgvKontakti.TabIndex = 2;
            // 
            // txtAdresa
            // 
            // 
            // 
            // 
            this.txtAdresa.CustomButton.Image = null;
            this.txtAdresa.CustomButton.Location = new System.Drawing.Point(168, 1);
            this.txtAdresa.CustomButton.Name = "";
            this.txtAdresa.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtAdresa.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtAdresa.CustomButton.TabIndex = 1;
            this.txtAdresa.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtAdresa.CustomButton.UseSelectable = true;
            this.txtAdresa.CustomButton.Visible = false;
            this.txtAdresa.Lines = new string[0];
            this.txtAdresa.Location = new System.Drawing.Point(192, 197);
            this.txtAdresa.MaxLength = 32767;
            this.txtAdresa.Name = "txtAdresa";
            this.txtAdresa.PasswordChar = '\0';
            this.txtAdresa.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtAdresa.SelectedText = "";
            this.txtAdresa.SelectionLength = 0;
            this.txtAdresa.SelectionStart = 0;
            this.txtAdresa.ShortcutsEnabled = true;
            this.txtAdresa.Size = new System.Drawing.Size(190, 23);
            this.txtAdresa.TabIndex = 9;
            this.txtAdresa.UseSelectable = true;
            this.txtAdresa.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtAdresa.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(35, 197);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(53, 19);
            this.metroLabel1.TabIndex = 2;
            this.metroLabel1.Text = "Adresa:";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(491, 104);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(120, 19);
            this.metroLabel5.TabIndex = 10;
            this.metroLabel5.Text = "Korespondovao sa:";
            // 
            // dgvEkonomisti
            // 
            this.dgvEkonomisti.AllowUserToResizeRows = false;
            this.dgvEkonomisti.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgvEkonomisti.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvEkonomisti.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvEkonomisti.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEkonomisti.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvEkonomisti.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvEkonomisti.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvEkonomisti.EnableHeadersVisualStyles = false;
            this.dgvEkonomisti.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgvEkonomisti.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgvEkonomisti.Location = new System.Drawing.Point(524, 141);
            this.dgvEkonomisti.Name = "dgvEkonomisti";
            this.dgvEkonomisti.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEkonomisti.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvEkonomisti.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvEkonomisti.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEkonomisti.Size = new System.Drawing.Size(389, 106);
            this.dgvEkonomisti.TabIndex = 11;
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(34, 285);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(29, 19);
            this.metroLabel8.TabIndex = 12;
            this.metroLabel8.Text = "Do:";
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(34, 251);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(31, 19);
            this.metroLabel10.TabIndex = 13;
            this.metroLabel10.Text = "Od:";
            // 
            // datumOd
            // 
            this.datumOd.Location = new System.Drawing.Point(99, 250);
            this.datumOd.MinimumSize = new System.Drawing.Size(0, 29);
            this.datumOd.Name = "datumOd";
            this.datumOd.Size = new System.Drawing.Size(190, 29);
            this.datumOd.TabIndex = 16;
            // 
            // datumDo
            // 
            this.datumDo.Location = new System.Drawing.Point(99, 285);
            this.datumDo.MinimumSize = new System.Drawing.Size(0, 29);
            this.datumDo.Name = "datumDo";
            this.datumDo.Size = new System.Drawing.Size(190, 29);
            this.datumDo.TabIndex = 17;
            // 
            // DodajKorisnika
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(932, 698);
            this.Controls.Add(this.datumDo);
            this.Controls.Add(this.datumOd);
            this.Controls.Add(this.metroLabel10);
            this.Controls.Add(this.metroLabel8);
            this.Controls.Add(this.dgvEkonomisti);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.KorisnikTab);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.txtAdresa);
            this.Controls.Add(this.metroPanel3);
            this.Controls.Add(this.btnOdustani);
            this.Controls.Add(this.btnDodaj);
            this.Name = "DodajKorisnika";
            this.Text = "DodajKorisnika";
            this.Load += new System.EventHandler(this.DodajKorisnika_Load);
            this.KorisnikTab.ResumeLayout(false);
            this.PravnoLicePage.ResumeLayout(false);
            this.PravnoLicePage.PerformLayout();
            this.FizickoLicePage.ResumeLayout(false);
            this.FizickoLicePage.PerformLayout();
            this.metroPanel3.ResumeLayout(false);
            this.metroPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKontakti)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEkonomisti)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroTabControl KorisnikTab;
        private MetroFramework.Controls.MetroTabPage PravnoLicePage;
        private MetroFramework.Controls.MetroButton btnDodajAdm;
        private MetroFramework.Controls.MetroButton btnOdustaniAdm;
        private MetroFramework.Controls.MetroTabPage FizickoLicePage;
        private MetroFramework.Controls.MetroButton btnDodaj;
        private MetroFramework.Controls.MetroButton btnOdustani;
        private MetroFramework.Controls.MetroPanel metroPanel3;
        private MetroFramework.Controls.MetroTextBox txtImeKontakta;
        private MetroFramework.Controls.MetroButton btnObrisiKontakt;
        private MetroFramework.Controls.MetroButton btnDodajKontakt;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroGrid dgvKontakti;
        private MetroFramework.Controls.MetroTextBox txtAdresa;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox txtPrezime;
        private MetroFramework.Controls.MetroTextBox txtJmbg;
        private MetroFramework.Controls.MetroTextBox txtLicnoIme;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox txtNaziv;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroTextBox txtPIB;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroGrid dgvEkonomisti;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroDateTime datumOd;
        private MetroFramework.Controls.MetroDateTime datumDo;
        private MetroFramework.Controls.MetroTextBox txtPrezimeKontakta;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private MetroFramework.Controls.MetroTextBox txtBrojTelefonaKontakta;
        private MetroFramework.Controls.MetroLabel metroLabel11;
    }
}