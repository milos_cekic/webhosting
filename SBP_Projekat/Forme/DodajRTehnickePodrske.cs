﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WebHosting.DTOs;
using WebHosting.Entiteti;

namespace WebHosting.Forme
{
    public partial class DodajRTehnickePodrske : MetroFramework.Forms.MetroForm
    {

        #region Atributi

        private bool kreirajETehnicke = true;
        private EkonomistaTehnickePodrskeView ekonomista;

        #endregion

                #region Konstruktori

        public DodajRTehnickePodrske()
        {
            InitializeComponent();
            kreirajETehnicke = true;
        }

        public DodajRTehnickePodrske(int id)            
        {
            InitializeComponent();
            kreirajETehnicke = false;
            ekonomista = DTOManager.getRadnikTehnickePodrskeView(id);
            PopuniKontrole();
        }

        #endregion

        #region Funkcije

        public void PopuniKontrole()
        {
            txtLicnoIme.Text = ekonomista.Licno_ime;
            txtJmbg.Text = ekonomista.Jmbg;
            txtPrezime.Text = ekonomista.Prezime;
            txtImeOca.Text = ekonomista.Ime_oca;
            txtGodRadStaza.Text = ekonomista.Godine_radnog_staza.ToString();
            txtGodinaRodj.Text = ekonomista.Godina_rodjenja.ToString();
            datumZaposlenja.Value = ekonomista.DatumZaposlenja;

            txtStepenStrucneSpreme.Text = ekonomista.Stepen_strucne_spreme.ToString();
            if (ekonomista.Datum_isteka_ugovora == null)
                dateIstekUgovora.Value = DateTime.MinValue;
            else
                dateIstekUgovora.Value = (DateTime)ekonomista.Datum_isteka_ugovora;
        }

        public bool Validacija()
        {
            //neka za sad ovako

            if (string.IsNullOrEmpty(txtLicnoIme.Text))
            {
                return false;
            }
            else if (string.IsNullOrEmpty(txtPrezime.Text))
            {
                return false;
            }
            else if (string.IsNullOrEmpty(txtJmbg.Text))
            {
                return false;
            }
            else if (string.IsNullOrEmpty(txtGodinaRodj.Text))
            {
                return false;
            }
            else if (string.IsNullOrEmpty(txtGodRadStaza.Text))
            {
                return false;
            }

            return true;
        }

        #endregion

        private void btnOdustani_Click(object sender, EventArgs e)
        {
            this.Close();
            DialogResult = DialogResult.Cancel;
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            if (kreirajETehnicke)
            {
                if (!Validacija())
                    return;

                EkonomistaTehnickePodrske a = new EkonomistaTehnickePodrske();

                a.Licno_ime = txtLicnoIme.Text;
                a.Prezime = txtPrezime.Text;
                a.Ime_oca = txtImeOca.Text;
                a.Jmbg = txtJmbg.Text;
                a.Godina_rodjenja = Int32.Parse(txtGodinaRodj.Text);
                a.Godine_radnog_staza = Int32.Parse(txtGodRadStaza.Text);
                a.Datum_zaposlenja = datumZaposlenja.Value;

                //a.Pozicija = "Administrator";
                a.R_neodredjeno = "Ne";
                a.R_odredjeno = "Da";

                a.Datum_isteka_ugovora = dateIstekUgovora.Value;
                a.Stepen_strucne_spreme=Int32.Parse(txtStepenStrucneSpreme.Text);

                a.R_korisnickePodrske = "Ne";
                DTOManager.AddRadnikTehnickePodrske(a);

//                DTOManager.updateRadnikTehnickePodrske(ekonomista);//?
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                if (!Validacija())
                    return;

                ekonomista.Licno_ime = txtLicnoIme.Text;
                ekonomista.Prezime = txtPrezime.Text;
                ekonomista.Ime_oca = txtImeOca.Text;
                ekonomista.Jmbg = txtJmbg.Text;
                ekonomista.Godina_rodjenja = Int32.Parse(txtGodinaRodj.Text);
                ekonomista.Godine_radnog_staza = Int32.Parse(txtGodRadStaza.Text);
                ekonomista.DatumZaposlenja = datumZaposlenja.Value;

                DTOManager.updateRadnikTehnickePodrske(ekonomista);
                //    this.DialogResult = DialogResult.OK;
            }
        }


    }
}
