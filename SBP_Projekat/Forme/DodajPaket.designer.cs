﻿namespace WebHosting.Forme
{
    partial class DodajPaket
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.txtProstor = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.txtCena = new MetroFramework.Controls.MetroTextBox();
            this.chkSkripting = new MetroFramework.Controls.MetroCheckBox();
            this.chkStaticne = new MetroFramework.Controls.MetroCheckBox();
            this.chkBaza = new MetroFramework.Controls.MetroCheckBox();
            this.labelSkripting = new MetroFramework.Controls.MetroLabel();
            this.txtNazivaBaze = new MetroFramework.Controls.MetroTextBox();
            this.labelNazivBaze = new MetroFramework.Controls.MetroLabel();
            this.txtSkriptingJezik = new MetroFramework.Controls.MetroTextBox();
            this.btnOdustaniPaket = new MetroFramework.Controls.MetroButton();
            this.btnDodajPaket = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(23, 97);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(90, 19);
            this.metroLabel2.TabIndex = 14;
            this.metroLabel2.Text = "Prostor u mb:";
            // 
            // txtProstor
            // 
            // 
            // 
            // 
            this.txtProstor.CustomButton.Image = null;
            this.txtProstor.CustomButton.Location = new System.Drawing.Point(168, 1);
            this.txtProstor.CustomButton.Name = "";
            this.txtProstor.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtProstor.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtProstor.CustomButton.TabIndex = 1;
            this.txtProstor.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtProstor.CustomButton.UseSelectable = true;
            this.txtProstor.CustomButton.Visible = false;
            this.txtProstor.Lines = new string[0];
            this.txtProstor.Location = new System.Drawing.Point(131, 97);
            this.txtProstor.MaxLength = 32767;
            this.txtProstor.Name = "txtProstor";
            this.txtProstor.PasswordChar = '\0';
            this.txtProstor.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtProstor.SelectedText = "";
            this.txtProstor.SelectionLength = 0;
            this.txtProstor.SelectionStart = 0;
            this.txtProstor.ShortcutsEnabled = true;
            this.txtProstor.Size = new System.Drawing.Size(190, 23);
            this.txtProstor.TabIndex = 15;
            this.txtProstor.UseSelectable = true;
            this.txtProstor.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtProstor.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(23, 60);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(42, 19);
            this.metroLabel4.TabIndex = 16;
            this.metroLabel4.Text = "Cena:";
            // 
            // txtCena
            // 
            // 
            // 
            // 
            this.txtCena.CustomButton.Image = null;
            this.txtCena.CustomButton.Location = new System.Drawing.Point(168, 1);
            this.txtCena.CustomButton.Name = "";
            this.txtCena.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtCena.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtCena.CustomButton.TabIndex = 1;
            this.txtCena.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtCena.CustomButton.UseSelectable = true;
            this.txtCena.CustomButton.Visible = false;
            this.txtCena.Lines = new string[0];
            this.txtCena.Location = new System.Drawing.Point(131, 60);
            this.txtCena.MaxLength = 32767;
            this.txtCena.Name = "txtCena";
            this.txtCena.PasswordChar = '\0';
            this.txtCena.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCena.SelectedText = "";
            this.txtCena.SelectionLength = 0;
            this.txtCena.SelectionStart = 0;
            this.txtCena.ShortcutsEnabled = true;
            this.txtCena.Size = new System.Drawing.Size(190, 23);
            this.txtCena.TabIndex = 17;
            this.txtCena.UseSelectable = true;
            this.txtCena.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtCena.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // chkSkripting
            // 
            this.chkSkripting.AutoSize = true;
            this.chkSkripting.Location = new System.Drawing.Point(23, 213);
            this.chkSkripting.Name = "chkSkripting";
            this.chkSkripting.Size = new System.Drawing.Size(159, 15);
            this.chkSkripting.TabIndex = 18;
            this.chkSkripting.Text = "Paket sa skripting jezikom";
            this.chkSkripting.UseSelectable = true;
            this.chkSkripting.CheckedChanged += new System.EventHandler(this.chkSkripting_CheckedChanged);
            // 
            // chkStaticne
            // 
            this.chkStaticne.AutoSize = true;
            this.chkStaticne.Location = new System.Drawing.Point(23, 141);
            this.chkStaticne.Name = "chkStaticne";
            this.chkStaticne.Size = new System.Drawing.Size(145, 15);
            this.chkStaticne.TabIndex = 19;
            this.chkStaticne.Text = "Paket za staticne strane";
            this.chkStaticne.UseSelectable = true;
            // 
            // chkBaza
            // 
            this.chkBaza.AutoSize = true;
            this.chkBaza.Location = new System.Drawing.Point(23, 162);
            this.chkBaza.Name = "chkBaza";
            this.chkBaza.Size = new System.Drawing.Size(105, 15);
            this.chkBaza.TabIndex = 20;
            this.chkBaza.Text = "Paket sa bazom";
            this.chkBaza.UseSelectable = true;
            this.chkBaza.CheckedChanged += new System.EventHandler(this.chkBaza_CheckedChanged);
            // 
            // labelSkripting
            // 
            this.labelSkripting.AutoSize = true;
            this.labelSkripting.Location = new System.Drawing.Point(23, 244);
            this.labelSkripting.Name = "labelSkripting";
            this.labelSkripting.Size = new System.Drawing.Size(92, 19);
            this.labelSkripting.TabIndex = 21;
            this.labelSkripting.Text = "Skripting jezik:";
            // 
            // txtNazivaBaze
            // 
            // 
            // 
            // 
            this.txtNazivaBaze.CustomButton.Image = null;
            this.txtNazivaBaze.CustomButton.Location = new System.Drawing.Point(168, 1);
            this.txtNazivaBaze.CustomButton.Name = "";
            this.txtNazivaBaze.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtNazivaBaze.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNazivaBaze.CustomButton.TabIndex = 1;
            this.txtNazivaBaze.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNazivaBaze.CustomButton.UseSelectable = true;
            this.txtNazivaBaze.CustomButton.Visible = false;
            this.txtNazivaBaze.Lines = new string[0];
            this.txtNazivaBaze.Location = new System.Drawing.Point(171, 184);
            this.txtNazivaBaze.MaxLength = 32767;
            this.txtNazivaBaze.Name = "txtNazivaBaze";
            this.txtNazivaBaze.PasswordChar = '\0';
            this.txtNazivaBaze.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNazivaBaze.SelectedText = "";
            this.txtNazivaBaze.SelectionLength = 0;
            this.txtNazivaBaze.SelectionStart = 0;
            this.txtNazivaBaze.ShortcutsEnabled = true;
            this.txtNazivaBaze.Size = new System.Drawing.Size(190, 23);
            this.txtNazivaBaze.TabIndex = 22;
            this.txtNazivaBaze.UseSelectable = true;
            this.txtNazivaBaze.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNazivaBaze.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // labelNazivBaze
            // 
            this.labelNazivBaze.AutoSize = true;
            this.labelNazivBaze.Location = new System.Drawing.Point(23, 184);
            this.labelNazivBaze.Name = "labelNazivBaze";
            this.labelNazivBaze.Size = new System.Drawing.Size(76, 19);
            this.labelNazivBaze.TabIndex = 23;
            this.labelNazivBaze.Text = "Naziv baze:";
            // 
            // txtSkriptingJezik
            // 
            // 
            // 
            // 
            this.txtSkriptingJezik.CustomButton.Image = null;
            this.txtSkriptingJezik.CustomButton.Location = new System.Drawing.Point(168, 1);
            this.txtSkriptingJezik.CustomButton.Name = "";
            this.txtSkriptingJezik.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtSkriptingJezik.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtSkriptingJezik.CustomButton.TabIndex = 1;
            this.txtSkriptingJezik.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtSkriptingJezik.CustomButton.UseSelectable = true;
            this.txtSkriptingJezik.CustomButton.Visible = false;
            this.txtSkriptingJezik.Lines = new string[0];
            this.txtSkriptingJezik.Location = new System.Drawing.Point(171, 240);
            this.txtSkriptingJezik.MaxLength = 32767;
            this.txtSkriptingJezik.Name = "txtSkriptingJezik";
            this.txtSkriptingJezik.PasswordChar = '\0';
            this.txtSkriptingJezik.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtSkriptingJezik.SelectedText = "";
            this.txtSkriptingJezik.SelectionLength = 0;
            this.txtSkriptingJezik.SelectionStart = 0;
            this.txtSkriptingJezik.ShortcutsEnabled = true;
            this.txtSkriptingJezik.Size = new System.Drawing.Size(190, 23);
            this.txtSkriptingJezik.TabIndex = 24;
            this.txtSkriptingJezik.UseSelectable = true;
            this.txtSkriptingJezik.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtSkriptingJezik.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // btnOdustaniPaket
            // 
            this.btnOdustaniPaket.Location = new System.Drawing.Point(203, 288);
            this.btnOdustaniPaket.Name = "btnOdustaniPaket";
            this.btnOdustaniPaket.Size = new System.Drawing.Size(75, 23);
            this.btnOdustaniPaket.TabIndex = 27;
            this.btnOdustaniPaket.Text = "Odustani";
            this.btnOdustaniPaket.UseSelectable = true;
            this.btnOdustaniPaket.Click += new System.EventHandler(this.BtnOdustaniPaket_Click);
            // 
            // btnDodajPaket
            // 
            this.btnDodajPaket.Location = new System.Drawing.Point(284, 288);
            this.btnDodajPaket.Name = "btnDodajPaket";
            this.btnDodajPaket.Size = new System.Drawing.Size(75, 23);
            this.btnDodajPaket.TabIndex = 28;
            this.btnDodajPaket.Text = "Dodaj";
            this.btnDodajPaket.UseSelectable = true;
            this.btnDodajPaket.Click += new System.EventHandler(this.btnDodajPaket_Click);
            // 
            // DodajPaket
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(407, 373);
            this.Controls.Add(this.btnOdustaniPaket);
            this.Controls.Add(this.btnDodajPaket);
            this.Controls.Add(this.labelSkripting);
            this.Controls.Add(this.txtNazivaBaze);
            this.Controls.Add(this.labelNazivBaze);
            this.Controls.Add(this.txtSkriptingJezik);
            this.Controls.Add(this.chkBaza);
            this.Controls.Add(this.chkStaticne);
            this.Controls.Add(this.chkSkripting);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.txtProstor);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.txtCena);
            this.Name = "DodajPaket";
            this.Text = "DodajPaket";
            this.Load += new System.EventHandler(this.DodajPaket_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox txtProstor;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroTextBox txtCena;
        private MetroFramework.Controls.MetroCheckBox chkSkripting;
        private MetroFramework.Controls.MetroCheckBox chkStaticne;
        private MetroFramework.Controls.MetroCheckBox chkBaza;
        private MetroFramework.Controls.MetroLabel labelSkripting;
        private MetroFramework.Controls.MetroTextBox txtNazivaBaze;
        private MetroFramework.Controls.MetroLabel labelNazivBaze;
        private MetroFramework.Controls.MetroTextBox txtSkriptingJezik;
        private MetroFramework.Controls.MetroButton btnOdustaniPaket;
        private MetroFramework.Controls.MetroButton btnDodajPaket;
    }
}