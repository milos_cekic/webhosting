﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WebHosting.DTOs;
using WebHosting.Entiteti;
using NHibernate;

namespace WebHosting.Forme
{
    public partial class VidiOdrzavanjePaketa : MetroFramework.Forms.MetroForm
    {
        public VidiOdrzavanjePaketa()
        {
            InitializeComponent();
        }

        private void VidiOdrzavanjePaketa_Load(object sender, EventArgs e)
        {
            PopuniTabele();
        }

        private void PopuniTabele()
        {
            dgvPostojeceTrenutnoOdrzavaniPaketi.DataSource = DTOManager.getListaOdrzavaPaketeView(false, 9);

            dgvDostupniAdministratori.DataSource = DTOManager.getRadnikeTehnickeStruke("Administrator");
            dgvDostupniAdministratori.Columns[0].Visible = false;
            dgvDostupniAdministratori.Columns[10].Visible = false;
            dgvDostupniAdministratori.Columns[9].Visible = false;
            dgvDostupniAdministratori.Columns[8].Visible = false;

            dgvDostupniProgrameri.DataSource = DTOManager.getRadnikeTehnickeStruke("Programer");
            dgvDostupniProgrameri.Columns[9].Visible = false;
            dgvDostupniProgrameri.Columns[8].Visible = false;

            dgvPaketiBazaSkripting.DataSource = DTOManager.getListaPaket(true, "Da", "Da", "Ne");

            dgvPaketiStaticne.DataSource = DTOManager.getListaPaket(true, "", "", "Da");
        }

        private void dgvDostupniRadnici_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgvPaketi_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void BtnOdustani_Click(object sender, EventArgs e)
        {
            this.Close();
            DialogResult = DialogResult.Cancel;
        }
    }
}
