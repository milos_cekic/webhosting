﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WebHosting.DTOs;
using WebHosting.Entiteti;
using NHibernate;

namespace WebHosting.Forme
{
    public partial class VidiNarudzbine : MetroFramework.Forms.MetroForm
    {

        #region atributi

        private PaketView paketKojiSeNarucuje;
        private RadnikView radnikKojiPrimaPaket;
        private Korisnik korisnik;

        #endregion

        public VidiNarudzbine()
        {
            InitializeComponent();
            paketKojiSeNarucuje = new PaketView();
            radnikKojiPrimaPaket = new RadnikView();
            korisnik = new Korisnik();

            ISession s = DataLayer.GetSession();

            korisnik = s.Get<Korisnik>(3);

            s.Close();
        }

        public VidiNarudzbine(int id)
        {

            InitializeComponent();

            paketKojiSeNarucuje = new PaketView();
            radnikKojiPrimaPaket = new RadnikView();
            this.korisnik = new Korisnik();

            ISession s = DataLayer.GetSession();

            this.korisnik = s.Get<Korisnik>(3);

            s.Close();



        }

        private void VidiNarudzbine_Load(object sender, EventArgs e)
        {
            dgvPaketiZaNarucivanje.DataSource = DTOManager.getListaPaket(false, "","","");
            dgvPostojeceNarudzbine.DataSource = DTOManager.getListaNarudzbinaView(true, 1);
            dgvPrimaociNarudzbine.DataSource = DTOManager.getRadnikeKorisnickePodrske();
            dgvPrimaociNarudzbine.Columns[9].Visible = false;
            dgvPrimaociNarudzbine.Columns[8].Visible = true;
        }

        private void BtnKreirajNarudzbinu_Click(object sender, EventArgs e)
        {
            Narudzbina narudzbina = new Narudzbina()
            {
                KupujeNarudzbinu = this.korisnik,
                Datum = DateTime.Now,
                TrajanjeMeseci = int.Parse(txtTrajanjeMeseci.Text)
            };

            foreach (DataGridViewRow row in dgvPrimaociNarudzbine.SelectedRows)
            {
                int id = ((RadnikView)dgvPrimaociNarudzbine.SelectedRows[0].DataBoundItem).Id;//malo da se ispravi ali moze ovako da se uzme iz dgv
                if (id < 1)
                {
                    return;
                }
                narudzbina.PrimaNarudzbinu = DTOManager.getRadnikKorisnickePodrske(id);
            }

            foreach (DataGridViewRow row in dgvPaketiZaNarucivanje.SelectedRows)
            {
                int id = ((Paket)dgvPaketiZaNarucivanje.SelectedRows[0].DataBoundItem).Id;//malo da se ispravi ali moze ovako da se uzme iz dgv
                if (id < 1)
                {
                    return;
                }
                narudzbina.PosedujePaket = DTOManager.getPaket(id);
            }


            DTOManager.AddNarudzbinu(narudzbina);


            ISession s = DataLayer.GetSession();



            //korisnik.KupujeNarudzbine.Add(narudzbina);

            //s.SaveOrUpdate(korisnik);
            //s.Flush();
            //s.Close();

            this.DialogResult = DialogResult.OK;

        }

        private void BtnOdustani_Click(object sender, EventArgs e)
        {
            this.Close();
            DialogResult = DialogResult.Cancel;
        }

        private void BtnObrisiIzavrano_Click(object sender, EventArgs e)
        {

        }
    }
}
