﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WebHosting.DTOs;
using WebHosting.Entiteti;

namespace WebHosting.Forme
{
    public partial class DodajServer : MetroFramework.Forms.MetroForm
    {
        #region atributi

        bool kreirajServer = true;
        private WindowsServerView windowsServer;
        private LinuxServerView linuxServer;
        private ServerView server;
        private List<PaketView> listaPaketa;

        #endregion

        public DodajServer()
        {
            InitializeComponent();
            kreirajServer = true;
            windowsServer = new WindowsServerView();
            listaPaketa = new List<PaketView>();
        }

        public DodajServer(int id)
        {
            InitializeComponent();
            kreirajServer = true;
            //server = DTOManager.getServerView(id);//radi
            windowsServer = DTOManager.getWindowsServerView(id);
            MessageBox.Show(windowsServer.IP_adresa);
            PopuniKontrole(id);
        }

        private void PopuniKontrole(int id)
        {
            txtIPadresa.Text = server.IP_adresa;
            //ovo ovde cisto za proveru, inace ce se znati koji se menja!

            //Promeniti strukturu, OS_tip je null

            //if (String.Compare(server.OS_tip, "S_LINUX") == 0)
            //{
            //    radioWindows.Checked = false;
            //    radioLinux.Checked = true;
            //    PrikaziSakrij();
            //}
            //else
            //{
            //    radioLinux.Checked = false;
            //    radioWindows.Checked = true;
            //    PrikaziSakrij();
            //    WindowsServerView wsv = DTOManager.getWindowsServerView(id);
            //    //if (wsv.Paketi != null)
            //    //{
            //    //    gridDodatiPaketi.DataSource = wsv.Paketi;//.ToList();
            //    //    //gridDodatiPaketi.Columns[0].HeaderText = "Paket";
            //    //}
            //}
        }

        private void DodajServer_Load(object sender, EventArgs e)
        {
            //S_WINDOWS S_LINUX
            //List<string> lista = new List<string>();
            //lista.Add("windows");
            //lista.Add("linux");
            //comboOS.DataSource = lista;

            //radioLinux.Checked = true;
            PrikaziSakrij();
        }

        private void PrikaziSakrij()
        {
            if (radioWindows.Checked == true)
            {
                labelPostojeciPaketi.Visible = true;
                labelPaketiKojiSeHostujuNaS.Visible = true;
                gridDodatiPaketi.Visible = true;
                gridPostojeciBez.Visible = true;

                gridPostojeciBez.DataSource = DTOManager.getListaPaket(false, "Da", "Ne", "Ne");

            }
            else
            {
                labelPostojeciPaketi.Visible = false;
                labelPaketiKojiSeHostujuNaS.Visible = false;
                gridDodatiPaketi.Visible = false;
                gridPostojeciBez.Visible = false;
            }
        }

        private void radioLinux_CheckedChanged(object sender, EventArgs e)
        {
            PrikaziSakrij();
        }

        private void radioWindows_CheckedChanged(object sender, EventArgs e)
        {
            PrikaziSakrij();
        }

        private void gridPostojeciBez_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ////MessageBox.Show(((Paket)gridPostojeciBez.Rows[e.RowIndex].DataBoundItem).Id.ToString());
            ////.Add(DTOManager.getPaket(((Paket)gridPostojeciBez.Rows[e.RowIndex].DataBoundItem).Id));
            ////Paket novi = DTOManager.getPaket(((Paket)gridPostojeciBez.Rows[e.RowIndex].DataBoundItem).Id);
            //listaPaketa.Add(DTOManager.getPaketView(((Paket)gridPostojeciBez.Rows[e.RowIndex].DataBoundItem).Id));

            ////MessageBox.Show(novi.Cena.ToString());
            //gridDodatiPaketi.DataSource = new List<Paket>();
            //gridDodatiPaketi.DataSource = listaPaketa;
        }

        private void btnOdustani_Click(object sender, EventArgs e)
        {
            this.Close();
            DialogResult = DialogResult.Cancel;
        }

        private void BtnDodajServer_Click(object sender, EventArgs e)
        {


            if (radioWindows.Checked)
            {
                WindowsServer ws = new WindowsServer();
                ws.IP_adresa = txtIPadresa.Text;
                DTOManager.addWindowsServer(ws);

                if (listaPaketa.Count != 0)
                {
                    foreach (var paket in listaPaketa)
                    {
                        //pitanje je da li ovako hoce
                        if (ws.Paketi.Where(x => x.HostujuSeNaServerima == ws).ToList().Count() > 0)
                            continue;
                        else
                        {
                            DTOManager.addHostujeSe(ws.Id, paket.Id);
                        }
                    }
                    //foreach (var paket in ws.Paketi)
                    //{
                    //    if (listaPaketa.Where(x => x. == osoba.KorisnikZaKogPamtimo && x.LicnoIme == osoba.LicnoIme && x.Prezime == osoba.Prezime).ToList().Count() == 0)
                    //        DTOManager.deleteKontaktKorisnik(osoba.Id);
                    //}
                    //MessageBox.Show("Uspesno dodavanje");

                    this.DialogResult = DialogResult.OK;

                }
                else if (radioLinux.Checked)
                {
                    LinuxServer linux = new LinuxServer();
                    linux.IP_adresa = txtIPadresa.Text;

                    DTOManager.addLinuxServer(linux);
                    this.DialogResult = DialogResult.OK;
                }
            }
        }

        private void GridPostojeciBez_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (gridPostojeciBez.SelectedRows.Count == 0)
            {
                return;
            }

            gridDodatiPaketi.DataSource = ((Paket)gridPostojeciBez.Rows[e.RowIndex].DataBoundItem);
        }
    }
}
