﻿namespace WebHosting.Forme
{
    partial class Glavna
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabovi = new MetroFramework.Controls.MetroTabControl();
            this.tabZaposleni = new MetroFramework.Controls.MetroTabPage();
            this.btdDodajKorisnicke = new MetroFramework.Controls.MetroButton();
            this.btnObrisiOznacenog = new MetroFramework.Controls.MetroButton();
            this.btnEkonomista = new MetroFramework.Controls.MetroButton();
            this.btnDodajRadnika = new MetroFramework.Controls.MetroButton();
            this.dgvZaposleni = new MetroFramework.Controls.MetroGrid();
            this.radioKorisnickePodrske = new MetroFramework.Controls.MetroRadioButton();
            this.radioProgramer = new MetroFramework.Controls.MetroRadioButton();
            this.radioAdministrator = new MetroFramework.Controls.MetroRadioButton();
            this.radioTehnickePodrske = new MetroFramework.Controls.MetroRadioButton();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.tabKorisnici = new MetroFramework.Controls.MetroTabPage();
            this.btnObrisiKorisnika = new MetroFramework.Controls.MetroButton();
            this.btnDodajKorisnika = new MetroFramework.Controls.MetroButton();
            this.dgvKorisnici = new MetroFramework.Controls.MetroGrid();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.radioFizicko = new MetroFramework.Controls.MetroRadioButton();
            this.radioPravno = new MetroFramework.Controls.MetroRadioButton();
            this.vidiNarudzbine = new MetroFramework.Controls.MetroButton();
            this.dodajPaket = new MetroFramework.Controls.MetroButton();
            this.btnDodajServer = new MetroFramework.Controls.MetroButton();
            this.btnOdrServeri = new MetroFramework.Controls.MetroButton();
            this.btnOdrPaketi = new MetroFramework.Controls.MetroButton();
            this.btnHostovanja = new MetroFramework.Controls.MetroButton();
            this.tabovi.SuspendLayout();
            this.tabZaposleni.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvZaposleni)).BeginInit();
            this.tabKorisnici.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKorisnici)).BeginInit();
            this.SuspendLayout();
            // 
            // tabovi
            // 
            this.tabovi.Controls.Add(this.tabZaposleni);
            this.tabovi.Controls.Add(this.tabKorisnici);
            this.tabovi.Location = new System.Drawing.Point(23, 63);
            this.tabovi.Name = "tabovi";
            this.tabovi.SelectedIndex = 0;
            this.tabovi.Size = new System.Drawing.Size(827, 403);
            this.tabovi.TabIndex = 0;
            this.tabovi.UseSelectable = true;
            // 
            // tabZaposleni
            // 
            this.tabZaposleni.Controls.Add(this.btnDodajServer);
            this.tabZaposleni.Controls.Add(this.vidiNarudzbine);
            this.tabZaposleni.Controls.Add(this.dodajPaket);
            this.tabZaposleni.Controls.Add(this.btdDodajKorisnicke);
            this.tabZaposleni.Controls.Add(this.btnObrisiOznacenog);
            this.tabZaposleni.Controls.Add(this.btnEkonomista);
            this.tabZaposleni.Controls.Add(this.btnDodajRadnika);
            this.tabZaposleni.Controls.Add(this.dgvZaposleni);
            this.tabZaposleni.Controls.Add(this.radioKorisnickePodrske);
            this.tabZaposleni.Controls.Add(this.radioProgramer);
            this.tabZaposleni.Controls.Add(this.radioAdministrator);
            this.tabZaposleni.Controls.Add(this.radioTehnickePodrske);
            this.tabZaposleni.Controls.Add(this.metroLabel1);
            this.tabZaposleni.HorizontalScrollbarBarColor = true;
            this.tabZaposleni.HorizontalScrollbarHighlightOnWheel = false;
            this.tabZaposleni.HorizontalScrollbarSize = 10;
            this.tabZaposleni.Location = new System.Drawing.Point(4, 38);
            this.tabZaposleni.Name = "tabZaposleni";
            this.tabZaposleni.Size = new System.Drawing.Size(819, 361);
            this.tabZaposleni.TabIndex = 0;
            this.tabZaposleni.Text = "Zaposleni";
            this.tabZaposleni.VerticalScrollbarBarColor = true;
            this.tabZaposleni.VerticalScrollbarHighlightOnWheel = false;
            this.tabZaposleni.VerticalScrollbarSize = 10;
            // 
            // btdDodajKorisnicke
            // 
            this.btdDodajKorisnicke.Location = new System.Drawing.Point(114, 335);
            this.btdDodajKorisnicke.Name = "btdDodajKorisnicke";
            this.btdDodajKorisnicke.Size = new System.Drawing.Size(143, 23);
            this.btdDodajKorisnicke.TabIndex = 16;
            this.btdDodajKorisnicke.Text = "Dodaj radnika korisnicke";
            this.btdDodajKorisnicke.UseSelectable = true;
            this.btdDodajKorisnicke.Click += new System.EventHandler(this.btdDodajKorisnicke_Click);
            // 
            // btnObrisiOznacenog
            // 
            this.btnObrisiOznacenog.Location = new System.Drawing.Point(667, 335);
            this.btnObrisiOznacenog.Name = "btnObrisiOznacenog";
            this.btnObrisiOznacenog.Size = new System.Drawing.Size(125, 23);
            this.btnObrisiOznacenog.TabIndex = 15;
            this.btnObrisiOznacenog.Text = "Obrisi oznacenog";
            this.btnObrisiOznacenog.UseSelectable = true;
            this.btnObrisiOznacenog.Click += new System.EventHandler(this.btnObrisiOznacenog_Click);
            // 
            // btnEkonomista
            // 
            this.btnEkonomista.Location = new System.Drawing.Point(523, 335);
            this.btnEkonomista.Name = "btnEkonomista";
            this.btnEkonomista.Size = new System.Drawing.Size(125, 23);
            this.btnEkonomista.TabIndex = 14;
            this.btnEkonomista.Text = "Dodaj ekonomistu";
            this.btnEkonomista.UseSelectable = true;
            this.btnEkonomista.Click += new System.EventHandler(this.btnEkonomista_Click);
            // 
            // btnDodajRadnika
            // 
            this.btnDodajRadnika.Location = new System.Drawing.Point(299, 335);
            this.btnDodajRadnika.Name = "btnDodajRadnika";
            this.btnDodajRadnika.Size = new System.Drawing.Size(196, 23);
            this.btnDodajRadnika.TabIndex = 13;
            this.btnDodajRadnika.Text = "Dodaj programera/administratora";
            this.btnDodajRadnika.UseSelectable = true;
            this.btnDodajRadnika.Click += new System.EventHandler(this.btnDodajRadnika_Click);
            // 
            // dgvZaposleni
            // 
            this.dgvZaposleni.AllowUserToResizeRows = false;
            this.dgvZaposleni.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgvZaposleni.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvZaposleni.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvZaposleni.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvZaposleni.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.dgvZaposleni.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvZaposleni.DefaultCellStyle = dataGridViewCellStyle14;
            this.dgvZaposleni.EnableHeadersVisualStyles = false;
            this.dgvZaposleni.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgvZaposleni.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgvZaposleni.Location = new System.Drawing.Point(-4, 37);
            this.dgvZaposleni.Name = "dgvZaposleni";
            this.dgvZaposleni.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvZaposleni.RowHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.dgvZaposleni.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvZaposleni.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvZaposleni.Size = new System.Drawing.Size(706, 275);
            this.dgvZaposleni.TabIndex = 8;
            this.dgvZaposleni.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvZaposleni_CellContentDoubleClick);
            // 
            // radioKorisnickePodrske
            // 
            this.radioKorisnickePodrske.AutoSize = true;
            this.radioKorisnickePodrske.Location = new System.Drawing.Point(275, 16);
            this.radioKorisnickePodrske.Name = "radioKorisnickePodrske";
            this.radioKorisnickePodrske.Size = new System.Drawing.Size(163, 15);
            this.radioKorisnickePodrske.TabIndex = 7;
            this.radioKorisnickePodrske.Text = "Radnici korisnicke podrske";
            this.radioKorisnickePodrske.UseSelectable = true;
            this.radioKorisnickePodrske.CheckedChanged += new System.EventHandler(this.radioKorisnickePodrske_CheckedChanged);
            // 
            // radioProgramer
            // 
            this.radioProgramer.AutoSize = true;
            this.radioProgramer.Location = new System.Drawing.Point(461, 16);
            this.radioProgramer.Name = "radioProgramer";
            this.radioProgramer.Size = new System.Drawing.Size(82, 15);
            this.radioProgramer.TabIndex = 6;
            this.radioProgramer.Text = "Programeri";
            this.radioProgramer.UseSelectable = true;
            this.radioProgramer.CheckedChanged += new System.EventHandler(this.radioProgramer_CheckedChanged);
            // 
            // radioAdministrator
            // 
            this.radioAdministrator.AutoSize = true;
            this.radioAdministrator.Location = new System.Drawing.Point(563, 16);
            this.radioAdministrator.Name = "radioAdministrator";
            this.radioAdministrator.Size = new System.Drawing.Size(99, 15);
            this.radioAdministrator.TabIndex = 5;
            this.radioAdministrator.Text = "Administratori";
            this.radioAdministrator.UseSelectable = true;
            this.radioAdministrator.CheckedChanged += new System.EventHandler(this.radioAdministrator_CheckedChanged);
            // 
            // radioTehnickePodrske
            // 
            this.radioTehnickePodrske.AutoSize = true;
            this.radioTehnickePodrske.Location = new System.Drawing.Point(82, 16);
            this.radioTehnickePodrske.Name = "radioTehnickePodrske";
            this.radioTehnickePodrske.Size = new System.Drawing.Size(175, 15);
            this.radioTehnickePodrske.TabIndex = 3;
            this.radioTehnickePodrske.Text = "Ekonomisti tehnicke podrske";
            this.radioTehnickePodrske.UseSelectable = true;
            this.radioTehnickePodrske.CheckedChanged += new System.EventHandler(this.radioTehnickePodrske_CheckedChanged);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(0, 12);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(46, 19);
            this.metroLabel1.TabIndex = 2;
            this.metroLabel1.Text = "Izlistaj:";
            // 
            // tabKorisnici
            // 
            this.tabKorisnici.Controls.Add(this.btnObrisiKorisnika);
            this.tabKorisnici.Controls.Add(this.btnDodajKorisnika);
            this.tabKorisnici.Controls.Add(this.dgvKorisnici);
            this.tabKorisnici.Controls.Add(this.metroLabel2);
            this.tabKorisnici.Controls.Add(this.radioFizicko);
            this.tabKorisnici.Controls.Add(this.radioPravno);
            this.tabKorisnici.HorizontalScrollbarBarColor = true;
            this.tabKorisnici.HorizontalScrollbarHighlightOnWheel = false;
            this.tabKorisnici.HorizontalScrollbarSize = 10;
            this.tabKorisnici.Location = new System.Drawing.Point(4, 38);
            this.tabKorisnici.Name = "tabKorisnici";
            this.tabKorisnici.Size = new System.Drawing.Size(819, 361);
            this.tabKorisnici.TabIndex = 1;
            this.tabKorisnici.Text = "Korisnici";
            this.tabKorisnici.VerticalScrollbarBarColor = true;
            this.tabKorisnici.VerticalScrollbarHighlightOnWheel = false;
            this.tabKorisnici.VerticalScrollbarSize = 10;
            // 
            // btnObrisiKorisnika
            // 
            this.btnObrisiKorisnika.Location = new System.Drawing.Point(677, 335);
            this.btnObrisiKorisnika.Name = "btnObrisiKorisnika";
            this.btnObrisiKorisnika.Size = new System.Drawing.Size(113, 23);
            this.btnObrisiKorisnika.TabIndex = 12;
            this.btnObrisiKorisnika.Text = "Obrisi izabranog";
            this.btnObrisiKorisnika.UseSelectable = true;
            this.btnObrisiKorisnika.Click += new System.EventHandler(this.btnObrisiKorisnika_Click);
            // 
            // btnDodajKorisnika
            // 
            this.btnDodajKorisnika.Location = new System.Drawing.Point(502, 335);
            this.btnDodajKorisnika.Name = "btnDodajKorisnika";
            this.btnDodajKorisnika.Size = new System.Drawing.Size(132, 23);
            this.btnDodajKorisnika.TabIndex = 11;
            this.btnDodajKorisnika.Text = "Dodaj Korisnika";
            this.btnDodajKorisnika.UseSelectable = true;
            this.btnDodajKorisnika.Click += new System.EventHandler(this.btnDodajKorisnika_Click);
            // 
            // dgvKorisnici
            // 
            this.dgvKorisnici.AllowUserToResizeRows = false;
            this.dgvKorisnici.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgvKorisnici.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvKorisnici.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvKorisnici.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvKorisnici.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this.dgvKorisnici.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvKorisnici.DefaultCellStyle = dataGridViewCellStyle17;
            this.dgvKorisnici.EnableHeadersVisualStyles = false;
            this.dgvKorisnici.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgvKorisnici.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgvKorisnici.Location = new System.Drawing.Point(3, 38);
            this.dgvKorisnici.Name = "dgvKorisnici";
            this.dgvKorisnici.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvKorisnici.RowHeadersDefaultCellStyle = dataGridViewCellStyle18;
            this.dgvKorisnici.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvKorisnici.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvKorisnici.Size = new System.Drawing.Size(759, 275);
            this.dgvKorisnici.TabIndex = 9;
            this.dgvKorisnici.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvKorisnici_CellMouseDoubleClick);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(3, 13);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(46, 19);
            this.metroLabel2.TabIndex = 7;
            this.metroLabel2.Text = "Izlistaj:";
            // 
            // radioFizicko
            // 
            this.radioFizicko.AutoSize = true;
            this.radioFizicko.Location = new System.Drawing.Point(219, 17);
            this.radioFizicko.Name = "radioFizicko";
            this.radioFizicko.Size = new System.Drawing.Size(79, 15);
            this.radioFizicko.TabIndex = 6;
            this.radioFizicko.Text = "Fizicka lica";
            this.radioFizicko.UseSelectable = true;
            this.radioFizicko.CheckedChanged += new System.EventHandler(this.RadioFizicko_CheckedChanged);
            // 
            // radioPravno
            // 
            this.radioPravno.AutoSize = true;
            this.radioPravno.Location = new System.Drawing.Point(111, 17);
            this.radioPravno.Name = "radioPravno";
            this.radioPravno.Size = new System.Drawing.Size(80, 15);
            this.radioPravno.TabIndex = 5;
            this.radioPravno.Text = "Pravna lica";
            this.radioPravno.UseSelectable = true;
            this.radioPravno.CheckedChanged += new System.EventHandler(this.RadioPravno_CheckedChanged);
            // 
            // vidiNarudzbine
            // 
            this.vidiNarudzbine.Location = new System.Drawing.Point(717, 120);
            this.vidiNarudzbine.Name = "vidiNarudzbine";
            this.vidiNarudzbine.Size = new System.Drawing.Size(75, 23);
            this.vidiNarudzbine.TabIndex = 20;
            this.vidiNarudzbine.Text = "Narudzbine";
            this.vidiNarudzbine.UseSelectable = true;
            this.vidiNarudzbine.Click += new System.EventHandler(this.VidiNarudzbine_Click);
            // 
            // dodajPaket
            // 
            this.dodajPaket.Location = new System.Drawing.Point(717, 50);
            this.dodajPaket.Name = "dodajPaket";
            this.dodajPaket.Size = new System.Drawing.Size(75, 23);
            this.dodajPaket.TabIndex = 19;
            this.dodajPaket.Text = "Dodaj Paket";
            this.dodajPaket.UseSelectable = true;
            this.dodajPaket.Click += new System.EventHandler(this.DodajPaket_Click);
            // 
            // btnDodajServer
            // 
            this.btnDodajServer.Location = new System.Drawing.Point(717, 83);
            this.btnDodajServer.Name = "btnDodajServer";
            this.btnDodajServer.Size = new System.Drawing.Size(75, 23);
            this.btnDodajServer.TabIndex = 21;
            this.btnDodajServer.Text = "Dodaj Server";
            this.btnDodajServer.UseSelectable = true;
            this.btnDodajServer.Click += new System.EventHandler(this.BtnDodajServer_Click);
            // 
            // btnOdrServeri
            // 
            this.btnOdrServeri.Location = new System.Drawing.Point(578, 34);
            this.btnOdrServeri.Name = "btnOdrServeri";
            this.btnOdrServeri.Size = new System.Drawing.Size(111, 23);
            this.btnOdrServeri.TabIndex = 24;
            this.btnOdrServeri.Text = "Odrzavanja servera";
            this.btnOdrServeri.UseSelectable = true;
            this.btnOdrServeri.Click += new System.EventHandler(this.BtnOdrServeri_Click);
            // 
            // btnOdrPaketi
            // 
            this.btnOdrPaketi.Location = new System.Drawing.Point(708, 34);
            this.btnOdrPaketi.Name = "btnOdrPaketi";
            this.btnOdrPaketi.Size = new System.Drawing.Size(111, 23);
            this.btnOdrPaketi.TabIndex = 23;
            this.btnOdrPaketi.Text = "Odrzavanja paketa";
            this.btnOdrPaketi.UseSelectable = true;
            this.btnOdrPaketi.Click += new System.EventHandler(this.BtnOdrPaketi_Click);
            // 
            // btnHostovanja
            // 
            this.btnHostovanja.Location = new System.Drawing.Point(440, 34);
            this.btnHostovanja.Name = "btnHostovanja";
            this.btnHostovanja.Size = new System.Drawing.Size(120, 23);
            this.btnHostovanja.TabIndex = 22;
            this.btnHostovanja.Text = "Hostovanja";
            this.btnHostovanja.UseSelectable = true;
            this.btnHostovanja.Click += new System.EventHandler(this.BtnHostovanja_Click);
            // 
            // Glavna
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(872, 507);
            this.Controls.Add(this.btnHostovanja);
            this.Controls.Add(this.tabovi);
            this.Controls.Add(this.btnOdrPaketi);
            this.Controls.Add(this.btnOdrServeri);
            this.Name = "Glavna";
            this.Text = "Web hosting";
            this.Load += new System.EventHandler(this.Glavna_Load);
            this.tabovi.ResumeLayout(false);
            this.tabZaposleni.ResumeLayout(false);
            this.tabZaposleni.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvZaposleni)).EndInit();
            this.tabKorisnici.ResumeLayout(false);
            this.tabKorisnici.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKorisnici)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTabControl tabovi;
        private MetroFramework.Controls.MetroTabPage tabZaposleni;
        private MetroFramework.Controls.MetroRadioButton radioKorisnickePodrske;
        private MetroFramework.Controls.MetroRadioButton radioProgramer;
        private MetroFramework.Controls.MetroRadioButton radioAdministrator;
        private MetroFramework.Controls.MetroRadioButton radioTehnickePodrske;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTabPage tabKorisnici;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroRadioButton radioFizicko;
        private MetroFramework.Controls.MetroRadioButton radioPravno;
        private MetroFramework.Controls.MetroButton btnObrisiOznacenog;
        private MetroFramework.Controls.MetroButton btnEkonomista;
        private MetroFramework.Controls.MetroButton btnDodajRadnika;
        private MetroFramework.Controls.MetroGrid dgvZaposleni;
        private MetroFramework.Controls.MetroButton btnObrisiKorisnika;
        private MetroFramework.Controls.MetroButton btnDodajKorisnika;
        private MetroFramework.Controls.MetroGrid dgvKorisnici;
        private MetroFramework.Controls.MetroButton btdDodajKorisnicke;
        private MetroFramework.Controls.MetroButton vidiNarudzbine;
        private MetroFramework.Controls.MetroButton dodajPaket;
        private MetroFramework.Controls.MetroButton btnDodajServer;
        private MetroFramework.Controls.MetroButton btnOdrServeri;
        private MetroFramework.Controls.MetroButton btnOdrPaketi;
        private MetroFramework.Controls.MetroButton btnHostovanja;
    }
}