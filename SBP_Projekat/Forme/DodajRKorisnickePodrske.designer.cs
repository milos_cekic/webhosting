﻿namespace WebHosting.Forme
{
    partial class DodajRKorisnickePodrske
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.dateIstekUgovora = new MetroFramework.Controls.MetroDateTime();
            this.radioOdredjenoNeodredjeo = new MetroFramework.Controls.MetroRadioButton();
            this.labelIstekUgovora = new MetroFramework.Controls.MetroLabel();
            this.datumZaposlenja = new MetroFramework.Controls.MetroDateTime();
            this.txtImeOca = new MetroFramework.Controls.MetroTextBox();
            this.txtPrezime = new MetroFramework.Controls.MetroTextBox();
            this.txtGodinaRodj = new MetroFramework.Controls.MetroTextBox();
            this.txtGodRadStaza = new MetroFramework.Controls.MetroTextBox();
            this.txtJmbg = new MetroFramework.Controls.MetroTextBox();
            this.txtLicnoIme = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.btnDodaj = new MetroFramework.Controls.MetroButton();
            this.btnOdustani = new MetroFramework.Controls.MetroButton();
            this.metroPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroPanel1
            // 
            this.metroPanel1.Controls.Add(this.dateIstekUgovora);
            this.metroPanel1.Controls.Add(this.radioOdredjenoNeodredjeo);
            this.metroPanel1.Controls.Add(this.labelIstekUgovora);
            this.metroPanel1.Controls.Add(this.datumZaposlenja);
            this.metroPanel1.Controls.Add(this.txtImeOca);
            this.metroPanel1.Controls.Add(this.txtPrezime);
            this.metroPanel1.Controls.Add(this.txtGodinaRodj);
            this.metroPanel1.Controls.Add(this.txtGodRadStaza);
            this.metroPanel1.Controls.Add(this.txtJmbg);
            this.metroPanel1.Controls.Add(this.txtLicnoIme);
            this.metroPanel1.Controls.Add(this.metroLabel7);
            this.metroPanel1.Controls.Add(this.metroLabel6);
            this.metroPanel1.Controls.Add(this.metroLabel5);
            this.metroPanel1.Controls.Add(this.metroLabel4);
            this.metroPanel1.Controls.Add(this.metroLabel3);
            this.metroPanel1.Controls.Add(this.metroLabel2);
            this.metroPanel1.Controls.Add(this.metroLabel1);
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(23, 63);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(481, 313);
            this.metroPanel1.TabIndex = 1;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // dateIstekUgovora
            // 
            this.dateIstekUgovora.Location = new System.Drawing.Point(181, 265);
            this.dateIstekUgovora.MinimumSize = new System.Drawing.Size(0, 29);
            this.dateIstekUgovora.Name = "dateIstekUgovora";
            this.dateIstekUgovora.Size = new System.Drawing.Size(190, 29);
            this.dateIstekUgovora.TabIndex = 18;
            // 
            // radioOdredjenoNeodredjeo
            // 
            this.radioOdredjenoNeodredjeo.AutoSize = true;
            this.radioOdredjenoNeodredjeo.Location = new System.Drawing.Point(24, 234);
            this.radioOdredjenoNeodredjeo.Name = "radioOdredjenoNeodredjeo";
            this.radioOdredjenoNeodredjeo.Size = new System.Drawing.Size(250, 15);
            this.radioOdredjenoNeodredjeo.TabIndex = 17;
            this.radioOdredjenoNeodredjeo.Text = "Da li se radi o radniku na odredjeno vreme?";
            this.radioOdredjenoNeodredjeo.UseSelectable = true;
            this.radioOdredjenoNeodredjeo.CheckedChanged += new System.EventHandler(this.radioOdredjenoNeodredjeo_CheckedChanged);
            // 
            // labelIstekUgovora
            // 
            this.labelIstekUgovora.AutoSize = true;
            this.labelIstekUgovora.Location = new System.Drawing.Point(17, 265);
            this.labelIstekUgovora.Name = "labelIstekUgovora";
            this.labelIstekUgovora.Size = new System.Drawing.Size(140, 19);
            this.labelIstekUgovora.TabIndex = 16;
            this.labelIstekUgovora.Text = "Datum isteka ugovora:";
            // 
            // datumZaposlenja
            // 
            this.datumZaposlenja.Location = new System.Drawing.Point(181, 185);
            this.datumZaposlenja.MinimumSize = new System.Drawing.Size(0, 29);
            this.datumZaposlenja.Name = "datumZaposlenja";
            this.datumZaposlenja.Size = new System.Drawing.Size(190, 29);
            this.datumZaposlenja.TabIndex = 15;
            // 
            // txtImeOca
            // 
            // 
            // 
            // 
            this.txtImeOca.CustomButton.Image = null;
            this.txtImeOca.CustomButton.Location = new System.Drawing.Point(168, 1);
            this.txtImeOca.CustomButton.Name = "";
            this.txtImeOca.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtImeOca.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtImeOca.CustomButton.TabIndex = 1;
            this.txtImeOca.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtImeOca.CustomButton.UseSelectable = true;
            this.txtImeOca.CustomButton.Visible = false;
            this.txtImeOca.Lines = new string[0];
            this.txtImeOca.Location = new System.Drawing.Point(181, 40);
            this.txtImeOca.MaxLength = 32767;
            this.txtImeOca.Name = "txtImeOca";
            this.txtImeOca.PasswordChar = '\0';
            this.txtImeOca.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtImeOca.SelectedText = "";
            this.txtImeOca.SelectionLength = 0;
            this.txtImeOca.SelectionStart = 0;
            this.txtImeOca.ShortcutsEnabled = true;
            this.txtImeOca.Size = new System.Drawing.Size(190, 23);
            this.txtImeOca.TabIndex = 14;
            this.txtImeOca.UseSelectable = true;
            this.txtImeOca.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtImeOca.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtPrezime
            // 
            // 
            // 
            // 
            this.txtPrezime.CustomButton.Image = null;
            this.txtPrezime.CustomButton.Location = new System.Drawing.Point(168, 1);
            this.txtPrezime.CustomButton.Name = "";
            this.txtPrezime.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtPrezime.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtPrezime.CustomButton.TabIndex = 1;
            this.txtPrezime.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtPrezime.CustomButton.UseSelectable = true;
            this.txtPrezime.CustomButton.Visible = false;
            this.txtPrezime.Lines = new string[0];
            this.txtPrezime.Location = new System.Drawing.Point(181, 67);
            this.txtPrezime.MaxLength = 32767;
            this.txtPrezime.Name = "txtPrezime";
            this.txtPrezime.PasswordChar = '\0';
            this.txtPrezime.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtPrezime.SelectedText = "";
            this.txtPrezime.SelectionLength = 0;
            this.txtPrezime.SelectionStart = 0;
            this.txtPrezime.ShortcutsEnabled = true;
            this.txtPrezime.Size = new System.Drawing.Size(190, 23);
            this.txtPrezime.TabIndex = 13;
            this.txtPrezime.UseSelectable = true;
            this.txtPrezime.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtPrezime.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtGodinaRodj
            // 
            // 
            // 
            // 
            this.txtGodinaRodj.CustomButton.Image = null;
            this.txtGodinaRodj.CustomButton.Location = new System.Drawing.Point(168, 1);
            this.txtGodinaRodj.CustomButton.Name = "";
            this.txtGodinaRodj.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtGodinaRodj.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtGodinaRodj.CustomButton.TabIndex = 1;
            this.txtGodinaRodj.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtGodinaRodj.CustomButton.UseSelectable = true;
            this.txtGodinaRodj.CustomButton.Visible = false;
            this.txtGodinaRodj.Lines = new string[0];
            this.txtGodinaRodj.Location = new System.Drawing.Point(181, 153);
            this.txtGodinaRodj.MaxLength = 32767;
            this.txtGodinaRodj.Name = "txtGodinaRodj";
            this.txtGodinaRodj.PasswordChar = '\0';
            this.txtGodinaRodj.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtGodinaRodj.SelectedText = "";
            this.txtGodinaRodj.SelectionLength = 0;
            this.txtGodinaRodj.SelectionStart = 0;
            this.txtGodinaRodj.ShortcutsEnabled = true;
            this.txtGodinaRodj.Size = new System.Drawing.Size(190, 23);
            this.txtGodinaRodj.TabIndex = 12;
            this.txtGodinaRodj.UseSelectable = true;
            this.txtGodinaRodj.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtGodinaRodj.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtGodRadStaza
            // 
            // 
            // 
            // 
            this.txtGodRadStaza.CustomButton.Image = null;
            this.txtGodRadStaza.CustomButton.Location = new System.Drawing.Point(168, 1);
            this.txtGodRadStaza.CustomButton.Name = "";
            this.txtGodRadStaza.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtGodRadStaza.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtGodRadStaza.CustomButton.TabIndex = 1;
            this.txtGodRadStaza.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtGodRadStaza.CustomButton.UseSelectable = true;
            this.txtGodRadStaza.CustomButton.Visible = false;
            this.txtGodRadStaza.Lines = new string[0];
            this.txtGodRadStaza.Location = new System.Drawing.Point(181, 125);
            this.txtGodRadStaza.MaxLength = 32767;
            this.txtGodRadStaza.Name = "txtGodRadStaza";
            this.txtGodRadStaza.PasswordChar = '\0';
            this.txtGodRadStaza.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtGodRadStaza.SelectedText = "";
            this.txtGodRadStaza.SelectionLength = 0;
            this.txtGodRadStaza.SelectionStart = 0;
            this.txtGodRadStaza.ShortcutsEnabled = true;
            this.txtGodRadStaza.Size = new System.Drawing.Size(190, 23);
            this.txtGodRadStaza.TabIndex = 11;
            this.txtGodRadStaza.UseSelectable = true;
            this.txtGodRadStaza.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtGodRadStaza.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtJmbg
            // 
            // 
            // 
            // 
            this.txtJmbg.CustomButton.Image = null;
            this.txtJmbg.CustomButton.Location = new System.Drawing.Point(168, 1);
            this.txtJmbg.CustomButton.Name = "";
            this.txtJmbg.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtJmbg.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtJmbg.CustomButton.TabIndex = 1;
            this.txtJmbg.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtJmbg.CustomButton.UseSelectable = true;
            this.txtJmbg.CustomButton.Visible = false;
            this.txtJmbg.Lines = new string[0];
            this.txtJmbg.Location = new System.Drawing.Point(181, 95);
            this.txtJmbg.MaxLength = 32767;
            this.txtJmbg.Name = "txtJmbg";
            this.txtJmbg.PasswordChar = '\0';
            this.txtJmbg.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtJmbg.SelectedText = "";
            this.txtJmbg.SelectionLength = 0;
            this.txtJmbg.SelectionStart = 0;
            this.txtJmbg.ShortcutsEnabled = true;
            this.txtJmbg.Size = new System.Drawing.Size(190, 23);
            this.txtJmbg.TabIndex = 10;
            this.txtJmbg.UseSelectable = true;
            this.txtJmbg.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtJmbg.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtLicnoIme
            // 
            // 
            // 
            // 
            this.txtLicnoIme.CustomButton.Image = null;
            this.txtLicnoIme.CustomButton.Location = new System.Drawing.Point(168, 1);
            this.txtLicnoIme.CustomButton.Name = "";
            this.txtLicnoIme.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtLicnoIme.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtLicnoIme.CustomButton.TabIndex = 1;
            this.txtLicnoIme.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtLicnoIme.CustomButton.UseSelectable = true;
            this.txtLicnoIme.CustomButton.Visible = false;
            this.txtLicnoIme.Lines = new string[0];
            this.txtLicnoIme.Location = new System.Drawing.Point(181, 11);
            this.txtLicnoIme.MaxLength = 32767;
            this.txtLicnoIme.Name = "txtLicnoIme";
            this.txtLicnoIme.PasswordChar = '\0';
            this.txtLicnoIme.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtLicnoIme.SelectedText = "";
            this.txtLicnoIme.SelectionLength = 0;
            this.txtLicnoIme.SelectionStart = 0;
            this.txtLicnoIme.ShortcutsEnabled = true;
            this.txtLicnoIme.Size = new System.Drawing.Size(190, 23);
            this.txtLicnoIme.TabIndex = 9;
            this.txtLicnoIme.UseSelectable = true;
            this.txtLicnoIme.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtLicnoIme.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(17, 67);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(60, 19);
            this.metroLabel7.TabIndex = 8;
            this.metroLabel7.Text = "Prezime:";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(17, 95);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(45, 19);
            this.metroLabel6.TabIndex = 7;
            this.metroLabel6.Text = "Jmbg:";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(17, 185);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(116, 19);
            this.metroLabel5.TabIndex = 6;
            this.metroLabel5.Text = "Datum zaposlenja:";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(17, 157);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(106, 19);
            this.metroLabel4.TabIndex = 5;
            this.metroLabel4.Text = "Godina rodjenja:";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(17, 129);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(134, 19);
            this.metroLabel3.TabIndex = 4;
            this.metroLabel3.Text = "Godine radnog staza:";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(17, 39);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(59, 19);
            this.metroLabel2.TabIndex = 3;
            this.metroLabel2.Text = "Ime oca:";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(17, 11);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(68, 19);
            this.metroLabel1.TabIndex = 2;
            this.metroLabel1.Text = "Licno ime:";
            // 
            // btnDodaj
            // 
            this.btnDodaj.Location = new System.Drawing.Point(429, 406);
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(75, 23);
            this.btnDodaj.TabIndex = 10;
            this.btnDodaj.Text = "Dodaj";
            this.btnDodaj.UseSelectable = true;
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // btnOdustani
            // 
            this.btnOdustani.Location = new System.Drawing.Point(341, 407);
            this.btnOdustani.Name = "btnOdustani";
            this.btnOdustani.Size = new System.Drawing.Size(75, 23);
            this.btnOdustani.TabIndex = 9;
            this.btnOdustani.Text = "Odustani";
            this.btnOdustani.UseSelectable = true;
            this.btnOdustani.Click += new System.EventHandler(this.btnOdustani_Click_1);
            // 
            // DodajRKorisnickePodrske
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(547, 493);
            this.Controls.Add(this.btnDodaj);
            this.Controls.Add(this.btnOdustani);
            this.Controls.Add(this.metroPanel1);
            this.Name = "DodajRKorisnickePodrske";
            this.Text = "DodajRKorisnickePodrske";
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroDateTime dateIstekUgovora;
        private MetroFramework.Controls.MetroRadioButton radioOdredjenoNeodredjeo;
        private MetroFramework.Controls.MetroLabel labelIstekUgovora;
        private MetroFramework.Controls.MetroDateTime datumZaposlenja;
        private MetroFramework.Controls.MetroTextBox txtImeOca;
        private MetroFramework.Controls.MetroTextBox txtPrezime;
        private MetroFramework.Controls.MetroTextBox txtGodinaRodj;
        private MetroFramework.Controls.MetroTextBox txtGodRadStaza;
        private MetroFramework.Controls.MetroTextBox txtJmbg;
        private MetroFramework.Controls.MetroTextBox txtLicnoIme;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroButton btnDodaj;
        private MetroFramework.Controls.MetroButton btnOdustani;
    }
}