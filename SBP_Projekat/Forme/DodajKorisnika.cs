﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using WebHosting.DTOs;
using WebHosting.Entiteti;

namespace WebHosting.Forme
{
    public partial class DodajKorisnika : MetroFramework.Forms.MetroForm
    {

        private bool pravnoPage = false;
        private List<EkonomistaTehnickePodrskeView> listaEkonomista;
        private List<KontaktOsoba> listaKontakata;

        private bool kreirajPravno = true;
        private PravnoLiceView pravnoLice;

        private bool kreirajFizicko = true;
        private FizickoLiceView fizicko;


        public DodajKorisnika()
        {
            InitializeComponent();
            listaEkonomista = new List<EkonomistaTehnickePodrskeView>();
            listaKontakata = new List<KontaktOsoba>();
        }

        public DodajKorisnika(int id, bool pravnoPage)            
        {
            //ako je adminPage onda za administratora update radimo
            //ako nije onda za programera
            listaEkonomista = new List<EkonomistaTehnickePodrskeView>();
            listaKontakata = new List<KontaktOsoba>();
            this.pravnoPage = pravnoPage;
            if (pravnoPage)
            {
                KorisnikTab.SelectedTab = PravnoLicePage;
                if (KorisnikTab.SelectedTab == PravnoLicePage)
                {
                    kreirajPravno = false;
                    pravnoLice = DTOManager.getPravnoLiceView(id);
                    PopuniKontrole();
                    PrikaziKontakte();
                    PrikaziEkonomiste();
                }
            }
            else
            {
                KorisnikTab.SelectedTab = FizickoLicePage;
                kreirajFizicko = false;
                fizicko = DTOManager.getFizickoLiceView(id);
                PopuniKontrole();
                PrikaziKontakte();
                PrikaziEkonomiste();
            }
        }


        public void PopuniKontrole()
        {
            if (pravnoPage)
            {
                txtPIB.Text = pravnoLice.PIB.ToString();
                txtNaziv.Text = pravnoLice.Naziv;
                txtAdresa.Text = pravnoLice.Adresa;
                datumOd.Value = pravnoLice.Od;
                datumDo.Value = pravnoLice.Do;


                dgvKontakti.DataSource = listaKontakata.ToList();
                dgvKontakti.Columns[0].HeaderText = "Kontakt osoba";

                //foreach (KontaktOsoba os in pravnoLice.KontaktOsobe)
                //{
                //    listaKontakata.Add(os);
                //}
            }
            else
            {
                txtLicnoIme.Text = fizicko.LicnoIme;
                txtPrezime.Text = fizicko.Prezime;
                txtJmbg.Text = fizicko.JMBG;
                txtAdresa.Text = fizicko.Adresa;
                datumOd.Value = fizicko.Od;
                datumDo.Value = fizicko.Do;


                dgvKontakti.DataSource = listaKontakata.ToList();
                dgvKontakti.Columns[0].HeaderText = "Kontakt osoba";

                //foreach (KontaktOsoba os in fizicko.KontaktOsobe)
                //{
                //    listaKontakata.Add(os);
                //}
            }
        }


        private void FizickoLicePage_Click(object sender, EventArgs e)
        {

        }

        private void DodajKorisnika_Load(object sender, EventArgs e)
        {
            dgvEkonomisti.DataSource = DTOManager.getRadnikeTehnickePodrskeEntiteti();
        }

        public void PrikaziEkonomiste()
        {
            dgvEkonomisti.DataSource = listaEkonomista.ToList();
            dgvEkonomisti.Columns[0].HeaderText = "Lista Ekonomista";
        }
        public void PrikaziKontakte()
        {
            dgvKontakti.DataSource = listaKontakata.ToList();
            dgvKontakti.Columns[0].HeaderText = "Lista kontakt osoba";
        }




        public bool Validacija()
        {
            //neka za sad ovako

            //ovo ne treba ovde jer validacija moze da bude pogresna ako je korisnik pravno lice recimo
            //if (string.IsNullOrEmpty(txtLicnoIme.Text))
            //{
            //    return false;
            //}
            //else if (string.IsNullOrEmpty(txtPrezime.Text))
            //{
            //    return false;
            //}
            //else if (string.IsNullOrEmpty(txtJmbg.Text))
            //{
            //    return false;
            //}
            if (string.IsNullOrEmpty(txtAdresa.Text))
            {
                return false;
            }
            else if (string.IsNullOrEmpty(datumOd.Text))
            {
                return false;
            }
            else if (string.IsNullOrEmpty(datumDo.Text))
            {
                return false;
            }
          
            return true;
        }

        private void btnOdustani_Click(object sender, EventArgs e)
        {
            this.Close();
            DialogResult = DialogResult.Cancel;
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            // Napomena: kad se prolazi kroz debugger sve je u redu,
            // kad se pokrene bez debuggiranja ne upisuje u tabele pravno/fizicko i kontaktosobe
            //problem bi se resio tako sto svuda gde se poziva load prebacimo u get
            //ne mozemo pronaci konkretni load koji pravi problem
            if (KorisnikTab.SelectedTab == PravnoLicePage)
            {
                if (kreirajPravno)
                {
                    PravnoLice pravno = new PravnoLice()
                    {
                        Od = datumOd.Value,
                        Do = datumDo.Value,
                        Adresa = txtAdresa.Text,
                        PIB = long.Parse(txtPIB.Text),
                        Naziv = txtNaziv.Text
                    };

                    foreach (DataGridViewRow row in dgvEkonomisti.SelectedRows)
                    {
                        int id = ((EkonomistaTehnickePodrske)dgvEkonomisti.SelectedRows[0].DataBoundItem).Id;//malo da se ispravi ali moze ovako da se uzme iz dgv
                        if (id < 1)
                        {
                            return;
                        }
                        pravno.KorespondovaoSaEkonomistom = DTOManager.getRadnikTehnickePodrske(id);
                    }

                    DTOManager.AddPravnoLice(pravno);

                    if (listaKontakata.Count != 0)
                    {
                        foreach (var kontakt in listaKontakata)
                        {
                            //pitanje je da li ovako hoce
                            if (pravno.KontaktOsobe.Where(x => x.KorisnikZaKogPamtimo == pravno && x.LicnoIme == kontakt.LicnoIme && x.Prezime == kontakt.Prezime).ToList().Count() > 0)
                                continue;
                            else
                            {
                                DTOManager.addOsobaKorisnik(pravno.Id, kontakt);
                            }
                        }
                        foreach (var osoba in pravno.KontaktOsobe)
                        {
                            if (listaKontakata.Where(x => x.KorisnikZaKogPamtimo == osoba.KorisnikZaKogPamtimo && x.LicnoIme == osoba.LicnoIme && x.Prezime == osoba.Prezime).ToList().Count() == 0)
                                DTOManager.deleteKontaktKorisnik(osoba.Id);
                        }

                    }
                    this.DialogResult = DialogResult.OK;

                }
                else
                {
                    if (!Validacija())
                        return;

                    pravnoLice.PIB = long.Parse(txtPIB.Text);
                    pravnoLice.Naziv = txtNaziv.Text;
                    pravnoLice.Od = datumOd.Value;
                    pravnoLice.Do = datumDo.Value;
                    pravnoLice.Adresa = txtAdresa.Text;


                    foreach (DataGridViewRow row in dgvEkonomisti.SelectedRows)
                    {
                        int id = ((EkonomistaTehnickePodrske)dgvEkonomisti.SelectedRows[0].DataBoundItem).Id;//malo da se ispravi ali moze ovako da se uzme iz dgv
                        if (id < 1)
                        {
                            return;
                        }
                        pravnoLice.Ekonomista = DTOManager.getRadnikTehnickePodrske(id);
                    }

                    //if (listaKontakata.Count != 0)
                    //{
                    //    foreach (var kontakt in listaKontakata)
                    //    {
                    //        if (pravnoLice.KontaktOsobe.Where(x => x.KorisnikZaKogPamtimo.Id == pravnoLice.Id && x.LicnoIme == kontakt.LicnoIme && x.Prezime == kontakt.Prezime).ToList().Count() > 0)
                    //            continue;
                    //        else
                    //        {
                    //            DTOManager.addKontaktKorisnik(pravnoLice.Id, kontakt);
                    //            //                            DTOManager.addSistemAdministrator(administrator.Id, sistem.Value);
                    //        }
                    //    }
                    //    //sta se ovde htelo???
                    //    foreach (var kontakt in pravnoLice.KontaktOsobe)
                    //    {
                    //        if (listaKontakata.Where(x => x.KorisnikZaKogPamtimo == kontakt.KorisnikZaKogPamtimo && x.LicnoIme == kontakt.LicnoIme && x.Prezime == kontakt.Prezime).ToList().Count() == 0)
                    //            DTOManager.deleteKontaktKorisnik(kontakt.Id);
                    //    }

                    //}
                    DTOManager.updatePravnoLice(pravnoLice);
                    this.DialogResult = DialogResult.OK;
                }
            }
            else
            {
                if (kreirajFizicko)
                {
                    if (!Validacija())
                        return;


                    FizickoLice novo = new FizickoLice()
                    {
                        LicnoIme = txtLicnoIme.Text,
                        Prezime = txtPrezime.Text,
                        JMBG = txtJmbg.Text,
                        Od = datumOd.Value,
                        Do = datumDo.Value,
                        Adresa = txtAdresa.Text

                    };

                    foreach (DataGridViewRow row in dgvEkonomisti.SelectedRows)
                    {
                        int id = ((EkonomistaTehnickePodrske)dgvEkonomisti.SelectedRows[0].DataBoundItem).Id;//malo da se ispravi ali moze ovako da se uzme iz dgv
                        if (id < 1)
                        {
                            return;
                        }
                        novo.KorespondovaoSaEkonomistom = DTOManager.getRadnikTehnickePodrske(id);
                    }

                    DTOManager.AddFizickoLice(novo);

                    if (listaKontakata.Count != 0)
                    {
                        foreach (var kontakt in listaKontakata)
                        {
                            //pitanje je da li ovako hoce
                            if (novo.KontaktOsobe.Where(x => x.KorisnikZaKogPamtimo == novo && x.LicnoIme == kontakt.LicnoIme && x.Prezime == kontakt.Prezime).ToList().Count() > 0)
                                continue;
                            else
                            {
                                DTOManager.addOsobaKorisnik(novo.Id, kontakt);
                            }
                        }
                        foreach (var osoba in novo.KontaktOsobe)
                        {
                            if (listaKontakata.Where(x => x.KorisnikZaKogPamtimo == osoba.KorisnikZaKogPamtimo && x.LicnoIme == osoba.LicnoIme && x.Prezime == osoba.Prezime).ToList().Count() == 0)
                                DTOManager.deleteKontaktKorisnik(osoba.Id);
                        }

                    }
                    this.DialogResult = DialogResult.OK;


                }
                else
                {
                    if (!Validacija())
                        return;

                    fizicko.LicnoIme = txtLicnoIme.Text;
                    fizicko.Prezime = txtPrezime.Text;
                    fizicko.JMBG = txtJmbg.Text;
                    fizicko.Od = datumOd.Value;
                    fizicko.Do = datumDo.Value;
                    fizicko.Adresa = txtAdresa.Text;



                    foreach (DataGridViewRow row in dgvEkonomisti.SelectedRows)
                    {
                        int id = ((EkonomistaTehnickePodrske)dgvEkonomisti.SelectedRows[0].DataBoundItem).Id;//malo da se ispravi ali moze ovako da se uzme iz dgv
                        if (id < 1)
                        {
                            return;
                        }
                        fizicko.Ekonomista = DTOManager.getRadnikTehnickePodrske(id);
                    }


                    //if (listaKontakata.Count != 0)
                    //{
                    //    foreach (var kontakt in listaKontakata)
                    //    {
                    //        if (fizicko.KontaktOsobe.Where(x => x.KorisnikZaKogPamtimo.Id == fizicko.Id && x.LicnoIme == kontakt.LicnoIme && x.Prezime == kontakt.Prezime).ToList().Count() > 0)
                    //            continue;
                    //        else
                    //        {
                    //            DTOManager.addKontaktKorisnik(fizicko.Id, kontakt);
                    //            //                            DTOManager.addSistemAdministrator(administrator.Id, sistem.Value);
                    //        }
                    //    }
                    //    //sta se ovde htelo???
                    //    foreach (var kontakt in fizicko.KontaktOsobe)
                    //    {
                    //        if (listaKontakata.Where(x => x.KorisnikZaKogPamtimo == kontakt.KorisnikZaKogPamtimo && x.LicnoIme == kontakt.LicnoIme && x.Prezime == kontakt.Prezime).ToList().Count() == 0)
                    //            DTOManager.deleteKontaktKorisnik(kontakt.Id);
                    //    }

                    //}
                    DTOManager.updateFizickoLice(fizicko);
//                    DTOManager.updatePravnoLice(pravnoLice);
                    this.DialogResult = DialogResult.OK;
                }

            }

        }

        private void txtLicnoIme_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsLetter(e.KeyChar) && !Char.IsControl(e.KeyChar))
                e.Handled = true;

        }

        private void txtPrezime_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsLetter(e.KeyChar) && !Char.IsControl(e.KeyChar))
                e.Handled = true;

        }

        private void txtJmbg_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
                e.Handled = true;

        }

        private void txtPIB_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
                e.Handled = true;

        }

        private void btnDodajKontakt_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtImeKontakta.Text))
            {
                return;
            }
            if (string.IsNullOrEmpty(txtPrezimeKontakta.Text))
            {
                return;
            }
            if (string.IsNullOrEmpty(txtBrojTelefonaKontakta.Text))
            {
                return;
            }

            //ok ovde dodajemo samo u listu ali tamo kada budemo dodavali u bazu cekamo da se kreira prvo korisnik da bi znali
            //njegov id
            KontaktOsoba osoba = new KontaktOsoba
            {
                LicnoIme = txtImeKontakta.Text,
                Prezime = txtPrezimeKontakta.Text,
                BrojTelefona = long.Parse(txtBrojTelefonaKontakta.Text)
            };
            //za id korisnika cemo posle
            listaKontakata.Add(osoba);
            txtImeKontakta.Text = "";
            txtPrezimeKontakta.Text = "";
            txtBrojTelefonaKontakta .Text= "";
            PrikaziKontakte();
        }

        private void btnObrisiKontakt_Click(object sender, EventArgs e)
        {

            if (dgvKontakti.SelectedRows.Count == 0)
            {
                return;
            }

            foreach (DataGridViewRow row in dgvKontakti.SelectedRows)
            {
                listaKontakata.Remove(((KontaktOsoba)dgvKontakti.SelectedRows[0].DataBoundItem));

            }
            PrikaziKontakte();
      }


    }
}

