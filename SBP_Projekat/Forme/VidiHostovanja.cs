﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WebHosting.DTOs;
using WebHosting.Entiteti;
using NHibernate;

namespace WebHosting.Forme
{
    public partial class VidiHostovanja : MetroFramework.Forms.MetroForm
    {
        public VidiHostovanja()
        {
            InitializeComponent();
        }

        private void VidiHostovanja_Load(object sender, EventArgs e)
        {
            dgvPostojeceTrenutnoOdrzavaniServeri.DataSource = DTOManager.getListaHostujeSeView(false, 1);

            dgvServeri.DataSource = DTOManager.getListaServerView(true, "");
            dgvDostupniPaketi.DataSource = DTOManager.getListaPaket(true, "", "", "Da");
        }

        private void BtnObrisiIzavrano_Click(object sender, EventArgs e)
        {

        }

        private void BtnOdustani_Click(object sender, EventArgs e)
        {
            this.Close();
            DialogResult = DialogResult.Cancel;
        }
    }
}
