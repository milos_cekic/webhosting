﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WebHosting.DTOs;

namespace WebHosting.Forme
{
    public partial class Glavna : MetroFramework.Forms.MetroForm
    {
        public Glavna()
        {
            InitializeComponent();
            radioPravno.Checked = true;
            this.tabovi.SelectedTab = tabKorisnici;
        }

        #region funkcije

        public void Prikazi()
        {
            if (this.tabovi.SelectedTab == tabZaposleni)
            {
                
                if (this.radioProgramer.Checked)
                {
                    
                    dgvZaposleni.DataSource = DTOManager.getRadnikeTehnickeStruke("Programer");
                    dgvZaposleni.Columns[9].Visible = false;
                    dgvZaposleni.Columns[8].Visible = false;
                    
                }
                else if(this.radioAdministrator.Checked)
                {
                    dgvZaposleni.DataSource = DTOManager.getRadnikeTehnickeStruke("Administrator");
                    dgvZaposleni.Columns[0].Visible = false;
                    dgvZaposleni.Columns[10].Visible = false;
                    dgvZaposleni.Columns[9].Visible = false;
                    dgvZaposleni.Columns[8].Visible = false;
                }
                else if (this.radioTehnickePodrske.Checked)
                {
                    dgvZaposleni.DataSource = DTOManager.getRadnikeTehnickePodrske();
                    dgvZaposleni.Columns[9].Visible = true;
                    dgvZaposleni.Columns[8].Visible = true;
                    
                }
                else 
                {
                    dgvZaposleni.DataSource = DTOManager.getRadnikeKorisnickePodrske();
                    dgvZaposleni.Columns[9].Visible = false;
                    dgvZaposleni.Columns[8].Visible = true;
                }
            }
            else if(this.tabovi.SelectedTab == tabKorisnici)
            {
                if (this.radioPravno.Checked)
                {
                    dgvKorisnici.DataSource = DTOManager.getPravnaLicaa();
                }
                else
                {
                    dgvKorisnici.DataSource = DTOManager.getFizickaLicaa();
                }
            }
            //else if (this.tabovi.SelectedTab == tabZaposleniii)
            //{
            //    if (this.radioMenadzer.Checked == true)
            //    {
            //        dgvZaposleni.DataSource = DTOManager.getMenadzere();
            //    }
            //    else if (this.radioFizicko.Checked == true)
            //    {
            //        dgvZaposleni.DataSource = DTOManager.getFizickoObezbedjenje();
            //    }
            //    else if (this.radioTehnicka.Checked == true)
            //    {
            //        dgvZaposleni.DataSource = DTOManager.getTehnicari();
            //    }
            //}
            //else if (this.tabovi.SelectedTab == tabRegCentriii)
            //{
            //    dgvRegCentri.DataSource = DTOManager.getCentre();
            //    dgvRegCentri.Columns[2].Visible = false;
            //    dgvRegCentri.Columns[3].Visible = false;
            //}
        }

        #endregion

        private void Glavna_Load(object sender, EventArgs e)
        {
            this.Prikazi();
            tabovi.SelectedTab = tabZaposleni;
        }

        private void radioTehnickePodrske_CheckedChanged(object sender, EventArgs e)
        {
            this.Prikazi();
        }

        private void radioKorisnickePodrske_CheckedChanged(object sender, EventArgs e)
        {
            this.Prikazi();
        }

        private void radioProgramer_CheckedChanged(object sender, EventArgs e)
        {
            this.Prikazi();
        }

        private void radioAdministrator_CheckedChanged(object sender, EventArgs e)
        {
            this.Prikazi();
        }

        private void btnDodajRadnika_Click(object sender, EventArgs e)
        {

            
            if (radioAdministrator.Checked)
            {
                var Forma = new DodajRTehnickeStruke();
                Forma.ShowDialog();
            }
            else if (radioProgramer.Checked)
            {
                var Forma = new DodajRTehnickeStruke();
                Forma.ShowDialog();
            }

            else if (radioKorisnickePodrske.Checked)
            {
                var Forma = new DodajRKorisnickePodrske();
                Forma.ShowDialog();
            }
            else if(radioTehnickePodrske.Checked)
            {
                var Forma = new DodajRTehnickePodrske();
            }
            Prikazi();
        }

        private void btnObrisiOznacenog_Click(object sender, EventArgs e)
        {
            if (dgvZaposleni.SelectedRows.Count == 0)
            {
                return;
            }
            if (radioProgramer.Checked)
            {
                foreach (DataGridViewRow row in dgvZaposleni.SelectedRows)
                {
                    int id = ((RadnikView)dgvZaposleni.SelectedRows[0].DataBoundItem).Id;//malo da se ispravi ali moze ovako da se uzme iz dgv
                    if (id < 1)
                    {
                        return;
                    }
                    MessageBox.Show(id.ToString());
                    DTOManager.deleteRadnikTehnickeStruke(id);
                    
                }

            }
            else if (radioAdministrator.Checked)
            {
                foreach (DataGridViewRow row in dgvZaposleni.SelectedRows)
                {
                    int id = ((RadnikView)dgvZaposleni.SelectedRows[0].DataBoundItem).Id;//malo da se ispravi ali moze ovako da se uzme iz dgv
                    if (id < 1)
                    {
                        return;
                    }
                    MessageBox.Show(id.ToString());
                    DTOManager.deleteRadnikTehnickeStruke(id);

                }

            }
            else if (radioTehnickePodrske.Checked)
            {
                foreach (DataGridViewRow row in dgvZaposleni.SelectedRows)
                {
                    int id = ((RadnikView)dgvZaposleni.SelectedRows[0].DataBoundItem).Id;//malo da se ispravi ali moze ovako da se uzme iz dgv
                    if (id < 1)
                    {
                        return;
                    }
                    MessageBox.Show(id.ToString());
                    DTOManager.deleteRadnikTehnickePodrske(id);
                }

            }
            else
            {
                foreach (DataGridViewRow row in dgvZaposleni.SelectedRows)
                {
                    int id = ((RadnikView)dgvZaposleni.SelectedRows[0].DataBoundItem).Id;//malo da se ispravi ali moze ovako da se uzme iz dgv
                    if (id < 1)
                    {
                        return;
                    }
                    MessageBox.Show(id.ToString());
                    DTOManager.deleteRadnikKorisnickePodrske(id);
                }

            }

            //}
            Prikazi();
        }

        private void btnDodajKorisnika_Click(object sender, EventArgs e)
        {
            var Forma = new DodajKorisnika();
            Forma.ShowDialog();
        }

        private void btnEkonomista_Click(object sender, EventArgs e)
        {
            var Forma = new DodajRTehnickePodrske();
            Forma.ShowDialog();
        }

        private void btdDodajKorisnicke_Click(object sender, EventArgs e)
        {
            var Forma = new DodajRKorisnickePodrske();
            Forma.ShowDialog();

        }

        private void dgvZaposleni_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
                if (radioAdministrator.Checked)
                {

                    var Forma = new DodajRTehnickeStruke(((RadnikView)dgvZaposleni.Rows[e.RowIndex].DataBoundItem).Id, true);
                    DialogResult dr = Forma.ShowDialog();
                    if (dr == DialogResult.OK)
                        Prikazi();
                }
                else if (this.radioProgramer.Checked)
                {
                    //dodao bih jos jedan koji ce da kaze da li je u pitanju administrator ili programer
                    var Forma = new DodajRTehnickeStruke(((RadnikView)dgvZaposleni.Rows[e.RowIndex].DataBoundItem).Id, false);
                    DialogResult dr = Forma.ShowDialog();
                    if (dr == DialogResult.OK)
                        Prikazi();
                }
                else if (this.radioKorisnickePodrske.Checked)
                {
                    var Forma = new DodajRKorisnickePodrske(((RadnikView)dgvZaposleni.Rows[e.RowIndex].DataBoundItem).Id);
                    DialogResult dr = Forma.ShowDialog();
                    if (dr == DialogResult.OK)
                        Prikazi();
                }
                else
                {
                    var Forma = new DodajRTehnickePodrske(((RadnikView)dgvZaposleni.Rows[e.RowIndex].DataBoundItem).Id);
                    DialogResult dr = Forma.ShowDialog();
                    if (dr == DialogResult.OK)
                        Prikazi();

                }
        }

        private void dgvKorisnici_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (radioPravno.Checked)
            {

                var Forma = new DodajKorisnika(((KorisnikView)dgvKorisnici.Rows[e.RowIndex].DataBoundItem).Id, true);
                DialogResult dr = Forma.ShowDialog();
                if (dr == DialogResult.OK)
                    Prikazi();
            }
            else
            {
                var Forma = new DodajKorisnika(((KorisnikView)dgvKorisnici.Rows[e.RowIndex].DataBoundItem).Id, false);
                DialogResult dr = Forma.ShowDialog();
                if (dr == DialogResult.OK)
                    Prikazi();
            }
        }

        private void btnObrisiKorisnika_Click(object sender, EventArgs e)
        {
            if (dgvKorisnici.SelectedRows.Count == 0)
            {
                return;
            }
            if (radioPravno.Checked)
            {
                foreach (DataGridViewRow row in dgvZaposleni.SelectedRows)
                {
                    int id = ((PravnoLiceView)dgvZaposleni.SelectedRows[0].DataBoundItem).Id;//malo da se ispravi ali moze ovako da se uzme iz dgv
                    if (id < 1)
                    {
                        return;
                    }
                    MessageBox.Show(id.ToString());
                    DTOManager.deletePravnoLice(id);
                }

            }
            else
            {
                foreach (DataGridViewRow row in dgvZaposleni.SelectedRows)
                {
                    int id = ((FizickoLiceView)dgvZaposleni.SelectedRows[0].DataBoundItem).Id;//malo da se ispravi ali moze ovako da se uzme iz dgv
                    if (id < 1)
                    {
                        return;
                    }
                    MessageBox.Show(id.ToString());
                    DTOManager.deleteFizickoLice(id);
                }

            }

        }

        private void DodajPaket_Click(object sender, EventArgs e)
        {
            var Forma = new DodajPaket();
            Forma.ShowDialog();
        }

        private void VidiNarudzbine_Click(object sender, EventArgs e)
        {
            var Forma = new VidiNarudzbine();
            Forma.ShowDialog();
        }

        private void RadioPravno_CheckedChanged(object sender, EventArgs e)
        {
            this.Prikazi();
        }

        private void RadioFizicko_CheckedChanged(object sender, EventArgs e)
        {
            this.Prikazi();
        }

        private void BtnDodajServer_Click(object sender, EventArgs e)
        {
            var Forma = new DodajServer();
            Forma.ShowDialog();
        }

        private void BtnOdrPaketi_Click(object sender, EventArgs e)
        {
            var Forma = new VidiOdrzavanjePaketa();
            Forma.ShowDialog();
        }

        private void BtnOdrServeri_Click(object sender, EventArgs e)
        {
            var Forma = new VidiOdrzavanjeServera();
            Forma.ShowDialog();
        }

        private void BtnHostovanja_Click(object sender, EventArgs e)
        {
            var Forma = new VidiHostovanja();
            Forma.ShowDialog();
        }
    }
}
