﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WebHosting.DTOs;
using WebHosting.Entiteti;

namespace WebHosting.Forme
{
    public partial class DodajRKorisnickePodrske : MetroFramework.Forms.MetroForm
    {

        #region Atributi

        private bool kreirajRKorisnickePodrske = true;
        private RadnikView rkorisnicke;

        #endregion

        #region Konstruktori

        public DodajRKorisnickePodrske()
        {
            InitializeComponent();
            kreirajRKorisnickePodrske = true;
            dateIstekUgovora.Visible = false;
 
        }

        public DodajRKorisnickePodrske(int id)            
        {
            InitializeComponent();
            dateIstekUgovora.Visible = false;
            kreirajRKorisnickePodrske = false;
            rkorisnicke = DTOManager.getRadnikView(id);
            PopuniKontrole();
 

        }

        public void PopuniKontrole()
        {
            txtLicnoIme.Text = rkorisnicke.Licno_ime;
            txtJmbg.Text = rkorisnicke.Jmbg;
            txtPrezime.Text = rkorisnicke.Prezime;
            txtImeOca.Text = rkorisnicke.Ime_oca;
            txtGodRadStaza.Text = rkorisnicke.Godine_radnog_staza.ToString();
            txtGodinaRodj.Text = rkorisnicke.Godina_rodjenja.ToString();
            datumZaposlenja.Value = rkorisnicke.DatumZaposlenja;

            if (rkorisnicke.Datum_isteka_ugovora is DateTime)
            {
                dateIstekUgovora.Visible = true;
                dateIstekUgovora.Value = (DateTime)rkorisnicke.Datum_isteka_ugovora;
            }
        }

        #endregion

        private void radioOdredjenoNeodredjeo_CheckedChanged(object sender, EventArgs e)
        {
            if (radioOdredjenoNeodredjeo.Checked)
            {
                labelIstekUgovora.Visible = true;
                dateIstekUgovora.Visible = true;
            }
            else
            {
                labelIstekUgovora.Visible = false;
                dateIstekUgovora.Visible = false;
            }
        }

        
        private void btnOdustani_Click_1(object sender, EventArgs e)
        {
            this.Close();
            DialogResult = DialogResult.Cancel;
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            if (kreirajRKorisnickePodrske)
            {
                //if (!Validacija())
                //    return;

                Radnik a = new Radnik();

                a.Licno_ime = txtLicnoIme.Text;
                a.Prezime = txtPrezime.Text;
                a.Ime_oca = txtImeOca.Text;
                a.Jmbg = txtJmbg.Text;
                a.Godina_rodjenja = Int32.Parse(txtGodinaRodj.Text);
                a.Godine_radnog_staza = Int32.Parse(txtGodRadStaza.Text);
                a.Datum_zaposlenja = datumZaposlenja.Value;

                if (radioOdredjenoNeodredjeo.Checked)
                {
                    a.R_neodredjeno = "Ne";
                    a.R_odredjeno = "Da";
                    a.Datum_isteka_ugovora = dateIstekUgovora.Value;
                }
                else
                {
                    a.R_neodredjeno = "Da";
                    a.R_odredjeno = "Ne";

                }

                a.R_korisnickePodrske = "Da";

                DTOManager.AddRadnikKorisnickePodrske(a);

                //                DTOManager.updateRadnikTehnickePodrske(ekonomista);//?
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                //if (!Validacija())
                //    return;

                rkorisnicke.Licno_ime = txtLicnoIme.Text;
                rkorisnicke.Prezime = txtPrezime.Text;
                rkorisnicke.Ime_oca = txtImeOca.Text;
                rkorisnicke.Jmbg = txtJmbg.Text;
                rkorisnicke.Godina_rodjenja = Int32.Parse(txtGodinaRodj.Text);
                rkorisnicke.Godine_radnog_staza = Int32.Parse(txtGodRadStaza.Text);
                rkorisnicke.DatumZaposlenja = datumZaposlenja.Value;


                if (dateIstekUgovora.Visible==true)
                    rkorisnicke.Datum_isteka_ugovora = dateIstekUgovora.Value;


                DTOManager.updateRadnikKorisnicke(rkorisnicke);
                //    this.DialogResult = DialogResult.OK;
            }

        }
    }
}
