﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WebHosting.DTOs;
using WebHosting.Entiteti;

namespace WebHosting.Forme
{
    public partial class DodajRTehnickeStruke : MetroFramework.Forms.MetroForm
    {
        //public DodajRTehnickeStruke()
        //{
        //    InitializeComponent();
        //}

        #region Atributi
        private bool adminPage = true;
        private HashSet<StringValue> listaSistema;
        private bool kreirajAdministratora = true;
        private RadnikTehnickeStrukeView administrator;

        private HashSet<StringValue> listaJezika;
        private bool kreirajProgramera = true;
        private RadnikTehnickeStrukeView programer;

        #endregion

        
        #region Konstruktori
        public DodajRTehnickeStruke()
        {
            Init();
            kreirajAdministratora = true;
            kreirajProgramera = true;
            adminPage = true;
        }

        public DodajRTehnickeStruke(int id, bool adminPage)            
        {
            //ako je adminPage onda za administratora update radimo
            //ako nije onda za programera
            Init();
            this.adminPage = adminPage;
            if (adminPage)
            {
                RadnikTab.SelectedTab = AdministratorPage;
                if (RadnikTab.SelectedTab == AdministratorPage)
                {
                    kreirajAdministratora = false;
                    administrator = DTOManager.getRadnikTehnickeStrukeView(id);
                    PopuniKontrole();
                    PrikaziSisteme();
                }
            }
            else
            {
                RadnikTab.SelectedTab = ProgramerPage;
                kreirajProgramera = false;
                programer = DTOManager.getRadnikTehnickeStrukeView(id);
                PopuniKontrole();
                PrikaziJezike();
            }
        }




        public void Init()
        {
            InitializeComponent();
            listaSistema = new HashSet<StringValue>();
            listaJezika = new HashSet<StringValue>();
        }

        #endregion

        #region Funkcije

        public void PopuniKontrole()
        {
            if (adminPage)
            {
                txtLicnoIme.Text = administrator.Licno_ime;
                txtJmbg.Text = administrator.Jmbg;
                txtPrezime.Text = administrator.Prezime;
                txtImeOca.Text = administrator.Ime_oca;
                txtGodRadStaza.Text = administrator.Godine_radnog_staza.ToString();
                txtGodinaRodj.Text = administrator.Godina_rodjenja.ToString();
                datumZaposlenja.Value = administrator.DatumZaposlenja;
                gridSistemi.DataSource = listaSistema.ToList();
                gridSistemi.Columns[0].HeaderText = "Operativni sistem";

                //foreach (OperativniSistemi os in administrator.OperSistemi)
                //{
                //    listaSistema.Add(new StringValue(os.OperativniSistem));
                //}
            }
            else
            {
                txtLicnoIme.Text = programer.Licno_ime;
                txtJmbg.Text = programer.Jmbg;
                txtPrezime.Text = programer.Prezime;
                txtImeOca.Text = programer.Ime_oca;
                txtGodRadStaza.Text = programer.Godine_radnog_staza.ToString();
                txtGodinaRodj.Text = programer.Godina_rodjenja.ToString();
                datumZaposlenja.Value = programer.DatumZaposlenja;
                gridJezici.DataSource = listaJezika.ToList();
                gridJezici.Columns[0].HeaderText = "Programski jezik";

                //foreach (ProgramskiJezici p in programer.ProgJezici)
                //{
                //    listaJezika.Add(new StringValue(p.ProgramskiJezik));
                //}
            }



        }

        public bool Validacija()
        {
            //neka za sad ovako

            if (string.IsNullOrEmpty(txtLicnoIme.Text))
            {
                return false;
            }
            else if (string.IsNullOrEmpty(txtPrezime.Text))
            {
                return false;
            }
            else if (string.IsNullOrEmpty(txtJmbg.Text))
            {
                return false;
            }
            else if (string.IsNullOrEmpty(txtGodinaRodj.Text))
            {
                return false;
            }
            else if (string.IsNullOrEmpty(txtGodRadStaza.Text))
            {
                return false;
            }

            return true;
        }

        public void PrikaziSisteme()
        {
            gridSistemi.DataSource = listaSistema.ToList();
            gridSistemi.Columns[0].HeaderText = "Operativni sistem";
        }

        public void PrikaziJezike()
        {
            gridJezici.DataSource = listaJezika.ToList();
            gridJezici.Columns[0].HeaderText = "Programski jezik";
        }

        #endregion

        private void metroLabel3_Click(object sender, EventArgs e)
        {

        }

        private void metroLabel4_Click(object sender, EventArgs e)
        {

        }

        private void btnOdustani_Click(object sender, EventArgs e)
        {
            this.Close();
            DialogResult = DialogResult.Cancel;
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
        }
        private void DodajAdministratora_Load(object sender, EventArgs e)
        {
            
        }

        private void metroTabPage1_Click(object sender, EventArgs e)
        {

        }

        private void txtLicnoIme_Click(object sender, EventArgs e)
        {

        }

        private void txtLicnoIme_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsLetter(e.KeyChar) && !Char.IsControl(e.KeyChar))
                e.Handled = true;
        }

        private void txtImeOca_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsLetter(e.KeyChar) && !Char.IsControl(e.KeyChar))
                e.Handled = true;
        }

        private void txtPrezime_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsLetter(e.KeyChar) && !Char.IsControl(e.KeyChar))
                e.Handled = true;
        }

        private void txtJmbg_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
                e.Handled = true;
        }

        private void txtGodRadStaza_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
                e.Handled = true;
        }

        private void txtGodinaRodj_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
                e.Handled = true;
        }

        private void txtProgJezik_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsLetter(e.KeyChar) && !Char.IsControl(e.KeyChar))
                e.Handled = true;
        }

        private void txtSistem_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (!Char.IsLetter(e.KeyChar) && !Char.IsControl(e.KeyChar))
            //    e.Handled = true;
        }

        private void btnDodajSistem_Click(object sender, EventArgs e)
        {
            //da probamo ovako
            kreirajProgramera = false;
            this.adminPage = true;
            if (string.IsNullOrEmpty(txtSistem.Text))
            {
                return;
            }
            listaSistema.Add(new StringValue(txtSistem.Text));
            txtSistem.Text = "";
            PrikaziSisteme();
        }


        private void btnObrisiSistem_Click(object sender, EventArgs e)
        {
            if (gridSistemi.SelectedRows.Count == 0)
            {
                return;
            }

            foreach (DataGridViewRow row in gridSistemi.SelectedRows)
            {
                listaSistema.Remove(((StringValue)gridSistemi.SelectedRows[0].DataBoundItem));

            }
            PrikaziSisteme();
        }

        private void btnDodajJezik_Click(object sender, EventArgs e)
        {
            //daj da probamo ovako
            kreirajAdministratora = false;
            this.adminPage = false;
            if (string.IsNullOrEmpty(txtProgJezik.Text))
            {
                return;
            }
            listaJezika.Add(new StringValue(txtProgJezik.Text));
            txtProgJezik.Text = "";
            PrikaziJezike();
        }

        private void btnObrisiJezik_Click(object sender, EventArgs e)
        {
            if (gridJezici.SelectedRows.Count == 0)
            {
                return;
            }

            foreach (DataGridViewRow row in gridJezici.SelectedRows)
            {
                listaJezika.Remove(((StringValue)gridSistemi.SelectedRows[0].DataBoundItem));

            }
            PrikaziJezike();
        }

        private void btnOdustaniProgramer_Click(object sender, EventArgs e)
        {
        }

        private void btnDodajProgramera_Click(object sender, EventArgs e)
        {
            if (kreirajProgramera)
            {
                if (!Validacija())
                    return;

                RadnikTehnickeStruke a = new RadnikTehnickeStruke();

                a.Licno_ime = txtLicnoIme.Text;
                a.Prezime = txtPrezime.Text;
                a.Ime_oca = txtImeOca.Text;
                a.Jmbg = txtJmbg.Text;
                a.Godina_rodjenja = Int32.Parse(txtGodinaRodj.Text);
                a.Godine_radnog_staza = Int32.Parse(txtGodRadStaza.Text);
                a.Datum_zaposlenja = datumZaposlenja.Value;

                a.Pozicija = "Programer";
                a.R_neodredjeno = "Da";
                a.R_odredjeno = "Ne";
                a.R_korisnickePodrske = "Ne";


                DTOManager.AddRadnikTehnickeStruke(a);

                if (listaJezika.Count != 0)
                {
                    foreach (var jezik in listaJezika)
                    {
                        if (a.ProgJezici.Where(x => x.ProgramskiJezik == jezik.Value).ToList().Count() > 0)
                            continue;
                        else
                        {
                            DTOManager.addJezikProgramer(a.Id, jezik.Value);
 //                           DTOManager.addSistemAdministrator(a.Id, jezik.Value);
                        }
                    }
                    foreach (var jezik in a.ProgJezici)
                    {
                        if (listaJezika.Where(x => x.Value == jezik.ProgramskiJezik).ToList().Count() == 0)
                            DTOManager.deleteJezikProgramer(jezik.Id);
                        //mislim da je ok
                            //                         DTOManager.deleteSistemAdministrator(jezik.Id);

                    }

                }
                //ovo necu da diram jer nisam obradio celu logiku
                //DTOManager.updateRadnikTehnickeStruke(administrator);//?
                this.DialogResult = DialogResult.OK;
            }
                //verovano ako nije onda je administrator ali ovo nema smisla jer se poziva dugme kreiraj programera
                //tako da else ne treba uopste da stoji ali neka ostane mozda je neka druga logika u pitanju
            else
            {
                if (!Validacija())
                    return;

                administrator.Licno_ime = txtLicnoIme.Text;
                administrator.Prezime = txtPrezime.Text;
                administrator.Ime_oca = txtImeOca.Text;
                administrator.Jmbg = txtJmbg.Text;
                administrator.Godina_rodjenja = Int32.Parse(txtGodinaRodj.Text);
                administrator.Godine_radnog_staza = Int32.Parse(txtGodRadStaza.Text);
                administrator.DatumZaposlenja = datumZaposlenja.Value;
                
                //if (listaSistema.Count != 0)
                //{
                //    foreach (var sistem in listaSistema)
                //    {
                //        if (administrator.OperSistemi.Where(x => x.OperativniSistem == sistem.Value).ToList().Count() > 0)
                //            continue;
                //        else
                //        {
                //            DTOManager.addSistemAdministrator(administrator.Id, sistem.Value);
                //        }
                //    }
                //    foreach (var sistem in administrator.OperSistemi)
                //    {
                //        if (listaSistema.Where(x => x.Value == sistem.OperativniSistem).ToList().Count() == 0)
                //            DTOManager.deleteSistemAdministrator(sistem.Id);

                //    }

                //}
//                DTOManager.updateRadnikTehnickeStruke(administrator);
                //    this.DialogResult = DialogResult.OK;
            }

        }

        private void btnOdustani_Click_1(object sender, EventArgs e)
        {
            this.Close();
            DialogResult = DialogResult.Cancel;

        }

        private void btnDodajKonacni_Click(object sender, EventArgs e)
        {
            if (this.adminPage)
            {
                if (kreirajAdministratora)
                {
                    if (!Validacija())
                        return;

                    RadnikTehnickeStruke a = new RadnikTehnickeStruke();

                    a.Licno_ime = txtLicnoIme.Text;
                    a.Prezime = txtPrezime.Text;
                    a.Ime_oca = txtImeOca.Text;
                    a.Jmbg = txtJmbg.Text;
                    a.Godina_rodjenja = Int32.Parse(txtGodinaRodj.Text);
                    a.Godine_radnog_staza = Int32.Parse(txtGodRadStaza.Text);
                    a.Datum_zaposlenja = datumZaposlenja.Value;

                    a.Pozicija = "Administrator";
                    a.R_neodredjeno = "Da";
                    a.R_odredjeno = "Ne";
                    a.R_korisnickePodrske = "Ne";


                    DTOManager.AddRadnikTehnickeStruke(a);

                    if (listaSistema.Count != 0)
                    {
                        foreach (var sistem in listaSistema)
                        {
                            //nisam siguran koliko je ovo optimalno
                            if (a.OperSistemi.Where(x => x.OperativniSistem == sistem.Value).ToList().Count() > 0)
                                continue;
                            else
                            {
                                DTOManager.addSistemAdministrator(a.Id, sistem.Value);
                            }
                        }
                        foreach (var sistem in a.OperSistemi)
                        {
                            if (listaSistema.Where(x => x.Value == sistem.OperativniSistem).ToList().Count() == 0)
                                DTOManager.deleteSistemAdministrator(sistem.Id);

                        }

                    }

                    //MessageBox.Show(administrator.Id.ToString());
                    //MessageBox.Show(a.Id.ToString());
                    //                DTOManager.updateRadnikTehnickeStruke(administrator);//?
                    this.DialogResult = DialogResult.OK;

                }
                else
                {
                    administrator.Licno_ime = txtLicnoIme.Text;
                    administrator.Prezime = txtPrezime.Text;
                    administrator.Ime_oca = txtImeOca.Text;
                    administrator.Jmbg = txtJmbg.Text;
                    administrator.Godina_rodjenja = Int32.Parse(txtGodinaRodj.Text);
                    administrator.Godine_radnog_staza = Int32.Parse(txtGodRadStaza.Text);
                    // administrator.Datum_zaposlenja = datumZaposlenja.Value;

                    //administrator.Pozicija = "Administrator";
                    //administrator.R_neodredjeno = "Da";
                    //administrator.R_odredjeno = "Ne";
                    //administrator.R_korisnickePodrske = "Ne";



                    //if (listaSistema.Count != 0)
                    //{
                    //    foreach (var sistem in listaSistema)
                    //    {
                    //        //nisam siguran koliko je ovo optimalno
                    //        if (administrator.OperSistemi.Where(x => x.OperativniSistem == sistem.Value).ToList().Count() > 0)
                    //            continue;
                    //        else
                    //        {
                    //            DTOManager.addSistemAdministrator(administrator.Id, sistem.Value);
                    //        }
                    //    }
                    //    foreach (var sistem in administrator.OperSistemi)
                    //    {
                    //        if (listaSistema.Where(x => x.Value == sistem.OperativniSistem).ToList().Count() == 0)
                    //            DTOManager.deleteSistemAdministrator(sistem.Id);

                    //    }

                    //}
                    DTOManager.updateRadnikTehnickeStruke(administrator);
                    this.DialogResult = DialogResult.OK;
                }
            }
            else
            {
                if (kreirajProgramera)
                {
                    if (!Validacija())
                        return;

                    RadnikTehnickeStruke a = new RadnikTehnickeStruke();

                    a.Licno_ime = txtLicnoIme.Text;
                    a.Prezime = txtPrezime.Text;
                    a.Ime_oca = txtImeOca.Text;
                    a.Jmbg = txtJmbg.Text;
                    a.Godina_rodjenja = Int32.Parse(txtGodinaRodj.Text);
                    a.Godine_radnog_staza = Int32.Parse(txtGodRadStaza.Text);
                    a.Datum_zaposlenja = datumZaposlenja.Value;

                    a.Pozicija = "Programer";
                    a.R_neodredjeno = "Da";
                    a.R_odredjeno = "Ne";
                    a.R_korisnickePodrske = "Ne";


                    DTOManager.AddRadnikTehnickeStruke(a);
                    if (listaJezika.Count != 0)
                    {
                        foreach (var jezik in listaJezika)
                        {
                            if (a.ProgJezici.Where(x => x.ProgramskiJezik == jezik.Value).ToList().Count() > 0)
                                continue;
                            else
                            {
                                DTOManager.addJezikProgramer(a.Id, jezik.Value);
                                //                            DTOManager.addSistemAdministrator(administrator.Id, sistem.Value);
                            }
                        }
                        //sta se ovde htelo???
                        foreach (var jezik in a.ProgJezici)
                        {
                            if (listaJezika.Where(x => x.Value == jezik.ProgramskiJezik).ToList().Count() == 0)
                                DTOManager.deleteJezikProgramer(jezik.Id);

                        }

                    }
                    //                    DTOManager.updateRadnikTehnickeStruke(programer);
                    this.DialogResult = DialogResult.OK;
                }
                else
                {
                    if (!Validacija())
                        return;

                    programer.Licno_ime = txtLicnoIme.Text;
                    programer.Prezime = txtPrezime.Text;
                    programer.Ime_oca = txtImeOca.Text;
                    programer.Jmbg = txtJmbg.Text;
                    programer.Godina_rodjenja = Int32.Parse(txtGodinaRodj.Text);
                    programer.Godine_radnog_staza = Int32.Parse(txtGodRadStaza.Text);
                    programer.DatumZaposlenja = datumZaposlenja.Value;


                    //if (listaJezika.Count != 0)
                    //{
                    //    foreach (var jezik in listaJezika)
                    //    {
                    //        if (programer.ProgJezici.Where(x => x.ProgramskiJezik == jezik.Value).ToList().Count() > 0)
                    //            continue;
                    //        else
                    //        {
                    //            DTOManager.addJezikProgramer(programer.Id, jezik.Value);
                    //            //                            DTOManager.addSistemAdministrator(administrator.Id, sistem.Value);
                    //        }
                    //    }
                    //    //sta se ovde htelo???
                    //    foreach (var jezik in programer.ProgJezici)
                    //    {
                    //        if (listaJezika.Where(x => x.Value == jezik.ProgramskiJezik).ToList().Count() == 0)
                    //            DTOManager.deleteJezikProgramer(jezik.Id);

                    //    }

                    //}
                    DTOManager.updateRadnikTehnickeStruke(programer);
                    this.DialogResult = DialogResult.OK;
                }

            }

        }
    }
}
