﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using WebHosting.DTOs;
using WebHosting.Entiteti;

namespace WebHosting.Forme
{
    public partial class DodajPaket : MetroFramework.Forms.MetroForm
    {

        #region atributi

        private bool kreirajPaket=true;
        private PaketView p;//?!

        #endregion

        public DodajPaket()
        {
            InitializeComponent();
            kreirajPaket = true;
        }

        public DodajPaket(int id)
        {
            kreirajPaket = false;

            p = DTOManager.getPaketView(id);

            PopuniKontrole();
        }

        public void PopuniKontrole()
        {

        }

        private void DodajPaket_Load(object sender, EventArgs e)
        {
            chkBaza.Checked = false;
            chkSkripting.Checked = false;
            chkStaticne.Checked = false;

            PrikaziSakrij();
        }

        private void PrikaziSakrij()
        {
            if(!chkSkripting.Checked)
            {
                labelSkripting.Visible = false;
                txtSkriptingJezik.Visible = false;
            }
            else
            {
                labelSkripting.Visible = true;
                txtSkriptingJezik.Visible = true;
            }
            if(!chkBaza.Checked)
            {
                labelNazivBaze.Visible = false;
                txtNazivaBaze.Visible = false;
            }
            else
            {
                labelNazivBaze.Visible = true;
                txtNazivaBaze.Visible = true;
            }
        }

        private void chkBaza_CheckedChanged(object sender, EventArgs e)
        {
            PrikaziSakrij();
        }

        private void chkSkripting_CheckedChanged(object sender, EventArgs e)
        {
            PrikaziSakrij();
        }

        private void btnDodajPaket_Click(object sender, EventArgs e)
        {
            if (kreirajPaket)
            {
                Paket paket = new Paket();
                paket.Cena = Int32.Parse(txtCena.Text);
                paket.Prostor_u_mb = Int32.Parse(txtProstor.Text);
                if (chkBaza.Checked)
                {
                    paket.Whp_sa_bazom = "Da";
                    paket.Dbms = txtNazivaBaze.Text;
                }
                else
                    paket.Whp_sa_bazom = "Ne";
                if (chkSkripting.Checked)
                {
                    paket.Whp_sa_skripting = "Da";
                    paket.Skripting_jezik = txtSkriptingJezik.Text;
                }
                if (chkStaticne.Checked)
                    paket.Whp_za_staticne = "Da";
                else
                    paket.Whp_za_staticne = "Ne";

                DTOManager.AddPaket(paket);

                //DTOManager.updatePaket(p);

                MessageBox.Show("Paket uspesno dodat u bazi!");
                this.DialogResult = DialogResult.OK;

            }
            else
            {

            }
        }

        private void BtnOdustaniPaket_Click(object sender, EventArgs e)
        {
            this.Close();
            DialogResult = DialogResult.Cancel;
        }
    }
}
