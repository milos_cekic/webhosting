﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WebHosting.DTOs;
using WebHosting.Entiteti;
using NHibernate;

namespace WebHosting.Forme
{
    public partial class VidiOdrzavanjeServera : MetroFramework.Forms.MetroForm
    {
        public VidiOdrzavanjeServera()
        {
            InitializeComponent();
        }

        private void VidiOdrzavanjeServera_Load(object sender, EventArgs e)
        {
            PopuniTabele();
        }

        private void PopuniTabele()
        {
            dgvPostojeceTrenutnoOdrzavaniServeri.DataSource = DTOManager.getListaOdrzavaServereView(false, 9);
            dgvServeri.DataSource = DTOManager.getListaServerView(true, "");
            
            dgvDostupniRadnici.DataSource = DTOManager.getRadnikeTehnickeStruke("Administrator");
            dgvServeri.DataSource = DTOManager.getListaServerView(true, "");
            
        }

        private void BtnOdustani_Click(object sender, EventArgs e)
        {
            this.Close();
            DialogResult = DialogResult.Cancel;
        }
    }
}
