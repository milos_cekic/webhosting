﻿namespace WebHosting
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.UcitavanjePaketa = new System.Windows.Forms.Button();
            this.DodajPaket = new System.Windows.Forms.Button();
            this.dodajKorisnika = new System.Windows.Forms.Button();
            this.UcitavanjeNarudzbine = new System.Windows.Forms.Button();
            this.DodajNarudzbinu = new System.Windows.Forms.Button();
            this.UcitavanjeServera = new System.Windows.Forms.Button();
            this.DodajServer = new System.Windows.Forms.Button();
            this.UcitavanjeRadnikaK = new System.Windows.Forms.Button();
            this.UcitavanjePravnogLica = new System.Windows.Forms.Button();
            this.UcitavanjeKorisnika = new System.Windows.Forms.Button();
            this.DodavanjeFizickog = new System.Windows.Forms.Button();
            this.KontaktOsoba = new System.Windows.Forms.Button();
            this.ProgramskiJezik = new System.Windows.Forms.Button();
            this.OdrzavaServere = new System.Windows.Forms.Button();
            this.HostujeSe = new System.Windows.Forms.Button();
            this.OdrzavaPakete = new System.Windows.Forms.Button();
            this.OperativniSistemi = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // UcitavanjePaketa
            // 
            this.UcitavanjePaketa.Location = new System.Drawing.Point(12, 25);
            this.UcitavanjePaketa.Name = "UcitavanjePaketa";
            this.UcitavanjePaketa.Size = new System.Drawing.Size(263, 23);
            this.UcitavanjePaketa.TabIndex = 0;
            this.UcitavanjePaketa.Text = "Ucitavanje paketa";
            this.UcitavanjePaketa.UseVisualStyleBackColor = true;
            this.UcitavanjePaketa.Click += new System.EventHandler(this.UcitavanjePaketa_Click);
            // 
            // DodajPaket
            // 
            this.DodajPaket.Location = new System.Drawing.Point(281, 25);
            this.DodajPaket.Name = "DodajPaket";
            this.DodajPaket.Size = new System.Drawing.Size(263, 23);
            this.DodajPaket.TabIndex = 1;
            this.DodajPaket.Text = "Dodaj paket";
            this.DodajPaket.UseVisualStyleBackColor = true;
            this.DodajPaket.Click += new System.EventHandler(this.DodajPaket_Click);
            // 
            // dodajKorisnika
            // 
            this.dodajKorisnika.Location = new System.Drawing.Point(283, 64);
            this.dodajKorisnika.Name = "dodajKorisnika";
            this.dodajKorisnika.Size = new System.Drawing.Size(263, 23);
            this.dodajKorisnika.TabIndex = 3;
            this.dodajKorisnika.Text = "Dodaj pravno lice";
            this.dodajKorisnika.UseVisualStyleBackColor = true;
            this.dodajKorisnika.Click += new System.EventHandler(this.dodajKorisnika_Click);
            // 
            // UcitavanjeNarudzbine
            // 
            this.UcitavanjeNarudzbine.Location = new System.Drawing.Point(12, 104);
            this.UcitavanjeNarudzbine.Name = "UcitavanjeNarudzbine";
            this.UcitavanjeNarudzbine.Size = new System.Drawing.Size(263, 23);
            this.UcitavanjeNarudzbine.TabIndex = 4;
            this.UcitavanjeNarudzbine.Text = "Ucitavanje narudzbine";
            this.UcitavanjeNarudzbine.UseVisualStyleBackColor = true;
            this.UcitavanjeNarudzbine.Click += new System.EventHandler(this.UcitavanjeNarudzbine_Click);
            // 
            // DodajNarudzbinu
            // 
            this.DodajNarudzbinu.Location = new System.Drawing.Point(281, 104);
            this.DodajNarudzbinu.Name = "DodajNarudzbinu";
            this.DodajNarudzbinu.Size = new System.Drawing.Size(263, 23);
            this.DodajNarudzbinu.TabIndex = 5;
            this.DodajNarudzbinu.Text = "Dodaj narudzbinu";
            this.DodajNarudzbinu.UseVisualStyleBackColor = true;
            this.DodajNarudzbinu.Click += new System.EventHandler(this.DodajNarudzbinu_Click);
            // 
            // UcitavanjeServera
            // 
            this.UcitavanjeServera.Location = new System.Drawing.Point(12, 144);
            this.UcitavanjeServera.Name = "UcitavanjeServera";
            this.UcitavanjeServera.Size = new System.Drawing.Size(263, 23);
            this.UcitavanjeServera.TabIndex = 6;
            this.UcitavanjeServera.Text = "Ucitavanje servera";
            this.UcitavanjeServera.UseVisualStyleBackColor = true;
            this.UcitavanjeServera.Click += new System.EventHandler(this.UcitavanjeServera_Click);
            // 
            // DodajServer
            // 
            this.DodajServer.Location = new System.Drawing.Point(283, 144);
            this.DodajServer.Name = "DodajServer";
            this.DodajServer.Size = new System.Drawing.Size(263, 23);
            this.DodajServer.TabIndex = 7;
            this.DodajServer.Text = "DodajServer";
            this.DodajServer.UseVisualStyleBackColor = true;
            this.DodajServer.Click += new System.EventHandler(this.DodajServer_Click);
            // 
            // UcitavanjeRadnikaK
            // 
            this.UcitavanjeRadnikaK.Location = new System.Drawing.Point(12, 185);
            this.UcitavanjeRadnikaK.Name = "UcitavanjeRadnikaK";
            this.UcitavanjeRadnikaK.Size = new System.Drawing.Size(263, 23);
            this.UcitavanjeRadnikaK.TabIndex = 9;
            this.UcitavanjeRadnikaK.Text = "Ucitavanje radnika";
            this.UcitavanjeRadnikaK.UseVisualStyleBackColor = true;
            this.UcitavanjeRadnikaK.Click += new System.EventHandler(this.UcitavanjeRadnikaK_Click);
            // 
            // UcitavanjePravnogLica
            // 
            this.UcitavanjePravnogLica.Location = new System.Drawing.Point(283, 185);
            this.UcitavanjePravnogLica.Name = "UcitavanjePravnogLica";
            this.UcitavanjePravnogLica.Size = new System.Drawing.Size(263, 23);
            this.UcitavanjePravnogLica.TabIndex = 10;
            this.UcitavanjePravnogLica.Text = "Ucitavanje pravnog lica";
            this.UcitavanjePravnogLica.UseVisualStyleBackColor = true;
            this.UcitavanjePravnogLica.Click += new System.EventHandler(this.UcitavanjePravnogLica_Click);
            // 
            // UcitavanjeKorisnika
            // 
            this.UcitavanjeKorisnika.Location = new System.Drawing.Point(12, 64);
            this.UcitavanjeKorisnika.Name = "UcitavanjeKorisnika";
            this.UcitavanjeKorisnika.Size = new System.Drawing.Size(263, 23);
            this.UcitavanjeKorisnika.TabIndex = 11;
            this.UcitavanjeKorisnika.Text = "Ucitavanje korisnika";
            this.UcitavanjeKorisnika.UseVisualStyleBackColor = true;
            this.UcitavanjeKorisnika.Click += new System.EventHandler(this.UcitavanjeKorisnika_Click);
            // 
            // DodavanjeFizickog
            // 
            this.DodavanjeFizickog.Location = new System.Drawing.Point(283, 224);
            this.DodavanjeFizickog.Name = "DodavanjeFizickog";
            this.DodavanjeFizickog.Size = new System.Drawing.Size(261, 23);
            this.DodavanjeFizickog.TabIndex = 12;
            this.DodavanjeFizickog.Text = "Dodaj fizicko lice";
            this.DodavanjeFizickog.UseVisualStyleBackColor = true;
            this.DodavanjeFizickog.Click += new System.EventHandler(this.DodavanjeFizickog_Click);
            // 
            // KontaktOsoba
            // 
            this.KontaktOsoba.Location = new System.Drawing.Point(144, 343);
            this.KontaktOsoba.Name = "KontaktOsoba";
            this.KontaktOsoba.Size = new System.Drawing.Size(261, 23);
            this.KontaktOsoba.TabIndex = 13;
            this.KontaktOsoba.Text = "Dodaj kontakt osobu";
            this.KontaktOsoba.UseVisualStyleBackColor = true;
            this.KontaktOsoba.Click += new System.EventHandler(this.KontaktOsoba_Click);
            // 
            // ProgramskiJezik
            // 
            this.ProgramskiJezik.Location = new System.Drawing.Point(283, 301);
            this.ProgramskiJezik.Name = "ProgramskiJezik";
            this.ProgramskiJezik.Size = new System.Drawing.Size(261, 23);
            this.ProgramskiJezik.TabIndex = 14;
            this.ProgramskiJezik.Text = "Dodaj programski jezik";
            this.ProgramskiJezik.UseVisualStyleBackColor = true;
            this.ProgramskiJezik.Click += new System.EventHandler(this.ProgramskiJezik_Click);
            // 
            // OdrzavaServere
            // 
            this.OdrzavaServere.Location = new System.Drawing.Point(12, 301);
            this.OdrzavaServere.Name = "OdrzavaServere";
            this.OdrzavaServere.Size = new System.Drawing.Size(263, 23);
            this.OdrzavaServere.TabIndex = 15;
            this.OdrzavaServere.Text = "Veza odrzava servere";
            this.OdrzavaServere.UseVisualStyleBackColor = true;
            this.OdrzavaServere.Click += new System.EventHandler(this.OdrzavaServere_Click);
            // 
            // HostujeSe
            // 
            this.HostujeSe.Location = new System.Drawing.Point(12, 224);
            this.HostujeSe.Name = "HostujeSe";
            this.HostujeSe.Size = new System.Drawing.Size(263, 23);
            this.HostujeSe.TabIndex = 16;
            this.HostujeSe.Text = "Veza hostuje se";
            this.HostujeSe.UseVisualStyleBackColor = true;
            this.HostujeSe.Click += new System.EventHandler(this.HostujeSe_Click);
            // 
            // OdrzavaPakete
            // 
            this.OdrzavaPakete.Location = new System.Drawing.Point(12, 262);
            this.OdrzavaPakete.Name = "OdrzavaPakete";
            this.OdrzavaPakete.Size = new System.Drawing.Size(263, 23);
            this.OdrzavaPakete.TabIndex = 17;
            this.OdrzavaPakete.Text = "Veza odrzava pakete";
            this.OdrzavaPakete.UseVisualStyleBackColor = true;
            this.OdrzavaPakete.Click += new System.EventHandler(this.OdrzavaPakete_Click);
            // 
            // OperativniSistemi
            // 
            this.OperativniSistemi.Location = new System.Drawing.Point(283, 262);
            this.OperativniSistemi.Name = "OperativniSistemi";
            this.OperativniSistemi.Size = new System.Drawing.Size(261, 23);
            this.OperativniSistemi.TabIndex = 18;
            this.OperativniSistemi.Text = "Dodaj operativni sistem";
            this.OperativniSistemi.UseVisualStyleBackColor = true;
            this.OperativniSistemi.Click += new System.EventHandler(this.OperativniSistemi_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 383);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(543, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Napomena: Kod ucitavanja entiteta, izvrsili smo i ucitavanje njegovih veza, za ko" +
    "je nismo pravili dodatne dugmice.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(137, 410);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(268, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Na taj nacin smo obuhvatili sve sto se trazilo u zadatku.";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(564, 432);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.OperativniSistemi);
            this.Controls.Add(this.OdrzavaPakete);
            this.Controls.Add(this.HostujeSe);
            this.Controls.Add(this.OdrzavaServere);
            this.Controls.Add(this.ProgramskiJezik);
            this.Controls.Add(this.KontaktOsoba);
            this.Controls.Add(this.DodavanjeFizickog);
            this.Controls.Add(this.UcitavanjeKorisnika);
            this.Controls.Add(this.UcitavanjePravnogLica);
            this.Controls.Add(this.UcitavanjeRadnikaK);
            this.Controls.Add(this.DodajServer);
            this.Controls.Add(this.UcitavanjeServera);
            this.Controls.Add(this.DodajNarudzbinu);
            this.Controls.Add(this.UcitavanjeNarudzbine);
            this.Controls.Add(this.dodajKorisnika);
            this.Controls.Add(this.DodajPaket);
            this.Controls.Add(this.UcitavanjePaketa);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button UcitavanjePaketa;
        private System.Windows.Forms.Button DodajPaket;
        private System.Windows.Forms.Button dodajKorisnika;
        private System.Windows.Forms.Button UcitavanjeNarudzbine;
        private System.Windows.Forms.Button DodajNarudzbinu;
        private System.Windows.Forms.Button UcitavanjeServera;
        private System.Windows.Forms.Button DodajServer;
        private System.Windows.Forms.Button UcitavanjeRadnikaK;
        private System.Windows.Forms.Button UcitavanjePravnogLica;
        private System.Windows.Forms.Button UcitavanjeKorisnika;
        private System.Windows.Forms.Button DodavanjeFizickog;
        private System.Windows.Forms.Button KontaktOsoba;
        private System.Windows.Forms.Button ProgramskiJezik;
        private System.Windows.Forms.Button OdrzavaServere;
        private System.Windows.Forms.Button HostujeSe;
        private System.Windows.Forms.Button OdrzavaPakete;
        private System.Windows.Forms.Button OperativniSistemi;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}

