﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebHosting.Entiteti;

namespace WebHosting.DTOs
{
    public class PravnoLiceView : KorisnikView
    {
        public virtual long PIB { get; set; }
        public virtual string Naziv { get; set; }

        public PravnoLiceView() : base()
        {

        }
        public PravnoLiceView(PravnoLice lice) : base(lice)
        {
            this.PIB = lice.PIB;
            this.Naziv = lice.Naziv;
        }
    }
}
