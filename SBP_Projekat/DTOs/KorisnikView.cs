﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebHosting.Entiteti;

namespace WebHosting.DTOs
{
    public class KorisnikView
    {
        public virtual int Id { get; protected set; }
        public virtual string Adresa { get; set; }

        public virtual DateTime Od { get; set; }
        public virtual DateTime Do { get; set; }

        //public virtual IList<KontaktOsoba> KontaktOsobe { get; set; }
        public virtual EkonomistaTehnickePodrske Ekonomista { get; set; }

        public KorisnikView()
        {
            //KontaktOsobe = new List<KontaktOsoba>();
            Ekonomista = new EkonomistaTehnickePodrske();
        }

        public KorisnikView(Korisnik k)
        {
            Ekonomista = new EkonomistaTehnickePodrske();
            //KontaktOsobe = new List<KontaktOsoba>();

            this.Id = k.Id;
            this.Adresa = k.Adresa;
            this.Od = k.Od;
            this.Do = k.Do;
            this.Ekonomista = k.KorespondovaoSaEkonomistom;
            //if (k.KontaktOsobe != null)
            //{
            //    foreach (KontaktOsoba s in k.KontaktOsobe)
            //        this.KontaktOsobe.Add(s);
            //}


        }

    }
}
