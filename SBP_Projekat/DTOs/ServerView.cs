﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebHosting.Entiteti;

namespace WebHosting.DTOs
{
    public class ServerView
    {
        public int Id { get; protected set; }
        public string IP_adresa { get; set; }
       // public string OS_tip { get; set; }

        //public IList<RadnikTehnickeStruke> OdrzavanOdStrane { get; set; }



        public ServerView()
        {
            //OdrzavanOdStrane = new List<RadnikTehnickeStruke>();

        }

        public ServerView(Server s)
        {
            this.Id = s.Id;
            this.IP_adresa = s.IP_adresa;
           // this.OS_tip = s.OS_tip;

            //this.OdrzavanOdStrane = new List<RadnikTehnickeStruke>();



            //if (s.OdrzavanOdStrane != null)
            //{
            //    foreach (RadnikTehnickeStruke rts in s.OdrzavanOdStrane)
            //        this.OdrzavanOdStrane.Add(rts);
            //}
        }
    }

    public class WindowsServerView : ServerView
    {
        //public IList<Paket> Paketi { get; set; }
        public int BrojPaketaKojiSeHostuju { get; set; }

        public WindowsServerView()
            : base()
        {
            //Paketi = new List<Paket>();
        }

        public WindowsServerView(WindowsServer w)
            : base(w)
        {
            this.BrojPaketaKojiSeHostuju = w.Paketi.Count;
            //Paketi = new List<Paket>();
            //if (w.Paketi != null)
            //    foreach (Paket p in w.Paketi)
            //        this.Paketi.Add(p);
        }
    }

    //?
    public class LinuxServerView : ServerView
    {
        public LinuxServerView() : base() { }
        public LinuxServerView(LinuxServer l) : base(l) { }
    }

}
