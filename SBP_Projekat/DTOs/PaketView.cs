﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebHosting.Entiteti;

namespace WebHosting.DTOs
{
    public class PaketView
    {
        public virtual int Id { get; set; }
        public virtual int Cena { get; set; }
        public virtual int Prostor_u_mb { get; set; }
        public virtual string Whp_sa_bazom { get; set; }
        public virtual string Dbms { get; set; }
        public virtual string Whp_sa_skripting { get; set; }
        public virtual string Skripting_jezik { get; set; }
        public virtual string Whp_za_staticne { get; set; }

        //public virtual IList<Narudzbina> PripadajuNarudzbinama { get; set; }

        // public virtual IList<RadnikTehnickeStruke> OdrzavajuRadnici { get; set; }
        //public virtual IList<WindowsServer> HostujuSeNaServerima { get; set; }

        public PaketView()
        {
            //PripadajuNarudzbinama = new List<Narudzbina>();
            //OdrzavajuRadnici = new List<RadnikTehnickeStruke>();
            //HostujuSeNaServerima = new List<WindowsServer>();
        }

        public PaketView(Paket p)
        {
            this.Id = p.Id;
            this.Cena = p.Cena;
            this.Prostor_u_mb = p.Prostor_u_mb;
            this.Whp_sa_bazom = p.Whp_sa_bazom;
            this.Dbms = p.Dbms;
            this.Whp_sa_skripting = p.Whp_sa_skripting;
            this.Skripting_jezik = p.Skripting_jezik;
            this.Whp_za_staticne = p.Whp_za_staticne;

            //if (p.PripadajuNarudzbinama.Count > 0)
            //{
            //    foreach (Narudzbina n in p.PripadajuNarudzbinama)
            //        this.PripadajuNarudzbinama.Add(n);  
            //}

        }
    }
}
