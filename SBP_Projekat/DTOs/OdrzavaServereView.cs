﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebHosting.Entiteti;

namespace WebHosting.DTOs
{
    public class OdrzavaServereView
    {
        public int Id { get; set; }
        public string ImeRadnika { get; set; }
        public string ImeOcaRadnika { get; set; }
        public string PrezimeRadnika { get; set; }
        public DateTime DatumZaposlenja { get; set; }
        public string PozicijaRadnika { get; set; }
        public string IpAdresaHosta { get; set; }

        public OdrzavaServereView() { }

        public OdrzavaServereView(OdrzavaServere os)
        {
            this.Id = os.Id;
            this.ImeRadnika = os.RadnikOdrzava.Licno_ime;
            this.ImeOcaRadnika = os.RadnikOdrzava.Ime_oca;
            this.PrezimeRadnika = os.RadnikOdrzava.Prezime;
            this.DatumZaposlenja = os.RadnikOdrzava.Datum_zaposlenja;
            this.PozicijaRadnika = os.RadnikOdrzava.Pozicija;
            this.IpAdresaHosta = os.OdrzavaSeServer.IP_adresa;
        }
    }
}
