﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebHosting.Entiteti;

namespace WebHosting.DTOs
{

    //public class BasicRadnikView
    //{
    //    public int Id { get; set; }
    //    public string Licno_ime { get; set; }
    //    public string Ime_oca { get; set; }
    //    public string Prezime { get; set; }



    //    public BasicRadnikView()
    //    {

    //    }
    //    public BasicRadnikView(Radnik r)
    //    {
    //        this.Id = r.Id;
    //        this.Licno_ime = r.Licno_ime;
    //        this.Ime_oca = r.Ime_oca;
    //        this.Prezime = r.Prezime;
    //    }
    //    public BasicRadnikView(RadnikView r)
    //    {
    //        this.Id = r.Id;
    //        this.Licno_ime = r.Licno_ime;
    //        this.Ime_oca = r.Ime_oca;
    //        this.Prezime = r.Prezime;
    //    }
    //}

    public class RadnikView
    {
        public int Id { get; set; }
        public string Jmbg { get; set; }
        public string Licno_ime { get; set; }
        public string Ime_oca { get; set; }
        public string Prezime { get; set; }
        public DateTime DatumZaposlenja { get; set; }
        public int Godine_radnog_staza { get; set; }
        public virtual int Godina_rodjenja { get; set; }
        //?
        public virtual DateTime? Datum_isteka_ugovora { get; set; }


       // public virtual IList<Narudzbina> PrimljeneNarudzbine { get; set; }

        public RadnikView()
        {
           // PrimljeneNarudzbine = new List<Narudzbina>();
        }

        public RadnikView(Radnik r)
        {
            this.Id = r.Id;
            this.Jmbg = r.Jmbg;
            this.Godina_rodjenja = r.Godina_rodjenja;
            this.Licno_ime = r.Licno_ime;
            this.Ime_oca = r.Ime_oca;
            this.Prezime = r.Prezime;
            this.DatumZaposlenja = r.Datum_zaposlenja;
            this.Godine_radnog_staza = r.Godine_radnog_staza;

            this.Datum_isteka_ugovora = r.Datum_isteka_ugovora;


            //if (PrimljeneNarudzbine != null)
            //{
            //    foreach (Narudzbina n in r.PrimljeneNarudzbine)
            //    {
            //        this.PrimljeneNarudzbine.Add(n);
            //    }
            //}
        }
    }

    public class RadnikTehnickeStrukeView : RadnikView
    {


        //public virtual IList<Server> OdrzavaServere { get; set; }
        //public virtual IList<Paket> OdrzavaPakete { get; set; }

        //public virtual IList<OperativniSistemi> OperSistemi { get; set; }
        //public virtual IList<ProgramskiJezici> ProgJezici { get; set; }

        public RadnikTehnickeStrukeView()
        {
            //OdrzavaServere = new List<Server>();
            //OdrzavaPakete = new List<Paket>();
            //OperSistemi = new List<OperativniSistemi>();
            //ProgJezici = new List<ProgramskiJezici>();
        }

        public RadnikTehnickeStrukeView(RadnikTehnickeStruke r) : base(r)
        {

            //OdrzavaServere = new List<Server>();
            //OdrzavaPakete = new List<Paket>();
            //OperSistemi = new List<OperativniSistemi>();
            //ProgJezici = new List<ProgramskiJezici>();


            //if (r.OdrzavaServere != null)
            //{
            //    foreach (Server s in r.OdrzavaServere)
            //        this.OdrzavaServere.Add(s);
            //}
            //if (r.OdrzavaPakete != null)
            //{
            //    foreach (Paket p in r.OdrzavaPakete)
            //        this.OdrzavaPakete.Add(p);
            //}
            //if (r.OperSistemi != null)
            //{
            //    foreach (OperativniSistemi os in r.OperSistemi)
            //        this.OperSistemi.Add(os);
            //}
            //if (r.ProgJezici != null)
            //{
            //    foreach (ProgramskiJezici p in r.ProgJezici)
            //        this.ProgJezici.Add(p);
            //}
        }

        public override string ToString()
        {
            return this.Licno_ime + " " + this.Ime_oca + " " + this.Prezime;
        }
    }

    public class EkonomistaTehnickePodrskeView : RadnikView
    {
        //public virtual IList<Korisnik> Korisnici { get; set; }

        public virtual int? Stepen_strucne_spreme { get; set; }

        public EkonomistaTehnickePodrskeView()
        {
           // Korisnici = new List<Korisnik>();
        }

        public EkonomistaTehnickePodrskeView(EkonomistaTehnickePodrske e) : base(e)
        {
            this.Stepen_strucne_spreme = e.Stepen_strucne_spreme;

            //if (e.Korisnici != null)
            //{
            //    foreach (Korisnik k in e.Korisnici)
            //        this.Korisnici.Add(k);
            //}
        }
    }
}
