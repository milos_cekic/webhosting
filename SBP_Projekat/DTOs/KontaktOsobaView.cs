﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebHosting.Entiteti;

namespace WebHosting.DTOs
{
    public class KontaktOsobaView
    {
        public int Id { get; set; }
        public string Licno_ime { get; set; }
        public string Prezime { get; set; }
        public long Broj_telefona { get; set; }

        public KontaktOsobaView() { }

        public KontaktOsobaView(KontaktOsoba ko)
        {
            this.Id = ko.Id;
            this.Licno_ime = ko.LicnoIme;
            this.Prezime = ko.Prezime;
            this.Broj_telefona = ko.BrojTelefona;
        }
    }
}
