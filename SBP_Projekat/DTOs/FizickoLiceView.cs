﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebHosting.Entiteti;

namespace WebHosting.DTOs
{
    public class FizickoLiceView : KorisnikView
    {
        public virtual string JMBG { get; set; }
        public virtual string LicnoIme { get; set; }
        public virtual string Prezime { get; set; }

        public FizickoLiceView() : base()
        {

        }

        public FizickoLiceView(FizickoLice lice) : base(lice)
        {
            this.JMBG = lice.JMBG;
            this.LicnoIme = lice.LicnoIme;
            this.Prezime = lice.Prezime;
        }
    }
}
