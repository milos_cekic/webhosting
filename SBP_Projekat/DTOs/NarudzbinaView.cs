﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebHosting.Entiteti;

namespace WebHosting.DTOs
{
    public class NarudzbinaView
    {
        public int Id { get; set; }
        public DateTime Datum { get; set; }
        public int TrajanjeMeseci { get; set; }
        public int CenaPaketa { get; set; }
        public int ProstorKojiZauzimaPaket { get; set; }
        public string PaketSadrziBazu { get; set; }

        public string Dbms { get; set; }
        public string PaketZaStaticneStrane { get; set; }

        public string PaketSaSkriptingJezikom { get; set; }
        public string SkriptingJezik { get; set; }
        public string AdresaKupca { get; set; }
        public string ImeRadnikaKojiJePrimioNarudzbinu { get; set; }
        public string PrezimeRadnikaKojiJePrimioNarudzbinu { get; set; }

        public NarudzbinaView()
        {

        }

        public NarudzbinaView(Narudzbina n)
        {
            this.Id = n.Id;
            this.Datum = n.Datum;
            this.TrajanjeMeseci = n.TrajanjeMeseci;
            this.CenaPaketa = n.PosedujePaket.Cena;
            this.AdresaKupca = n.KupujeNarudzbinu.Adresa;
            this.ImeRadnikaKojiJePrimioNarudzbinu = n.PrimaNarudzbinu.Licno_ime;
            this.PrezimeRadnikaKojiJePrimioNarudzbinu = n.PrimaNarudzbinu.Prezime;
            this.ProstorKojiZauzimaPaket = n.PosedujePaket.Prostor_u_mb;
            this.PaketZaStaticneStrane = n.PosedujePaket.Whp_za_staticne;
            this.PaketSadrziBazu = n.PosedujePaket.Whp_sa_bazom;
            this.PaketSaSkriptingJezikom = n.PosedujePaket.Whp_sa_skripting;
            this.Dbms = n.PosedujePaket.Dbms;
            this.SkriptingJezik = n.PosedujePaket.Skripting_jezik;
        }
    }
}
