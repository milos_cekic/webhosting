﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebHosting.Entiteti;

namespace WebHosting.DTOs
{
    public class HostujeSeView
    {
        public int Id { get; set; }
        public string Ip_adresa_hosta { get; set; }
        public int CenaPaketa { get; set; }
        public int ProstorKojiZauzimaPaket { get; set; }
        public string PaketSadrziBazu { get; set; }
        public string Dbms { get; set; }
        public string PaketZaStaticneStrane { get; set; }
        public string PaketSaSkriptingJezikom { get; set; }
        public string SkriptingJezik { get; set; }

        public HostujeSeView() { }

        public HostujeSeView(HostujeSe hs)
        {
            this.Id = hs.Id;
            this.Ip_adresa_hosta = hs.HostujeServer.IP_adresa;
            this.CenaPaketa = hs.HostujePaket.Cena;
            this.ProstorKojiZauzimaPaket = hs.HostujePaket.Prostor_u_mb;
            this.PaketSadrziBazu = hs.HostujePaket.Whp_sa_bazom;
            this.Dbms = hs.HostujePaket.Dbms;
            this.PaketZaStaticneStrane = hs.HostujePaket.Whp_za_staticne;
            this.PaketSaSkriptingJezikom = hs.HostujePaket.Whp_sa_skripting;
            this.SkriptingJezik = hs.HostujePaket.Skripting_jezik;
        }
    }
}
