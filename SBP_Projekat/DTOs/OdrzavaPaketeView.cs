﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebHosting.Entiteti;

namespace WebHosting.DTOs
{
    public class OdrzavaPaketeView
    {
        public int Id { get; set; }
        public string ImeRadnika { get; set; }
        public string ImeOcaRadnika { get; set; }
        public string PrezimeRadnika { get; set; }
        //public int CenaPaketa { get; set; }
        //public int ProstorKojiZauzimaPaket { get; set; }
        public string PaketSadrziBazu { get; set; }

        public string Dbms { get; set; }
        public string PaketZaStaticneStrane { get; set; }

        public string PaketSaSkriptingJezikom { get; set; }
        public string SkriptingJezik { get; set; }

        public OdrzavaPaketeView() { }

        public OdrzavaPaketeView(OdrzavaPakete op)
        {
            this.Id = op.Id;
            this.ImeRadnika = op.RadnikOdrzava.Licno_ime;
            this.ImeOcaRadnika = op.RadnikOdrzava.Ime_oca;
            this.PrezimeRadnika = op.RadnikOdrzava.Prezime;
            this.PaketSadrziBazu = op.OdrzavaSePaket.Whp_sa_bazom;
            this.Dbms = op.OdrzavaSePaket.Dbms;
            this.PaketZaStaticneStrane = op.OdrzavaSePaket.Whp_za_staticne;
            this.PaketSaSkriptingJezikom = op.OdrzavaSePaket.Whp_sa_skripting;
            this.SkriptingJezik = op.OdrzavaSePaket.Skripting_jezik;
        }
    }
}
