﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebHosting.Entiteti;
using FluentNHibernate.Mapping;

namespace WebHosting.Mapiranja
{
    public class OdrzavaServereMapiranja : ClassMap<OdrzavaServere>
    {
        public OdrzavaServereMapiranja()
        {
            Table("ODRZAVA_SERVERE");

            Id(x => x.Id, "ID").GeneratedBy.TriggerIdentity();

            References(x => x.RadnikOdrzava).Column("ID_RADNIKA");
            References(x => x.OdrzavaSeServer).Column("ID_SERVERA");

        }
    }
}
