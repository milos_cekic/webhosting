﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebHosting.Entiteti;
using FluentNHibernate.Mapping;

namespace WebHosting.Mapiranja
{
    public class PaketMapiranja : ClassMap<Paket>
    {
        public PaketMapiranja()
        {
            //Mapiranje tabele
            Table("PAKET");

            //mapiranje primarnog kljuca
            Id(x => x.Id, "ID").GeneratedBy.TriggerIdentity();

            //mapiranje svojstava
            Map(x => x.Cena, "CENA");
            Map(x => x.Prostor_u_mb, "PROSTOR_U_MB");
            Map(x => x.Whp_sa_bazom, "WHP_SA_BAZOM");
            Map(x => x.Dbms, "DBMS");
            Map(x => x.Whp_sa_skripting, "WHP_SA_SKRIPTING");
            Map(x => x.Skripting_jezik, "SKRIPTING_JEZIK");
            Map(x => x.Whp_za_staticne, "WHP_ZA_STATICNE");

            //mapiranje veze 1:N Paket:Narudzbina
            HasMany(x => x.PripadajuNarudzbinama).KeyColumn("ID_PAKETA").LazyLoad().Cascade.All().Inverse();

            HasManyToMany(x => x.HostujuSeNaServerima)
                   .Table("HOSTUJE_SE")
                   .ParentKeyColumn("ID_PAKETA")
                   .ChildKeyColumn("ID_WINDOWS_SERVERA")
                   .Inverse()
                   .Cascade.All();

            HasManyToMany(x => x.OdrzavajuRadnici)
                    .Table("ODRZAVA_PAKETE")
                    .ParentKeyColumn("ID_PAKETA")
                    .ChildKeyColumn("ID_RADNIKA")
                    .Inverse()
                    .Cascade.All();
        }
    }
    //class PaketSaBazomMapiranja : SubclassMap<PaketSaBazom>
    //{
    //    public PaketSaBazomMapiranja()
    //    {
    //        DiscriminatorValue("Sa_bazom");

    //        Map(x => x.Dbms, "DBMS");
    //    }

    //}
    //class PaketSaSkriptingMapiranja : SubclassMap<PaketSaSkripting>
    //{
    //    public PaketSaSkriptingMapiranja()
    //    {
    //        DiscriminatorValue("Sa_skripting");

    //        Map(x => x.Skripting_jezik, "SKRIPTING_JEZIK");
    //    }
    //}
    //class PaketZaStaticneMapiranja : SubclassMap<PaketZaStaticne>
    //{
    //    public PaketZaStaticneMapiranja()
    //    {
    //        DiscriminatorValue("Za_staticne");
    //    }
    //}
    //class PaketBazaStaticneMapiranja : SubclassMap<PaketBazaStaticne>
    //{
    //    public PaketBazaStaticneMapiranja()
    //    {
    //        DiscriminatorValue("Baza_staticne");

    //        Map(x => x.Dbms, "DBMS");
    //    }
    //}
    //class PaketBazaSkriptongMapiranja : SubclassMap<PaketBazaSkriptong>
    //{
    //    public PaketBazaSkriptongMapiranja()
    //    {
    //        DiscriminatorValue("Baza_skripting");

    //        Map(x => x.Dbms, "DBMS");
    //        Map(x => x.Skripting_jezik, "SKRIPTING_JEZIK");
    //    }
    //}
    //class PaketSkriptingStaticneMapiranja : SubclassMap<PaketSkriptingStaticne>
    //{
    //    public PaketSkriptingStaticneMapiranja()
    //    {
    //        DiscriminatorValue("Skripting_staticne");
    //    }
    //}
    //class PaketKompletniMapiranja : SubclassMap<PaketKompletni>
    //{
    //    public PaketKompletniMapiranja()
    //    {
    //        DiscriminatorValue("Kompletni");

    //        Map(x => x.Dbms, "DBMS");
    //        Map(x => x.Skripting_jezik, "SKRIPTING_JEZIK");
    //    }
    //}
}
