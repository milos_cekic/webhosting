﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebHosting.Entiteti;
using FluentNHibernate.Mapping;

namespace WebHosting.Mapiranja
{
    public class RadnikMapiranja : ClassMap<Radnik>
    {
        public RadnikMapiranja()
        {
            //Mapiranje tabele
            Table("RADNIK");

            DiscriminateSubClassesOnColumn("SPECIJALIZACIJA", "Radnik_korisnicke_podrske");

            //mapiranje primarnog kljuca
            Id(x => x.Id, "ID").GeneratedBy.TriggerIdentity();

            //mapiranje svojstava
            Map(x => x.Jmbg, "JMBG");
            Map(x => x.Licno_ime, "LICNO_IME");
            Map(x => x.Ime_oca, "IME_OCA");
            Map(x => x.Prezime, "PREZIME");
            Map(x => x.Godina_rodjenja, "GODINA_RODJENJA");
            Map(x => x.Datum_zaposlenja, "DATUM_ZAPOSLENJA");
            Map(x => x.Godine_radnog_staza, "GODINE_RADNOG_STAZA");
            Map(x => x.R_odredjeno, "R_ODREDJENO");
            Map(x => x.Datum_isteka_ugovora, "UGOVOR_ISTICE");
            Map(x => x.Stepen_strucne_spreme, "STEPEN_STRUCNE_SPREME");
            Map(x => x.R_neodredjeno, "R_NEODREDJENO");
            Map(x => x.Pozicija, "POZICIJA");
            Map(x => x.R_korisnickePodrske, "R_KORISNICKE");
            Map(x => x.SpecijalnostRadnika, "SPECIJALIZACIJA");

            //string predikat = "CASE WHEN (R_ODREDJENO = 'Da' AND R_NEODREDJENO = 'Ne') THEN 'Radnik_odredjeno' ELSE 'Radnik_neodredjeno' END";

            //.Formula(predikat);

            //mairanje veze 1:N RadnikKorisnickePodrske:Narudzbina
            HasMany(x => x.PrimljeneNarudzbine).KeyColumn("ID_KORISNICKE_PODRSKE").LazyLoad().Cascade.All().Inverse();


        }
    }

    public class EkonomistaTehnickePodrskeMapiranja : SubclassMap<EkonomistaTehnickePodrske>
    {
        public EkonomistaTehnickePodrskeMapiranja()
        {
            //Table("EKONOMISTA_TEHNICKE_PODRSKE");

            DiscriminatorValue("Ekonomista_tehnicke_podrske");

            //KeyColumn("ID");

            //Map(x => x.Stepen_strucne_spreme, "STEPEN_STRUCNE_SPREME");



            //mairanje veze 1:N EkonomistaTehnickePodrske:Korisnik
            HasMany(x => x.Korisnici).KeyColumn("ID_EKONOMISTE").LazyLoad().Cascade.All().Inverse();
        }
    }

    public class RadnikTehnickeStrukeMapiranja : SubclassMap<RadnikTehnickeStruke>
    {
        public RadnikTehnickeStrukeMapiranja()
        {
            //Table("RADNIK_TEHNICKE_STRUKE");

            //Id(x => x.Id, "ID").GeneratedBy.TriggerIdentity();
            //KeyColumn("ID");

            //DiscriminateSubClassesOnColumn("POZICIJA");//?
            //Map(x => x.Pozicija, "POZICIJA");

            DiscriminatorValue("Radnik_tehnicke_struke");

            HasManyToMany(x => x.OdrzavaServere)
                     .Table("ODRZAVA_SERVERE")
                     .ParentKeyColumn("ID_RADNIKA")
                     .ChildKeyColumn("ID_SERVERA")
                     .Inverse()
                     .Cascade.All();

            HasManyToMany(x => x.OdrzavaPakete)
                     .Table("ODRZAVA_PAKETE")
                     .ParentKeyColumn("ID_RADNIKA")
                     .ChildKeyColumn("ID_PAKETA")
                     .Cascade.All();

            HasMany(x => x.ProgJezici).KeyColumn("ID_PROGRAMERA");

            HasMany(x => x.OperSistemi).KeyColumn("ID_ADMINISTRATORA");
        }
    }

    //public class RadnikNeodredjenoMapiranja : SubclassMap<RadnikNeodredjeno>
    //{
    //    public RadnikNeodredjenoMapiranja()
    //    {
    //        DiscriminatorValue("Radnik_neodredjeno");
    //    }
    //}

    //public class RadnikOdredjenoMapiranja : SubclassMap<RadnikOdredjeno>
    //{
    //    public RadnikOdredjenoMapiranja()
    //    {
    //        DiscriminatorValue("Radnik_odredjeno");

    //        Map(x => x.Datum_isteka_ugovora, "DATUM_ISTEKA_UGOVORA");
    //    }
    //}



    //public class EkonomistaTehnickePodrskeMapiranja : SubclassMap<EkonomistaTehnickePodrske>
    //{
    //    public EkonomistaTehnickePodrskeMapiranja()
    //    {
    //        Table("EKONOMISTA_TEHNICKE_PODRSKE");//?

    //        KeyColumn("ID");

    //        Map(x => x.Stepen_strucne_spreme, "STEPEN_STRUCNE_SPREME");

    //    }
    //}
}
