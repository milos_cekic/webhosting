﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebHosting.Entiteti;
using FluentNHibernate.Mapping;

namespace WebHosting.Mapiranja
{
    public class KorisnikMapiranja : ClassMap<Korisnik>
    {
        public KorisnikMapiranja()
        {
            //Mapiranje tabele
            Table("KORISNIK");

            //mapiranje primarnog kljuca
            Id(x => x.Id, "ID").GeneratedBy.TriggerIdentity();

            //mapiranje svojstava
            Map(x => x.Adresa, "ADRESA");
            Map(x => x.Do, "DO");
            Map(x => x.Od, "OD");

            //Mapiranje veze 1:N ekonomista-korisnik
            References(x => x.KorespondovaoSaEkonomistom).Column("ID_EKONOMISTE").LazyLoad().Cascade.All();

            //Mapiranje veze 1:N Korisnik-Narudzbina
            HasMany(x => x.KupujeNarudzbine).KeyColumn("ID_KORISNIKA").LazyLoad().Cascade.All().Inverse();

            //Mapiranje identifikujuce veze
            HasMany(x => x.KontaktOsobe).KeyColumn("ID_KORISNIKA").LazyLoad().Cascade.All().Inverse();
        }

    }
    public class PravnoLiceMapiranja : SubclassMap<PravnoLice>
    {
        public PravnoLiceMapiranja()
        {
            Table("PRAVNO_LICE");

            KeyColumn("ID");

            Map(x => x.PIB, "PIB");
            Map(x => x.Naziv, "NAZIV");
        }
    }
    public class FizickoLiceMapiranja : SubclassMap<FizickoLice>
    {
        public FizickoLiceMapiranja()
        {
            Table("FIZICKO_LICE");

            KeyColumn("ID");

            Map(x => x.JMBG, "JMBG");
            Map(x => x.LicnoIme, "LICNO_IME");
            Map(x => x.Prezime, "PREZIME");
        }
    }
}
