﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebHosting.Entiteti;
using FluentNHibernate.Mapping;

namespace WebHosting.Mapiranja
{
    public class OperativniSistemiMapiranja : ClassMap<OperativniSistemi>
    {
        public OperativniSistemiMapiranja()
        {
            Table("OPERATIVNI_SISTEMI");

            Id(x => x.Id).GeneratedBy.TriggerIdentity();

            Map(x => x.OperativniSistem, "OPERATIVNI_SISTEM");

            References(x => x.Administrator).Column("ID_ADMINISTRATORA");
        }
    }
}
