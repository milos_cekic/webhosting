﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebHosting.Entiteti;
using FluentNHibernate.Mapping;

namespace WebHosting.Mapiranja
{
    public class ServerMapiranja : ClassMap<Server>
    {
        public ServerMapiranja()
        {
            //Mapiranje tabele
            Table("SERVER");

            DiscriminateSubClassesOnColumn("OS_TIP");

            //mapiranje primarnog kljuca
            Id(x => x.Id, "ID").GeneratedBy.TriggerIdentity();

            //mapiranje svojstava
            Map(x => x.IP_adresa, "IP_ADRESA");
            // Map(x => x.OS_tip, "OS_TIP");



            HasManyToMany(x => x.OdrzavanOdStrane)
                .Table("ODRZAVA_SERVERE")
                .ParentKeyColumn("ID_SERVERA")
                .ChildKeyColumn("ID_RADNIKA")
                .Cascade.All();

        }
    }

    public class LinuxServerMapiranja : SubclassMap<LinuxServer>
    {
        public LinuxServerMapiranja()
        {
            DiscriminatorValue("S_LINUX");
        }
    }

    public class WindowsServerMapiranja : SubclassMap<WindowsServer>
    {
        public WindowsServerMapiranja()
        {
            DiscriminatorValue("S_WINDOWS");

            HasManyToMany(x => x.Paketi)
                .Table("HOSTUJE_SE")
                .ParentKeyColumn("ID_WINDOWS_SERVERA")
                .ChildKeyColumn("ID_PAKETA")
                .Cascade.All();
        }
    }
}
