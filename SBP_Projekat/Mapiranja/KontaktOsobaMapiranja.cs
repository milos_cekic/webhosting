﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebHosting.Entiteti;
using FluentNHibernate.Mapping;

namespace WebHosting.Mapiranja
{
    public class KontaktOsobaMapiranja : ClassMap<KontaktOsoba>
    {
        public KontaktOsobaMapiranja()
        {
            Table("KONTAKT_OSOBA");

            Id(x => x.Id, "ID").GeneratedBy.TriggerIdentity();

            Map(x => x.LicnoIme, "LICNO_IME");
            Map(x => x.Prezime, "PREZIME");
            Map(x => x.BrojTelefona, "BROJ_TELEFONA");

            References(x => x.KorisnikZaKogPamtimo).Column("ID_KORISNIKA").LazyLoad();
        }
    }
}
