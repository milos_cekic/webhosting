﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebHosting.Entiteti;
using FluentNHibernate.Mapping;

namespace WebHosting.Mapiranja
{
    public class NarudzbinaMapiranja : ClassMap<Narudzbina>
    {
        public NarudzbinaMapiranja()
        {
            Table("NARUDZBINA");

            Id(x => x.Id, "ID").GeneratedBy.TriggerIdentity();

            Map(x => x.Datum, "DATUM");

            //mapiranje veze 1:N Korisnik:Narudzbina
            References(x => x.KupujeNarudzbinu).Column("ID_KORISNIKA").LazyLoad().Cascade.All();

            //mapiranje veze 1:N Paket:Narudzbina   
            References(x => x.PosedujePaket).Column("ID_PAKETA").LazyLoad().Cascade.All();
            Map(x => x.TrajanjeMeseci, "TRAJANJE_MESECI");

            //mapiranje veze 1:N RadnikKorisnickePodrske:Narudzbina
            References(x => x.PrimaNarudzbinu).Column("ID_KORISNICKE_PODRSKE").LazyLoad().Cascade.All();
        }
    }
}
