﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebHosting.Entiteti;
using FluentNHibernate.Mapping;

namespace WebHosting.Mapiranja
{
    public class OdrzavaPaketeMapiranja : ClassMap<OdrzavaPakete>
    {
        public OdrzavaPaketeMapiranja()
        {
            Table("ODRZAVA_PAKETE");

            Id(x => x.Id, "Id").GeneratedBy.TriggerIdentity().UnsavedValue(-1);

            References(x => x.OdrzavaSePaket).Column("ID_PAKETA");
            References(x => x.RadnikOdrzava).Column("ID_RADNIKA");
        }
    }
}
