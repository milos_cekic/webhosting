﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebHosting.Entiteti;
using FluentNHibernate.Mapping;

namespace WebHosting.Mapiranja
{
    public class ProgramskiJeziciMapiranja : ClassMap<ProgramskiJezici>
    {
        public ProgramskiJeziciMapiranja()
        {
            Table("PROGRAMSKI_JEZICI");

            Id(x => x.Id).GeneratedBy.TriggerIdentity();

            Map(x => x.ProgramskiJezik, "PROGRAMSKI_JEZIK");

            References(x => x.Programer).Column("ID_PROGRAMERA");
        }
    }
}
