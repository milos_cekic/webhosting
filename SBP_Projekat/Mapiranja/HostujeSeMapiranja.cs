﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebHosting.Entiteti;
using FluentNHibernate.Mapping;

namespace WebHosting.Mapiranja
{
    public class HostujeSeMapiranja : ClassMap<HostujeSe>
    {
        public HostujeSeMapiranja()
        {
            Table("HOSTUJE_SE");

            Id(x => x.Id, "Id").GeneratedBy.TriggerIdentity().UnsavedValue(-1);

            References(x => x.HostujePaket).Column("ID_PAKETA");
            References(x => x.HostujeServer).Column("ID_WINDOWS_SERVERA");
        }
    }
}
