﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//dodato
using NHibernate;
using WebHosting.Entiteti;

namespace WebHosting
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void UcitavanjePaketa_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                //Ucitavaju se podaci o paketu za zadatim brojem
                Paket p = s.Load<Paket>(1);

                MessageBox.Show("Cena paketa: "+p.Cena.ToString());

                string nazivi = "Nazivi radnika koji odrzava taj paket:\n";
                foreach(RadnikTehnickeStruke r in p.OdrzavajuRadnici)
                {
                    nazivi += r.Id.ToString() + ". " + r.Licno_ime+" "+r.Prezime+"\n";
                }
                MessageBox.Show(nazivi);
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void DodajPaket_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Narudzbina n = s.Get<Narudzbina>(1);

                Paket p = new Paket();

                p.Cena = 300;
                p.Prostor_u_mb = 50;
                p.Whp_sa_bazom = "Da";
                p.Dbms = "Oracle";
                p.Whp_sa_skripting = "Ne";
                p.Whp_za_staticne = "Ne";

                p.PripadajuNarudzbinama.Add(n);
                n.PosedujePaket = p;

                s.Save(p);
                
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }


        private void UcitavanjeServera_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                //Radnik o = s.Get<Radnik>(1);
                WindowsServer w1 = s.Get<WindowsServer>(1);

                if (w1 != null)
                {
                    MessageBox.Show("Ip adresa servera: \n"+w1.IP_adresa);

                    string prikaz = "Na njemu se hostuju paketi sa cenama:\n";
                    foreach (Paket p1 in w1.Paketi)
                    {
                        prikaz += p1.Id.ToString()+". "+p1.Cena.ToString() + " \n";
                    }
                    MessageBox.Show(prikaz);
                    string radnici = "Taj paket odrzavaju:\n";
                    foreach(Radnik r1 in w1.OdrzavanOdStrane)
                    {
                        radnici += r1.Licno_ime + " " + r1.Prezime + "\n";
                    }
                    MessageBox.Show(radnici);
                }
                else
                {
                    MessageBox.Show("Ne postoji server sa zadatim identifikatorom");
                }
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void dodajKorisnika_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();


                PravnoLice k = new PravnoLice();

                Narudzbina n1 = s.Get<Narudzbina>(3);
                KontaktOsoba p1 = s.Get<KontaktOsoba>(3);

                EkonomistaTehnickePodrske e1 = s.Get<EkonomistaTehnickePodrske>(1);

                k.Adresa = "Nova adresa";
                k.Od = DateTime.Now.Date;
                k.Do = DateTime.Now.Date;
                k.KontaktOsobe.Add(p1);
                k.KupujeNarudzbine.Add(n1);
                k.KorespondovaoSaEkonomistom = e1;
                k.Naziv = "Novi naziv";
                k.PIB = 237283782;

                s.Save(k);

                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void UcitavanjeRadnikaK_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Radnik ekonomistaTehnicke = s.Load<Radnik>(1);
                RadnikTehnickeStruke  radnikTehnickeStruke= s.Load<RadnikTehnickeStruke>(12);//programer
                RadnikTehnickeStruke administrator = s.Load<RadnikTehnickeStruke>(8);
                Radnik korisnickePodrske = s.Load<Radnik>(6);

                if (administrator != null)
                {
                    if (ekonomistaTehnicke != null)
                    {
                        if (radnikTehnickeStruke != null)
                        {
                            if (korisnickePodrske != null)
                            {
                                string programskiJezici = "\nProgramske jezike koje poznaje:\n";
                                string operativniSistemi = "\nOperativne sisteme koje poznaje:\n";
                                foreach (ProgramskiJezici s1 in radnikTehnickeStruke.ProgJezici)
                                    programskiJezici += s1.ProgramskiJezik + "\n";
                                foreach (OperativniSistemi s1 in administrator.OperSistemi)
                                    operativniSistemi += s1.OperativniSistem + "\n";
                                MessageBox.Show(ekonomistaTehnicke.Licno_ime + " " + ekonomistaTehnicke.Prezime + "\n" + ekonomistaTehnicke.SpecijalnostRadnika + " \n" + ekonomistaTehnicke.Pozicija + "\n");
                                MessageBox.Show(administrator.Licno_ime + " " + administrator.Prezime + " \n" + administrator.SpecijalnostRadnika + " \n" + administrator.Pozicija + "\n"+operativniSistemi+"\n");
                                MessageBox.Show(radnikTehnickeStruke.Licno_ime + " " + radnikTehnickeStruke.Prezime + " \n" + radnikTehnickeStruke.SpecijalnostRadnika + " \n" + radnikTehnickeStruke.Pozicija + "\n"+programskiJezici+"\n");
                                MessageBox.Show(korisnickePodrske.Licno_ime + " " + korisnickePodrske.Prezime + " \n" + korisnickePodrske.SpecijalnostRadnika + " \n" + korisnickePodrske.Pozicija + "\n");

                            }
                            else
                                MessageBox.Show("Ne postoji radnik korisnicke podrske sa zadatim identifikatorom!");
                        }
                        else
                            MessageBox.Show("Ne postoji radnik tehnicke struke sa zadatimidentifikatorom!");
                    }
                    else
                    {
                        MessageBox.Show("Ne postoji ekonomista tehnicke podrske sa zadatim identifikatorom!");
                    }
                }
                
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void UcitavanjePravnogLica_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                PravnoLice e1 = s.Get<PravnoLice>(3);

                if (e1 != null)
                {
                    MessageBox.Show("Adresa pravnog lica: " + e1.Adresa);
                    MessageBox.Show("Od kad koresponduje: " + e1.Od.ToString());
                    MessageBox.Show("S kim koresponduje: " + e1.KorespondovaoSaEkonomistom.Licno_ime);
                }
                else
                {
                    MessageBox.Show("Ne postoji pravno lice sa zadatim identifikatorom");
                }


                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void UcitavanjeNarudzbine_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                //Ucitavaju se podaci o paketu za zadatim brojem
                Narudzbina n = s.Get<Narudzbina>(3);

                MessageBox.Show("Trajanje paketa u mesecima: "+n.TrajanjeMeseci.ToString());

                MessageBox.Show("Korisnik: " + n.KupujeNarudzbinu.Adresa);

                MessageBox.Show("Cena paketa za narudzbinu je: " + n.PosedujePaket.Cena);
                
                MessageBox.Show("Radnik koji je primio ovu narudzbinu je: " + n.PrimaNarudzbinu.Licno_ime + " " + n.PrimaNarudzbinu.Prezime);
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void UcitavanjeKorisnika_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                PravnoLice k1 = s.Get<PravnoLice>(3);

                if (k1 != null)
                {
                    MessageBox.Show("Adresa pravnog lica: " + k1.Adresa);

                    string prikaz = "";
                    foreach (KontaktOsoba osoba in k1.KontaktOsobe)
                    {
                        prikaz += osoba.LicnoIme + " \n";
                    }
                    MessageBox.Show("Imena kontakt osoba su: " + prikaz);

                    string prikazNarudzbina = "";
                    foreach (Narudzbina n1 in k1.KupujeNarudzbine)
                    {
                        prikazNarudzbina += n1.TrajanjeMeseci.ToString() + " \n";
                    }
                    MessageBox.Show("Trajanje meseca narudzbina korisnika: " + prikazNarudzbina);

                }
                else
                {
                    MessageBox.Show("Ne postoji pravno lice sa zadatim identifikatorom");
                }


                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void DodajServer_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();


                WindowsServer server = new WindowsServer();

                server.IP_adresa = "192.168.10.10";
                
                s.Save(server);

                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void DodajNarudzbinu_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();


                Narudzbina n = new Narudzbina();

                Korisnik k1 = s.Get<Korisnik>(3);
                Paket p1 = s.Get<Paket>(3);
                Radnik radnikKorisnicke = s.Get<Radnik>(14);

                n.Datum = DateTime.Now.Date;
                n.KupujeNarudzbinu = k1;
                n.PosedujePaket = p1;
                n.TrajanjeMeseci = 24;

                n.PrimaNarudzbinu = radnikKorisnicke;

                s.Save(n);
                //s.SaveOrUpdate(p);

                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void DodavanjeFizickog_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();


                FizickoLice  k = new FizickoLice();

                Narudzbina n1 = s.Get<Narudzbina>(3);
                KontaktOsoba p1 = s.Get<KontaktOsoba>(3);
                EkonomistaTehnickePodrske e1 = s.Get<EkonomistaTehnickePodrske>(1);

                k.Adresa = "Nova adresa";
                k.Od = DateTime.Now.Date;
                k.Do = DateTime.Now.Date;
                k.KontaktOsobe.Add(p1);
                k.KupujeNarudzbine.Add(n1);
                k.KorespondovaoSaEkonomistom = e1;
                k.JMBG = "0612837465932";
                k.LicnoIme = "Milos";
                k.Prezime = "Tasic";

                s.Save(k);

                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void HostujeSe_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Paket p1 = s.Load<Paket>(5);

                foreach (WindowsServer x in p1.HostujuSeNaServerima)
                    MessageBox.Show("Ip adresa: " + x.IP_adresa);

                WindowsServer w1 = s.Get<WindowsServer>(1);

                foreach (Paket x in w1.Paketi)
                    MessageBox.Show("Id paketa: " + x.Id);

                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void OdrzavaPakete_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Paket p1 = s.Load<Paket>(5);

                foreach (RadnikTehnickeStruke x in p1.OdrzavajuRadnici)
                    MessageBox.Show(x.Licno_ime+" "+x.Prezime);

                RadnikTehnickeStruke w1 = s.Get<RadnikTehnickeStruke>(12);

                foreach (Paket x in w1.OdrzavaPakete)
                    MessageBox.Show("Id paketa: " + x.Id);

                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void OdrzavaServere_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Server p1 = s.Load<Server>(1);

                foreach (RadnikTehnickeStruke x in p1.OdrzavanOdStrane)
                    MessageBox.Show(x.Licno_ime + " " + x.Prezime);

                RadnikTehnickeStruke w1 = s.Get<RadnikTehnickeStruke>(8);

                foreach (Server x in w1.OdrzavaServere)
                    MessageBox.Show("Ip adresa: " + x.IP_adresa);

                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void OperativniSistemi_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                RadnikTehnickeStruke n = s.Get<RadnikTehnickeStruke>(8);

                OperativniSistemi p = new OperativniSistemi();

                p.OperativniSistem = "Ubuntu Server 16.04.1 LTS";
                p.Administrator = n;

                n.OperSistemi.Add(p);

                s.Save(p);

                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void ProgramskiJezik_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                RadnikTehnickeStruke n = s.Get<RadnikTehnickeStruke>(9);

                ProgramskiJezici p = new ProgramskiJezici();

                p.ProgramskiJezik = "C#";
                p.Programer = n;

                n.ProgJezici.Add(p);

                s.Save(p);

                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void KontaktOsoba_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                PravnoLice n = s.Get<PravnoLice>(3);

                KontaktOsoba p = new KontaktOsoba();

                p.LicnoIme = "Bogdan";
                p.Prezime = "Nastasovic";

                p.KorisnikZaKogPamtimo = n;
                p.BrojTelefona = 38163987278;

                n.KontaktOsobe.Add(p);

                s.Save(p);

                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }
    }
}
