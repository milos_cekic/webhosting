﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebHosting.Entiteti
{
    public class Paket
    {
        public virtual int Id { get; protected set; }
        public virtual int Cena { get; set; }
        public virtual int Prostor_u_mb { get; set; }
        public virtual string Whp_sa_bazom { get; set; }//nije bool!?
        public virtual string Dbms { get; set; }
        public virtual string Whp_sa_skripting { get; set; }
        public virtual string Skripting_jezik { get; set; }
        public virtual string Whp_za_staticne { get; set; }

        public virtual IList<Narudzbina> PripadajuNarudzbinama { get; set; }

        public virtual IList<RadnikTehnickeStruke> OdrzavajuRadnici { get; set; }
        public virtual IList<WindowsServer> HostujuSeNaServerima { get; set; }

        public Paket()
        {
            PripadajuNarudzbinama = new List<Narudzbina>();
            OdrzavajuRadnici = new List<RadnikTehnickeStruke>();
            HostujuSeNaServerima = new List<WindowsServer>();
        }

    }

}
//public class PaketSaBazom : Paket 
//{
//    public virtual string Dbms { get; set; }
//}
//public class PaketSaSkripting : Paket 
//{
//    public virtual string Skripting_jezik { get; set; }
//}
//public class PaketZaStaticne : Paket { 

//    public virtual IList<Server> Serveri { get; set; }

//    public PaketZaStaticne()
//    {
//        Serveri = new List<Server>();
//    }
//}
//public class PaketBazaSkriptong : Paket 
//{
//    public virtual string Dbms { get; set; }
//    public virtual string Skripting_jezik { get; set; }
//}
//public class PaketBazaStaticne : Paket {

//    public virtual string Dbms { get; set; }

//    public virtual IList<Server> Serveri { get; set; }

//    public PaketBazaStaticne()
//    {
//        Serveri = new List<Server>();
//    }
//}
//public class PaketSkriptingStaticne : Paket {

//    public virtual string Skripting_jezik { get; set; }

//    public virtual IList<Server> Serveri { get; set; }

//    public PaketSkriptingStaticne()
//    {
//        Serveri = new List<Server>();
//    }
//}
//public class PaketKompletni : Paket {

//    public virtual string Skripting_jezik { get; set; }
//    public virtual string Dbms { get; set; }

//    public virtual IList<Server> Serveri { get; set; }

//    public PaketKompletni()
//    {
//        Serveri = new List<Server>();
//    }
//}

