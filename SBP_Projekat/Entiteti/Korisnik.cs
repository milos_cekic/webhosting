﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebHosting.Entiteti
{
    //abstract
    public class Korisnik
    {
        public virtual int Id { get; protected set; }
        public virtual string Adresa { get; set; }

        public virtual EkonomistaTehnickePodrske KorespondovaoSaEkonomistom { get; set; }
        public virtual DateTime Od { get; set; }
        public virtual DateTime Do { get; set; }

        //iz veze 1-N kupuje narudzbinu
        public virtual IList<Narudzbina> KupujeNarudzbine { get; set; }

        //iz identifikujuce veze
        public virtual IList<KontaktOsoba> KontaktOsobe { get; set; }

        public Korisnik()
        {
            KupujeNarudzbine = new List<Narudzbina>();
            KontaktOsobe = new List<KontaktOsoba>();
        }
    }
    public class FizickoLice : Korisnik
    {
        public virtual string JMBG { get; set; }
        public virtual string LicnoIme { get; set; }
        public virtual string Prezime { get; set; }
    }

    public class PravnoLice : Korisnik
    {
        public virtual long PIB { get; set; }
        public virtual string Naziv { get; set; }
    }
}
