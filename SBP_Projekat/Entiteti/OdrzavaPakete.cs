﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebHosting.Entiteti
{
    public class OdrzavaPakete
    {
        public virtual int Id { get; protected set; }

        public virtual Paket OdrzavaSePaket { get; set; }
        public virtual RadnikTehnickeStruke RadnikOdrzava { get; set; }
    }
}
