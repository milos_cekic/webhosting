﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebHosting.Entiteti
{
    public abstract class Server
    {
        public virtual int Id { get; protected set; }
        public virtual string IP_adresa { get; set; }
        public virtual string OS_tip { get; set; }

        public virtual IList<RadnikTehnickeStruke> OdrzavanOdStrane { get; set; }

        public Server()
        {
            OdrzavanOdStrane = new List<RadnikTehnickeStruke>();
        }

    }

    public class LinuxServer : Server
    {

    }

    public class WindowsServer : Server
    {
        public virtual IList<Paket> Paketi { get; set; }

        public WindowsServer()
        {
            Paketi = new List<Paket>();
        }
    }
}
