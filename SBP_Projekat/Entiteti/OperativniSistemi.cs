﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebHosting.Entiteti
{
    public class OperativniSistemi
    {
        public virtual int Id { get; protected set; }

        public virtual RadnikTehnickeStruke Administrator { get; set; }
        public virtual string OperativniSistem { get; set; }
    }
}
