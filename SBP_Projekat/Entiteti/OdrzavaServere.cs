﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WebHosting.Entiteti
{
    public class OdrzavaServere
    {
        public virtual int Id { get; protected set; }

        public virtual Server OdrzavaSeServer { get; set; }
        public virtual RadnikTehnickeStruke RadnikOdrzava { get; set; }
    }
}
