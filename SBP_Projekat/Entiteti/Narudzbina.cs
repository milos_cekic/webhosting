﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebHosting.Entiteti
{
    public class Narudzbina
    {
        public virtual int Id { get; protected set; }
        public virtual DateTime Datum { get; set; }

        //iz veze 1-N poseduje pakete
        public virtual int TrajanjeMeseci { get; set; }
        public virtual Paket PosedujePaket { get; set; }

        //iz veze 1-N kupuje narudzbinu
        public virtual Korisnik KupujeNarudzbinu { get; set; }

        //iz veze 1-N prima narudzbinu
        public virtual Radnik PrimaNarudzbinu { get; set; }
    }
}
