﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebHosting.Entiteti
{
    public class HostujeSe
    {
        public virtual int Id { get; protected set; }

        public virtual Paket HostujePaket { get; set; }
        public virtual WindowsServer HostujeServer { get; set; }
    }
}
