﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebHosting.Entiteti
{
    public class Radnik
    {
        public virtual int Id { get; protected set; }
        public virtual string Jmbg { get; set; }
        public virtual string Licno_ime { get; set; }
        public virtual string Ime_oca { get; set; }
        public virtual string Prezime { get; set; }
        public virtual int Godina_rodjenja { get; set; }
        public virtual DateTime Datum_zaposlenja { get; set; }
        public virtual int Godine_radnog_staza { get; set; }
        public virtual string R_odredjeno { get; set; }
        public virtual DateTime? Datum_isteka_ugovora { get; set; }
        public virtual int? Stepen_strucne_spreme { get; set; }
        public virtual string R_neodredjeno { get; set; }
        public virtual string Pozicija { get; set; }
        public virtual string R_korisnickePodrske { get; set; }
        public virtual string SpecijalnostRadnika { get; set; }

        public virtual IList<Narudzbina> PrimljeneNarudzbine { get; set; }

        public Radnik()
        {
            PrimljeneNarudzbine = new List<Narudzbina>();
        }


    }
    public class RadnikTehnickeStruke : Radnik
    {
        public virtual IList<Server> OdrzavaServere { get; set; }
        public virtual IList<Paket> OdrzavaPakete { get; set; }

        public virtual IList<OperativniSistemi> OperSistemi { get; set; }
        public virtual IList<ProgramskiJezici> ProgJezici { get; set; }

        public RadnikTehnickeStruke()
        {
            OdrzavaServere = new List<Server>();
            OdrzavaPakete = new List<Paket>();
            OperSistemi = new List<OperativniSistemi>();
            ProgJezici = new List<ProgramskiJezici>();
        }
    }
    public class EkonomistaTehnickePodrske : Radnik
    {
        public virtual IList<Korisnik> Korisnici { get; set; }

        public EkonomistaTehnickePodrske()
        {
            Korisnici = new List<Korisnik>();
        }
    }

    //public abstract class RadnikOdredjeno : Radnik 
    //{
    //    public virtual DateTime Datum_isteka_ugovora { get; set; }
    //}
    //public abstract class RadnikNeodredjeno : Radnik { }


}
