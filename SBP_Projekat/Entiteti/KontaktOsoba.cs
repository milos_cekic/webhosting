﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebHosting.Entiteti
{
    public class KontaktOsoba
    {
        public virtual int Id { get; protected set; }

        public virtual string LicnoIme { get; set; }
        public virtual string Prezime { get; set; }
        public virtual long BrojTelefona { get; set; }

        public virtual Korisnik KorisnikZaKogPamtimo { get; set; }
    }
}
