﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebHosting.Entiteti
{
    public class ProgramskiJezici
    {
        public virtual int Id { get; protected set; }

        public virtual RadnikTehnickeStruke Programer { get; set; }
        public virtual String ProgramskiJezik { get; set; }
    }
}
