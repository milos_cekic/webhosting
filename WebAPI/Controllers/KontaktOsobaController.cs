﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebHosting;
using WebHosting.Entiteti;
using WebHosting.DTOs;

namespace WebAPI.Controllers
{
    public class KontaktOsobaController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<KontaktOsobaView> Get()
        {
            DataProvider provider = new DataProvider();

            IEnumerable<KontaktOsobaView> osobe = provider.GetKontaktOsobe();

            return osobe;
        }

        // GET api/<controller>/5
        public KontaktOsobaView Get(int id)
        {
            DataProvider provider = new DataProvider();

            return provider.GetKontaktOsobaView(id);
        }

        // POST api/<controller>
        public int Post([FromBody]KontaktOsoba v)//radi, ali je LicnoIme i broj telefona su unique! (nece sa View)
        {
            DataProvider provider = new DataProvider();

            return provider.AddKontaktOsoba(v);
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]KontaktOsobaView v)//radi
        {
            DataProvider provider = new DataProvider();

            provider.UpdateKontaktOsoba(id, v);

            return;
        }

        // DELETE api/<controller>/5
        public int Delete(int id)//radi
        {
            DataProvider provider = new DataProvider();

            return provider.RemoveKontaktOsoba(id);
        }
    }
}