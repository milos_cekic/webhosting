﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;

using WebHosting;
using WebHosting.DTOs;
using WebHosting.Entiteti;




namespace WebAPI.Controllers
{
    public class PaketController : ApiController
    {
        // GET api/paket?baza=Da&skripting=Ne&staticne=Da
        public IEnumerable<PaketView> Get(string baza, string skripting, string staticne)
        {
            DataProvider provider = new DataProvider();

            IEnumerable<PaketView> paketi = provider.GetPaketi(baza, skripting, staticne);

            return paketi;
        }

        // GET api/paket/5
        public PaketView Get(int id)
        {
            DataProvider provider = new DataProvider();

            return provider.GetPaketView(id);
        }

        // POST api/paket
        public int Post([FromBody]PaketView v)//radi
        {
            DataProvider provider = new DataProvider();

            return provider.AddPaket(v);
        }

        // PUT api/paket/5
        public void Put(int id, [FromBody]PaketView v)//radi
        {
            DataProvider provider = new DataProvider();

            provider.UpdatePaket(id, v);

            return;
        }

        // DELETE api/paket/5
        public int Delete(int id)//radi
        {
            DataProvider provider = new DataProvider();

            return provider.RemovePaket(id);
        }
    }
}