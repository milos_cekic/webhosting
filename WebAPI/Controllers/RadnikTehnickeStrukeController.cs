﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebHosting;
using WebHosting.Entiteti;
using WebHosting.DTOs;

namespace WebAPI.Controllers
{
    public class RadnikTehnickeStrukeController : ApiController
    {
        // GET api/radniktehnickestruke?pozicija=Programer za programere
        // GET api/radniktehnickestruke?pozicija=Administrator za administratore
        public IEnumerable<RadnikTehnickeStrukeView> Get(string pozicija)
        {
            DataProvider provider = new DataProvider();

            IEnumerable<RadnikTehnickeStrukeView> radnici = provider.GetRadnikTehnickeStrukeView(pozicija);

            return radnici;
        }

        // GET api/<controller>/5
        public RadnikTehnickeStrukeView Get(int id, string pozicija)
        {
            DataProvider provider = new DataProvider();
            return provider.GetRadnikTehnickeStrukeView(id, pozicija);
        }

        // POST api/<controller>
        public int Post([FromBody]RadnikTehnickeStrukeView r)//!!!
        {
            DataProvider provider = new DataProvider();
            return provider.AddRadnikTehnickeStruke(r);
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]RadnikTehnickeStrukeView r)//radi
        {
            DataProvider provider = new DataProvider();

            provider.UpdateRadnikTehnickeStruke(id, r);

            return;
        }

        // DELETE api/<controller>/5
        public int Delete(int id)
        {
            DataProvider provider = new DataProvider();

            return provider.RemoveRadnikTehnickeStruke(id);
        }
    }
}