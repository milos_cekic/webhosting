﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebHosting;
using WebHosting.Entiteti;
using WebHosting.DTOs;

namespace WebAPI.Controllers
{
    public class PravnoLiceController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<PravnoLiceView> Get()
        {
            DataProvider provider = new DataProvider();

            IEnumerable<PravnoLiceView> korisnici = provider.GetPravnaLicaView();

            return korisnici;
        }

        // GET api/<controller>/5
        public PravnoLiceView Get(int id)
        {
            DataProvider provider = new DataProvider();

            return provider.GetPravnoLiceView(id);
        }

        // POST api/<controller>
        public int Post([FromBody]PravnoLiceView v)
        {
            //radi lepo, PIB je unique vrednost!
            DataProvider provider = new DataProvider();

            return provider.AddPravnoLice(v);
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]PravnoLiceView v)
        {
            DataProvider provider = new DataProvider();

            provider.UpdatePravnoLice(id, v);

            return;
        }

        // DELETE api/<controller>/5
        public int Delete(int id)//vrati se!!!
        {
            DataProvider provider = new DataProvider();

            return provider.RemovePravnoLice(id);
        }
    }
}