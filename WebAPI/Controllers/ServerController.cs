﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebHosting;
using WebHosting.Entiteti;
using WebHosting.DTOs;

namespace WebAPI.Controllers
{
    public class ServerController : ApiController
    {
        // GET api/<controller> tip: windows ili linux 
        public IEnumerable<ServerView> Get(string tip)
        {
            DataProvider provider = new DataProvider();

            IEnumerable<ServerView> serveri = provider.GetServeriView(tip);

            return serveri;
        }

        // GET api/<controller>/5
        public ServerView Get(int id, string tip)
        {
            DataProvider provider = new DataProvider();
            return provider.GetServerView(id, tip);
        }

        // POST api/<controller>
        public int Post([FromBody]ServerView v, string tip)//radi, ip adresa je unique! 
        {
            DataProvider provider = new DataProvider();

            return provider.AddServer(v, tip);
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]ServerView v, string tip)//radi
        {
            DataProvider provider = new DataProvider();

            provider.UpdateServer(id, v, tip);

            return;
        }

        // DELETE api/<controller>/5
        public int Delete(int id, string tip)//radi
        {
            DataProvider provider = new DataProvider();

            return provider.RemoveServer(id, tip);
        }
    }
}