﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using WebHosting;
using WebHosting.Entiteti;
using WebHosting.DTOs;

namespace WebAPI.Controllers
{
    public class EkonomistaTehnickePodrskeController : ApiController
    {
        // GET api/ekonomistatehnickepodrske
        public IEnumerable<EkonomistaTehnickePodrskeView> Get()
        {
            DataProvider provider = new DataProvider();

            IEnumerable<EkonomistaTehnickePodrskeView> ekonomisti = provider.GetEkonomistiTehnickePodrskeView();

            return ekonomisti;
        }

        // GET api/ekonomistatehnickepodrske/5
        public EkonomistaTehnickePodrskeView Get(int id)
        {
            DataProvider provider = new DataProvider();

            return provider.GetEkonomistaTehnickePodrskeView(id);
        }

        // POST api/ekonomistatehnickepodrske
        public int Post([FromBody]EkonomistaTehnickePodrskeView v)
        {
            DataProvider provider = new DataProvider();

            return provider.AddEkonomistaTehnickePodrske(v);
        }

        // PUT api/ekonomistatehnickepodrske/5
        public void Put(int id, [FromBody]EkonomistaTehnickePodrskeView v)
        {
            DataProvider provider = new DataProvider();

            provider.UpdateEkonomistaTehnickePodrske(id, v);

            return;
        }

        // DELETE api/ekonomistatehnickepodrske/5
        public int Delete(int id)
        {
            DataProvider provider = new DataProvider();

            return provider.RemoveEkonomistaTehnickePodrske(id);
        }
    }
}