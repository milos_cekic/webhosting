﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebHosting;
using WebHosting.Entiteti;
using WebHosting.DTOs;

namespace WebAPI.Controllers
{
    public class RadnikKorisnickePodrskeController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<RadnikView> Get()
        {
            DataProvider provider = new DataProvider();

            IEnumerable<RadnikView> radnici = provider.GetRadniciKorisnickePodrskeView();

            return radnici;
        }

        // GET api/<controller>/5
        public RadnikView Get(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.GetRadnikKorisnickePodrskeView(id);
        }

        // POST api/<controller>
        public int Post([FromBody]RadnikView r)//!!!
        {
            DataProvider provider = new DataProvider();
            return provider.AddRadnikKorisnickePodrske(r);
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]RadnikView r)
        {
            DataProvider provider = new DataProvider();

            provider.UpdateRadnikKorisnickePodrske(id, r);

            return;
        }

        // DELETE api/<controller>/5
        public int Delete(int id)
        {
            DataProvider provider = new DataProvider();

            return provider.RemoveRadnikKorisnickePodrske(id);
        }
    }
}