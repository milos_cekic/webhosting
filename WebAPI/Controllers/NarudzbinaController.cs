﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebHosting;
using WebHosting.Entiteti;
using WebHosting.DTOs;

namespace WebAPI.Controllers
{
    public class NarudzbinaController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<NarudzbinaView> Get()
        {
            DataProvider provider = new DataProvider();

            IEnumerable<NarudzbinaView> narudzbine = provider.GetNarudzbineView();

            return narudzbine;
        }

        // GET api/<controller>/5
        public NarudzbinaView Get(int id)
        {
            DataProvider provider = new DataProvider();

            return provider.GetNarudzbinaView(id);
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}