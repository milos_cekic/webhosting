﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebHosting;
using WebHosting.Entiteti;
using WebHosting.DTOs;

namespace WebAPI.Controllers
{
    public class OdrzavaPaketeController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<OdrzavaPaketeView> Get()
        {
            DataProvider provider = new DataProvider();

            IEnumerable<OdrzavaPaketeView> odrzavanja = provider.GetOdrzavaPaketeListaView();

            return odrzavanja;
        }

        // GET api/<controller>/5
        public OdrzavaPaketeView Get(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.GetOdrzavaPaketeView(id);
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}