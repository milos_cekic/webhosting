﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebHosting;
using WebHosting.Entiteti;
using WebHosting.DTOs;

namespace WebAPI.Controllers
{
    public class FizickoLiceController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<FizickoLiceView> Get()
        {
            DataProvider provider = new DataProvider();

            IEnumerable<FizickoLiceView> korisnici = provider.GetFizickaLicaView();

            return korisnici;
        }

        // GET api/<controller>/5
        public FizickoLiceView Get(int id)
        {
            DataProvider provider = new DataProvider();

            return provider.GetFizickoLiceView(id);
        }

        // POST api/<controller>
        public int Post([FromBody]FizickoLiceView v)//radi
        {
            //radi lepo, JMBG je unique vrednost!
            DataProvider provider = new DataProvider();

            return provider.AddFizickoLice(v);
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]FizickoLiceView v)//radi
        {
            DataProvider provider = new DataProvider();

            provider.UpdateFizickoLice(id, v);

            return;
        }

        // DELETE api/<controller>/5
        public int Delete(int id)//radi
        {
            DataProvider provider = new DataProvider();

            return provider.RemoveFizickoLice(id);
        }
    }
}